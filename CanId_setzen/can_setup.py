# pip3 install --user python-can
# note the "python" in front of the "can"
# "can" would install a different library called libcan!
import can

# standard
import atexit
import platform


BITRATE_250k = 250000
BITRATE_500k = 500000


buses = list()

def get_bus(busid=0):
	if isinstance(busid, int):
		return buses[busid]
	else:
		bus = next((bus for bus in buses if bus.channel==busid), None)

		if bus == None:
			raise IndexError("bus %s is not initialized" % busid)

		return bus


if platform.system() == "Linux":
	import subprocess

	def format_cmd(cmd):
		return "    $ " + " ".join(cmd)

	def run(cmd):
		print("[executing %s]" % format_cmd(cmd).strip())
		subprocess.run(cmd, check=True)

	def init(bitrate, channel=None):
		global cmd_bus_up, cmd_bus_down
		can_interface = channel
		can_interface = "can0" if can_interface is None else can_interface

		cmd_bus_down = ("sudo", "ip", "link", "set", can_interface, "down")
		cmd_bus_up   = ("sudo", "ip", "link", "set", can_interface, "up", "type", "can", "bitrate", str(bitrate))

		check_configured_bitrate(can_interface, bitrate)

		bus = can.interface.Bus(can_interface, bustype="socketcan")
		buses.append(bus)
		return bus


	def check_configured_bitrate(can_interface, bitrate):
		if can_interface[0] == 'v':
			# virtual CAN bus has no bitrate
			return

		cmd = "ip -d link show " + can_interface + " | grep 'state DOWN'"
		p = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=False)
		if p.returncode == 0:
			try:
				run(cmd_bus_up)
				return
			except:
				pass

			print("can bus is down")
			print("please try to start the bus with")
			print("")
			print(format_cmd(cmd_bus_up))
			print("")

			exit(1)


		cmd = "ip -d link show " + can_interface + " | awk '/bitrate/ {print $2}'"

		# check is not that useful here because awk overrides the return code of ip
		p = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)

		if p.stderr:
			print(p.stderr.decode(encoding="utf8"))
			exit(1)

		if not p.stdout:
			try:
				run(cmd_bus_up)
				return
			except:
				pass

			print("failed to determine the bitrate")
			print("please try to start the bus with")
			print("")
			print(format_cmd(cmd_bus_up))
			print("")

			exit(1)

		configured_bitrate = int(p.stdout)

		if configured_bitrate != bitrate:
			try:
				run(cmd_bus_down)
				run(cmd_bus_up)
				return
			except:
				pass

			print("please change the bitrate from {configured} bit/s to {required} bit/s.".format(configured=configured_bitrate, required=bitrate))
			print()
			print(format_cmd(cmd_bus_down))
			print(format_cmd(cmd_bus_up))
			print()

			exit(1)

	def handle_exception(exc):
		raise exc
		print(exc)
		print("please try to start the bus with")
		print("")
		print(format_cmd(cmd_bus_up))
		print("")
		print("", end="", flush=True)
		exit(1)

else:
	def init(bitrate, channel=None):
		bus = can.Bus(channel, bustype="pcan", bitrate=bitrate)
		bus.channel = ""
		buses.append(bus)
		return bus

	def handle_exception(exc):
		raise exc


def ensure_one_bus_is_initialized(bitrate, channel=None):
	if buses:
		return

	return init(bitrate, channel=channel)

def reinit(bitrate, channel=None):
	if buses:
		shutdown_if_up(busid=channel)

	return init(bitrate, channel=channel)


def shutdown_if_up(busid=None):
	"""Closes one bus or all buses.
	
	Trying to close a bus which is not open is no problem.
	This function is called automatically by atexit and
	does not need to be called manually.
	However, it can be useful for flushing the input buffer.
	
	Keyword arguments:
	busid -- An identifier for the CAN bus to listen on.
	         If None: Close all CAN buses.
	         Otherwise: Either the channel passed to init
	         or the index in which order the bus was initialized.
	
	Return value:
	None
	"""

	def shutdown(bus):
		bus.shutdown()
		if bus.channel:
			print("bus %s was shutdown" % bus.channel)
		else:
			print("bus was shutdown")

	if busid == None:
		if not buses:
			print("[no buses were initialized]")
			return

		for bus in buses:
			shutdown(bus)

		buses.clear()

	else:
		try:
			bus = get_bus(busid)
			buses.remove(bus)
			shutdown(bus)

		except IndexError:
			print("[bus %s was not initialized]" % busid)

atexit.register(shutdown_if_up)
