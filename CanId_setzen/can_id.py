import can
import cantools
import os
import model
import time
import can_setup
import sys
import collections


SList = []
SDict = {}
BROADCAST_ID = 0x0
STANDARD_ID = 0xFF
BAUDRATE = 250e3
CM_amount = 4
CM_count = 0
time_between_doors_closed = 0
last_time = 0
current_time = 0
start_time = 0
first_time_set = False

dbc_name = r"cmcu_ipc.dbc"
# determine if application is a script file or frozen exe
if getattr(sys, 'frozen', False):
    application_path = os.path.dirname(sys.executable)
elif __file__:
    application_path = os.path.dirname(__file__)

FILEPATH = os.path.join(application_path, dbc_name)


nodeId_init = False
bus = None
stack = None
dbc = None
mdl = None


### helper object to setup CAN RX callback
class ListenerSubclass(can.Listener):
    def __init__(self, on_receive):
        self._on_receive = on_receive

    def on_message_received(self, msg):
        self._on_receive(msg)


def split_arbitration_id(arbitration_id):
    slot_num = (arbitration_id >> 16) & 0x7
    device_id = (arbitration_id >> 8) & 0xFF
    message_id = arbitration_id & 0xFF
    return slot_num, device_id, message_id


def join_arbitration_id(device_id, slot_num, message_id):
    arbitration_id = (slot_num << 16) | (device_id << 8) | message_id
    return arbitration_id


def send_IPC_Cmd(value, CMCU_CAN_ID):
    global mdl
    message_mdl = mdl.getMessageByName('IPC_Cmd')
    message_mdl.getSignalByName('Cmd').value = value
    transmitCANMessage(message_mdl, CMCU_CAN_ID)


def send_IPC_Set_CAN_ID(id, CMCU_CAN_ID):
    global mdl
    message_mdl = mdl.getMessageByName('IPC_Set_CAN_ID')
    message_mdl.getSignalByName('IPC_Config_CAN_ID').value = id
    transmitCANMessage(message_mdl, CMCU_CAN_ID)


def transmitCANMessage(message_mdl, CMCU_CAN_ID):
    global mdl, bus
    try:
        message_prototype = dbc.get_message_by_name(message_mdl.name)
        data_signals = {}
        for signal in message_mdl.signals:
            data_signals.update({signal.name: signal.value})
        data = message_prototype.encode(data_signals, strict=False)

        canMessage = can.Message(
            arbitration_id=join_arbitration_id(CMCU_CAN_ID,
                                                0,
                                                message_prototype.frame_id),
            data=data
        )
        bus.send(canMessage)
        print(f"sent {data_signals} to {CMCU_CAN_ID}")

    except Exception as e:
        print('ERROR SENDING')
        print(e)


def setup_mdl(filepath):
    global mdl, dbc
    dbc = cantools.database.load_file(filepath)

    for message in dbc.messages:
        mdl_message = model.Message(message)

        for signal in message.signals:
            if signal.name.find('CMCU_Bat') == 0:
                name = signal.name
                for i in range(8):
                    signal.name = name[:8] + str(i) + name[8:]
                    mdl_signal = model.Signal(signal, mdl_message)
                    mdl_message.signals.append(mdl_signal)
                signal.name = name
            else:
                mdl_signal = model.Signal(signal, mdl_message)
                mdl_message.signals.append(mdl_signal)

        mdl.messages.append(mdl_message)


def receiveCANMessage(message):
    global dbc, mdl, nodeId_init, CM_amount, CM_count,\
        time_between_doors_closed, last_time, current_time, start_time, first_time_set
    message_id = None
    try:
        slotNum, device_id, message_id = split_arbitration_id(message.arbitration_id)
        raw_data = dbc.decode_message(message_id | 0x1FF80000, message.data)

        for key, value in raw_data.items():
            if nodeId_init == True:
                if not first_time_set:
                    start_time = time.time()
                    first_time_set = True
                current_time = time.time()
                # pre process is finished
                if 'CMCU_State' in key:
                    print(value)
                    try:
                        if ("DOOR_CLOSED" in str(value)) and (device_id == STANDARD_ID):
                            ''' if received == DOOR_CLOSED'''
                            print('Door was closed, sent out Node id')
                            CM_count += 1
                            send_IPC_Set_CAN_ID(CM_count, STANDARD_ID)
                            last_time = time.time()
                    except Exception as e:
                        print(e)

            if CM_count == 0:
                time_between_doors_closed = current_time - start_time
            else:
                time_between_doors_closed = current_time - last_time



            if 'Serial' in key and 'Bat' not in key:
                # check for cmcu serian number
                if value not in SDict.keys():
                    SDict[value] = device_id
                    print(f'Received Serialnbr {value} from CMCU_ID {device_id} | {hex(message.arbitration_id)}')
                    CM_amount = len(SDict)
                    print(f'amount of CMs: {CM_amount}')
                elif SDict[value] != device_id:
                    oldID = SDict[value]
                    SDict[value] = device_id
                    print(f'ID of Serialnbr {value} changed from {oldID} to {device_id} | {hex(message.arbitration_id)}')




        if (CM_count == CM_amount) or time_between_doors_closed > 15:
            nodeId_init = False

    except Exception as e:
        print(e)



def run_setCanId():
    global mdl, bus, nodeId_init
    mdl = model.Model()
    setup_mdl(FILEPATH)
    bus = can_setup.init(bitrate=250000)
    can_notifier = can.Notifier(bus, [ListenerSubclass(receiveCANMessage)])
    # send_IPC_Cmd('RESTART', BROADCAST_ID)
    # time.sleep(2)
    send_IPC_Set_CAN_ID(STANDARD_ID, BROADCAST_ID)  # set all cmcus to id 255
    time.sleep(2)
    send_IPC_Cmd('DOOR_OPEN', STANDARD_ID)  # open all doors
    time.sleep(3)
    nodeId_init = True
    while nodeId_init:
        pass
    time.sleep(2)
    for NodeID in range(1, CM_amount + 1):
        send_IPC_Cmd('DOOR_OPEN', NodeID)
        time.sleep(2)



if __name__ == '__main__':
    run_setCanId()
print("All Node id's were set")
