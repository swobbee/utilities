class Model:
    def __init__(self):
        self.messages = list()

    def getMessageByName(self, name):
        for message in self.messages:
            if message.name == name:
                return message
        return None

    def getSignalByName(self, name):
        for message in self.messages:
            for signal in message.signals:
                if signal.name == name:
                    return signal
        return None


class Message:
    def __init__(self, dbc_message):
        self.name = dbc_message.name
        self.value = None  # TODO Rmv
        self.signals = list()

        if self.name.find('CMCU_') != -1:
            self.readOnly = True
        else:
            self.readOnly = False

    def getSignalByName(self, name):
        for signal in self.signals:
            if signal.name == name:
                return signal
        return None


class Signal:
    def __init__(self, dbc_signal, parent_model):
        self.name = dbc_signal.name

        if hasattr(dbc_signal, 'choices'):
            self.choices = dbc_signal.choices
        else:
            self.choices = None
        if hasattr(dbc_signal, 'unit'):
            self.unit = dbc_signal.unit
        else:
            self.unit = None

        self.value = None

        # readOnly if parent message was readOnly
        self.readOnly = parent_model.readOnly

    def getChoicesIndexByValue(self, value):
        if self.choices:
            for choice_index_key, choice_value in self.choices.items():
                if choice_value == value:
                    return choice_index_key

        return None
