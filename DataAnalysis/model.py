import pandas as pd
import numpy as np
from collections import namedtuple
import os


class Model():
    def __init__(self):
        self.data = {}
        self.first_date = None
        self.last_date = None

    def load_cm(self, cm_name, filepath):
        names = 'time temp1 temp2 temp3 temp4 temp5 temp6 tempA tempC current TMcurrent'.split()
        df = pd.read_csv(rf'{filepath}',
                         sep=',',
                         names=names,
                         index_col=0,
                         header=0,
                         na_values='',
                         engine='python')

        df.index = pd.to_datetime(df.index)
        if not df.index.is_monotonic:
            df = df.sort_index()

        df = df[~df.index.duplicated()]

        df.fillna(method='bfill', inplace=True)

        # mean std
        df['temp_mean'] = df.iloc[:, :6].mean(axis=1)
        df['temp_std'] = df.iloc[:, :6].std(axis=1)

        self.data.update({cm_name: df})

        if not self.first_date:
            self.first_date = df.index[0]
            self.last_date = df.index[-1]


    def load_SP(self, path_to_data):
        for file in os.listdir(path_to_data):
            filename, file_extension = os.path.splitext(file)
            if file_extension == '.csv':
                self.load_cm(filename, f'{path_to_data}\{file}')

    def filter_by_date(self, start_date, end_date):
        dfs = {}
        for df in self.data:
            _df = self.data[df]
            mask = ((_df.index >= start_date) & (_df.index <= end_date))
            dfs.update({df: _df.loc[mask]})
        return dfs

    def calc_gradient(self, data_column, sample_rate_in_min=1):
        f = f'{sample_rate_in_min}T'
        df = data_column.resample(f).mean()
        # runtime = (dfx - dfx[0]).fillna(pd.Timedelta(seconds=0)).astype('timedelta64[s]')
        #y_dif = np.diff(dfy) / dfx.diff().fillna(pd.Timedelta(seconds=0)).astype('timedelta64[m]')
        dxdy =df.diff()
        y_diff = dxdy.values/ (dxdy.index.to_series().diff()/ pd.Timedelta(minutes=1))
        return y_diff







