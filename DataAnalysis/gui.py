import dash
import dash_core_components as dcc
import dash_html_components as html
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly
from collections import namedtuple
import pandas as pd
import os
import numpy as np
from scipy import integrate
path = os.path.split(__file__)[0]

from model import Model

Trace = namedtuple("Trace", ["x", "y", "signal_name"])


path_to_data = r'C:\Users\p.goldmann\Desktop'
model = Model()
model.load_SP(path_to_data)
dff = model.data

error_raw_1_curves = {}
error_raw_2_curves = {}
colors = plotly.colors.DEFAULT_PLOTLY_COLORS

app = dash.Dash(__name__)
app.layout = html.Div(style={'height': '95vh'}, children=[
    dcc.DatePickerRange(
        id='my-date-picker-range',  # ID to be used for callback
        calendar_orientation='horizontal',  # vertical or horizontal
        day_size=39,  # size of calendar image. Default is 39
        end_date_placeholder_text="Return",  # text that appears when no end date chosen
        with_portal=False,  # if True calendar will open in a full screen overlay portal
        first_day_of_week=0,  # Display of calendar when open (0 = Sunday)
        reopen_calendar_on_clear=True,
        is_RTL=False,  # True or False for direction of calendar
        clearable=True,  # whether or not the user can clear the dropdown
        number_of_months_shown=1,  # number of months shown when calendar is open
        min_date_allowed=model.first_date,  # minimum date allowed on the DatePickerRange component
        max_date_allowed=model.last_date,  # maximum date allowed on the DatePickerRange component
        initial_visible_month=model.first_date,  # the month initially presented when the user opens the calendar
        start_date=model.first_date,
        end_date=model.last_date,
        display_format='MMM Do, YY',  # how selected dates are displayed in the DatePickerRange component.
        month_format='MMMM, YYYY',  # how calendar headers are displayed when the calendar is opened.
        minimum_nights=0,  # minimum number of days between start and end date
        persistence=True,
        persisted_props=['start_date'],
        persistence_type='session',  # session, local, or memory. Default is 'local'
        updatemode='bothdates'  # singledate or bothdates. Determines when callback is triggered
    ),
    dcc.Dropdown(
        id="dfs-dropdown",
        options=[{"label": f"{key}", 'value': f"{key}"} for key in sorted(model.data.keys())],
        multi=True,
        searchable=True,
        style={"width": "50%"}
    ),
    dcc.Dropdown(
        id="bat-dropdown",
        options=[{"label": f"{key}", 'value': f"{key}"} for key in model.data[list(model.data.keys())[0]].columns if 'temp' in key],
        multi=True,
        searchable=True,
        style={"width": "50%"}
    ),
    dcc.RadioItems(
        id='radiobtn',
        options=[
            {'label': 'Show CM',                                          'value': 'single'},
            {'label': 'Compare Mean\n\u03B8 CM',                          'value': 'cm'},
            {'label': 'Compare Mean\nd'+'\u03B8'+'/dt CM',                'value': 'cm_div'},
            {'label': 'Compare Mean\n'+'\u03B8'+' Battery',               'value': 'bat'},
            {'label': 'Compare Mean\n'+'\u03B8'+'/dt Battery',            'value': 'bat_div'},
            {'label': 'Compare relative Mean\n\u03B8 CM',                 'value': 'cm_rel'},
            {'label': 'Compare relative Mean\nd'+'\u03B8'+'/dt CM',       'value': 'cm_div_rel'},
            {'label': 'Compare relative Mean\n'+'\u03B8'+' Battery',      'value': 'bat_rel'},
            {'label': 'Compare relative Mean\n'+'\u03B8'+'/dt Battery',   'value': 'bat_div_rel'}

        ],
        value='single',
        labelStyle={'display': 'inline-block'},
        style={"width": "30%"}
    ),
    dcc.Graph(id="graph", style={'height': 'inherit'}),
    html.Div(id="hidden-div", style={"display": "none"}),
    html.Div(id="hidden-div2", style={"display": "none"})
])


@app.callback(
    [dash.dependencies.Output("hidden-div", "children")],
    [dash.dependencies.Input('my-date-picker-range', 'start_date'),
     dash.dependencies.Input('my-date-picker-range', 'end_date')],
    prevent_initial_call=True
)
def filterDate(start_date, end_date):
    if (start_date is not None) and (end_date is not None):
        global dff
        dff = model.filter_by_date(start_date, end_date)

    return []


@app.callback(
    dash.dependencies.Output("graph", "figure"),
    [dash.dependencies.Input("dfs-dropdown", "value"),
    dash.dependencies.Input("bat-dropdown", "value"),
    dash.dependencies.Input("radiobtn", "value")],
    prevent_initial_call=False)
def update(cm_selected, bat_selected, style='single'):
    global dff
    if not cm_selected:
        return go.Figure()

    fig = go.Figure()
    if style=='single':
        fig = plot_cm_data(cm_selected, dff)
    elif style=='cm':
        fig = plot_mean(cm_selected, dff)
    elif style=="cm_div":
        fig = plot_div(cm_selected, dff)
    elif style == 'bat':
        fig = plot_mean(cm_selected, dff, bat_selected=bat_selected)
    elif style=="bat_div":
        fig = plot_div(cm_selected, dff, bat_selected=bat_selected)

    return fig



def plot_div(cm_selected, data, bat_selected=None):
    legends = set()
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    if not bat_selected:
        bat_selected=['temp_mean']


    for curve_number, CM in enumerate(cm_selected):
        for bat_nr, bat_name in enumerate(bat_selected):
            showlegend = (CM and bat_selected[bat_nr]) not in legends
            legends.add((CM, bat_nr))
            y_dif = model.calc_gradient(data[CM][bat_name])
            x_dif = y_dif.index

            fig.add_trace(
                go.Scattergl(
                    x=x_dif,
                    y=y_dif,
                    name=f'\u03B8_{CM}_{bat_name}',
                    showlegend=showlegend,
                    line={"color": colors[curve_number+bat_nr+1 % len(colors)],
                          "shape": "hv"}
                )
            )

            # y_data = dff[CM]["current"]
            # fig.add_trace(
            #     go.Scattergl(
            #         x=dfx,
            #         y=y_data,
            #         name=f'I_{CM}',
            #         showlegend=showlegend,
            #         line={"color": colors[curve_number+1 % len(colors)],
            #               "shape": "hv"}
            #
            #     ),
            #     secondary_y=True
            # )
            #
            # y_data = dff[CM]["TMcurrent"]
            #
            # fig.add_trace(
            #     go.Scattergl(
            #         x=dfx,
            #         y=y_data,
            #         name=f'I_TM',
            #         showlegend=showlegend,
            #         line={"color": colors[curve_number+2 % len(colors)],
            #               "shape": "hv"}
            #
            #     ),
            #     secondary_y=True
            # )

        # Add figure title
        fig.update_layout(
            title_text="Change of Temperature per ChargeModules"
        )
        # Add X-Label
        fig.update_xaxes(title_text="Date & Time", showspikes=True, spikemode="across+marker")
        # Add Y-Label
        fig.update_yaxes(title_text="Temperature Change [°C/min]", secondary_y=False)
        fig.update_yaxes(title_text="Current [A]", secondary_y=True)
        fig.update_traces(xaxis=f"x{len(cm_selected)}")
        # fig.update_layout({"uirevision": True, "clickmode": "event"})
    return fig



def plot_mean(cm_selected, data, bat_selected=None):
    legends = set()
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    if not bat_selected:
        bat_selected=['temp_mean']

    for curve_number, CM in enumerate(cm_selected):
        for bat_nr, bat_name in enumerate(bat_selected):
            showlegend = (CM and bat_selected[bat_nr]) not in legends
            legends.add((CM, bat_nr))
            y_data = data[CM][bat_name]
            x_data = data[CM][bat_name].index

            fig.add_trace(
                go.Scattergl(
                    x=x_data,
                    y=y_data,
                    name=f'{CM}_{bat_name}',
                    showlegend=showlegend,
                    line={"color": colors[curve_number+bat_nr % len(colors)],
                          "shape": "hv"}
                )
            )

        y_data = dff[CM]["current"]
        fig.add_trace(
            go.Scattergl(
                x=x_data,
                y=y_data,
                name=f'I_{CM}',
                showlegend=showlegend,
                line={"color": colors[curve_number+1 % len(colors)],
                      "shape": "hv"}

            ),
            secondary_y=True
        )
    # Add figure title
    fig.update_layout(
        title_text="Mean Temperature of ChargeModules"
    )
    # Add X-Label
    fig.update_xaxes(title_text="Date & Time", showspikes=True, spikemode="across+marker")
    # Add Y-Label
    fig.update_yaxes(title_text="Temperature [°C]", secondary_y=False)
    fig.update_yaxes(title_text="Current [A]", secondary_y=True)
    fig.update_traces(xaxis=f"x{len(cm_selected)}")
    #fig.update_layout({"uirevision": True, "clickmode": "event"})
    return fig





def plot_cm_data(cm_selected, dff):
    fig = make_subplots(rows=len(cm_selected), cols=1, shared_xaxes=True)

    traces = {}
    legends = set()
    print(f'called single plot')
    for plot_row, CM in enumerate(cm_selected):
        signaldata = dff[CM]
        for signal_name, series in signaldata.items():
            x = series.index
            y = series.values
            traces[(CM, signal_name, plot_row)] = Trace(x, y, signal_name)

    for curve_number, (CM, signal_name, plot_row) in enumerate(sorted(traces.keys())):
        showlegend = (CM, signal_name) not in legends
        legends.add((CM, signal_name))
        trace = traces[(CM, signal_name, plot_row)]
        x_data = trace.x
        y_data = trace.y
        if "std" in signal_name:
            std = trace.y
            mean = traces[(CM, 'temp_mean', plot_row)].y
            y_upper = mean+std
            y_lower = mean-std

            fig.add_trace(
                go.Scattergl(
                    name='Upper Bound',
                    x=x_data,
                    y=y_upper,
                    mode='lines',
                    marker=dict(color="#444"),
                    line=dict(width=0),
                    showlegend=False
                ),
                row=plot_row+1,
                col=1
            )

            fig.add_trace(
                go.Scattergl(
                    name='Lower Bound',
                    x=x_data,
                    y=y_lower,
                    marker=dict(color="#444"),
                    line=dict(width=0),
                    mode='lines',
                    fillcolor='rgba(68, 68, 68, 0.3)',
                    fill='tonexty',
                    showlegend=False
                ),
                row=plot_row+1,
                col=1
            )

        else:
            fig.add_trace(
                go.Scattergl(
                    x=x_data,
                    y=y_data,
                    name=f'{CM}_{signal_name}',
                    legendgroup=signal_name,
                    showlegend=showlegend,
                    line={"color": colors[curve_number % len(colors)],
                          "shape": "hv"}
                ),
                row=plot_row + 1,
                col=1
            )

    # Add figure title
    fig.update_layout(
        title_text="ChargeModule Data"
    )
    # Add X-Label
    fig.update_xaxes(title_text="Date & Time", showspikes=True, spikemode="across+marker")
    # Add Y-Label
    fig.update_yaxes(title_text="Temperature [°C]")
    fig.update_traces(xaxis=f"x{len(cm_selected)}")
    fig.update_layout({"uirevision": True, "clickmode": "event"})
    return fig



# def plot_integral(fig, cm_selected, dff):
#     print('called integral plot')
#     legends = set()
#     fig = make_subplots(specs=[[{"secondary_y": True}]])
#     for curve_number, CM in enumerate(cm_selected):
#         showlegend = CM not in legends
#         legends.add(CM)
#         dfy = dff[CM]['temp_mean']
#         dfx = dfy.index
# 
#         runtime = (dfx - dfx[0]).fillna(pd.Timedelta(seconds=0)).astype('timedelta64[s]')
#         Y = integrate.cumtrapz(dfy,runtime, initial=0)
# 
# 
#         fig.add_trace(
#             go.Scattergl(
#                 x=dfx,
#                 y=Y,
#                 name=f'\u03B8_{CM}',
#                 showlegend=showlegend,
#                 line={"color": colors[curve_number % len(colors)],
#                       "shape": "hv"}
#             )
#         )
# 
#         y_data = dff[CM]["current"]
#         fig.add_trace(
#             go.Scattergl(
#                 x=dfx,
#                 y=y_data,
#                 name=f'I_{CM}',
#                 showlegend=showlegend,
#                 line={"color": colors[curve_number + 1 % len(colors)],
#                       "shape": "hv"}
# 
#             ),
#             secondary_y=True
#         )
# 
#         y_data = dff[CM]["TMcurrent"]
# 
#         fig.add_trace(
#             go.Scattergl(
#                 x=dfx,
#                 y=y_data,
#                 name=f'I_TM',
#                 showlegend=showlegend,
#                 line={"color": colors[curve_number + 2 % len(colors)],
#                       "shape": "hv"}
# 
#             ),
#             secondary_y=True
#         )
#     return fig


app.run_server(debug=True)
