import cantools
import logging
from datetime import datetime
import os
import sys

root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../")
sys.path.append(root_dir)
from BatterieSimulator.battery import Battery

logger = logging.getLogger(__name__)

DBC_OKAI = os.path.join(root_dir, "doc", "interface_definitions", "bat_okai.dbc")

class BatteryOkai(Battery):
    def __init__(self, can_bus):
        super(BatteryOkai, self).__init__(can_bus)

        self.dbc = cantools.database.load_file(DBC_OKAI)
        self.skip_message_bool = False

        self.proto_bat_state = self.dbc.get_message_by_name("BMS_BatStateResp")
        self.proto_serial_number = self.dbc.get_message_by_name("BMS_SerialNumberResp")
        self.proto_voltage_current = self.dbc.get_message_by_name("BMS_VoltageCurrentResp")
        self.proto_temperature = self.dbc.get_message_by_name("BMS_TemperatureResp")
        self.proto_error_status_1 = self.dbc.get_message_by_name("BMS_ErrorStatus1Resp")
        self.proto_error_status_2 = self.dbc.get_message_by_name("BMS_ErrorStatus2Resp")
        self.proto_techCV = self.dbc.get_message_by_name("BMS_TechCVResp")

        # initialize error options supported by battery
        self.errors.update({signal.name: False for signal in self.proto_error_status_1.signals})
        self.errors.update({signal.name: False for signal in self.proto_error_status_2.signals})
        self.msg_names = [msg.name for msg in self.dbc.messages if msg.name.find('CMCU')>-1]
        self.skip_msg_dict = {msg_name:False for msg_name in self.msg_names}





    def run(self):
        # nothing to do here, OKAI battery only sends data when requested
        pass

    def set_skip_message(self, skip = False, msg_name='All'):
        if msg_name == 'All':
            self.skip_msg_dict = {msg_name:skip for msg_name in self.msg_names}
        else:
            self.skip_msg_dict[msg_name] = skip

    def process_rx_msg(self, msg):
        try:
            try:
                name = self.dbc.get_message_by_frame_id(msg.arbitration_id).name
            except KeyError as ke:
                print(f'received {hex(ke.args[0])}')
                return
            if name == "CMCU_BatStateReq" and not self.skip_msg_dict[name]:
                self.send_bat_state()
            elif name == "CMCU_SerialNumberReq" and not self.skip_msg_dict[name]:
                self.send_serial_number()
            elif name == "CMCU_VoltageCurrentReq" and not self.skip_msg_dict[name]:
                self.send_voltage_current()
            elif name == "CMCU_TemperatureReq" and not self.skip_msg_dict[name]:
                self.send_temperature()
            elif name == "CMCU_ErrorStatus1Req" and not self.skip_msg_dict[name]:
                self.send_error_status_1()
            elif name == "CMCU_ErrorStatus2Req" and not self.skip_msg_dict[name]:
                self.send_error_status_2()
            elif name == "CMCU_TechCVReq" and not self.skip_msg_dict[name]:
                self.send_tech_cv()


            if self.skip_msg_dict[name]:
                self.rx_signal.emit([datetime.fromtimestamp(msg.timestamp).strftime('%Y-%m-%dT%H:%M:%SZ'), "skiped msg"])
            else:
                self.rx_signal.emit([datetime.fromtimestamp(msg.timestamp).strftime('%Y-%m-%dT%H:%M:%SZ'), name])
        except Exception as e:
            logger.exception(e)

    def send_bat_state(self):
        info = {
            "BMS_SOC": self.soc,
            "BMS_SOH": self.soh
        }
        data = self.proto_bat_state.encode(info)
        self.send_can_msg(self.proto_bat_state, data)
        self.tx_signal.emit([datetime.now(),self.proto_bat_state.name, info])

    def send_serial_number(self):
        data = self.serialnbr.to_bytes(self.proto_serial_number.length, byteorder="big")
        self.send_can_msg(self.proto_serial_number, data)
        self.tx_signal.emit([datetime.now(), self.proto_serial_number.name, data])

    def send_voltage_current(self):
        info = {
            "BMS_BatVoltage": self.voltage,
            "BMS_BatCurrent": self.current
        }
        data = self.proto_voltage_current.encode(info)
        self.send_can_msg(self.proto_voltage_current, data)
        self.tx_signal.emit([datetime.now(), self.proto_bat_state.name, info])

    def send_temperature(self):
        info = {
            "BMS_TempMax": self.temperature_max,
            "BMS_TempMin": self.temperature_min
        }
        data = self.proto_temperature.encode(info)
        self.send_can_msg(self.proto_temperature, data)
        self.tx_signal.emit([datetime.now(), self.proto_temperature.name, info])

    def send_tech_cv(self):
        info = {
            "BMS_RecentlyUnchargedTime": 0,
            "BMS_MaxUnchargedTime": 0,
            "BMS_ChargeCycles": 1
        }
        data = self.proto_techCV.encode(info)
        self.send_can_msg(self.proto_techCV, data)
        self.tx_signal.emit([datetime.now(), self.proto_techCV.name, info])


    def send_error_status_1(self):
        error_dict = {signal.name: self.errors[signal.name] for signal in self.proto_error_status_1.signals}
        data = self.proto_error_status_1.encode(error_dict)
        self.send_can_msg(self.proto_error_status_1, data)
        self.tx_signal.emit([datetime.now(), self.proto_error_status_1.name, {key:value for key,value in error_dict.items() if value==True}])

    def send_error_status_2(self):
        error_dict = {signal.name: self.errors[signal.name] for signal in self.proto_error_status_2.signals}
        data = self.proto_error_status_2.encode(error_dict)
        self.send_can_msg(self.proto_error_status_2, data)
        self.tx_signal.emit([datetime.now(), self.proto_error_status_2.name, {key:value for key,value in error_dict.items() if value==True}])


if __name__ == "__main__":
    import can

    battery_bus = can.interface.Bus('test', bustype='virtual')
    battery = BatteryOkai(battery_bus)

    can_notifier = can.Notifier(battery_bus, [BatteryOkai(battery_bus)])

    testmessage = can.Message()
    testmessage.arbitration_id = 0x2294000
    testmessage.dlc = 0
    testmessage.is_extended_id = True

    test_bus = can.interface.Bus('test', bustype='virtual')
    test_bus.send(testmessage)

    print(test_bus.recv(1024))
