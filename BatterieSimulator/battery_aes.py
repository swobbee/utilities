import cantools
from battery import Battery
import logging
from datetime import datetime
import os
import sys

root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../")
sys.path.append(root_dir)

try:
    from cmcu_unittest.hardwareSimulations.GPIOs import GPIOs
except:
    class GPIOs():
        def __init__(self):
            pass
logger = logging.getLogger(__name__)
DBC_AES = os.path.join(root_dir, "doc", "interface_definitions", "swobbee.dbc")

class BatteryAES(Battery, GPIOs):
    def __init__(self, can_bus):
        super(BatteryAES, self).__init__(can_bus, 0.1)

        self.battery_active = False
        try:
            super(GPIOs, self).__init__()
            self.__init_CN1_gpios__()
        except:
            # simulator does not run on a raspberry pi
            self.battery_active = True
            self.bat_states = {"wake_up":True}

        self.defualt_node_id = 0xFF
        self.node_id = self.defualt_node_id

        self.number_of_cells = 16
        self.cell_voltages = {f'Cell_Voltage_{str(n).zfill(2)}': float("3." + str(n).zfill(2)) for n in range(1, self.number_of_cells+1)}
        self.cell_temperatures = {f'Temperature_{str(n).zfill(2)}': 20 + n for n in range(1, self.number_of_cells+1)}

        self.dbc = cantools.database.load_file(DBC_AES)
        self.msg_names = [msg.name for msg in self.dbc.messages]

        # tx message prototypes
        self.proto_state = self.dbc.get_message_by_name("State")
        self.proto_info = self.dbc.get_message_by_name("Info")
        self.proto_serial_number = self.dbc.get_message_by_name("Serial_Number")
        self.proto_voltages_1 = self.dbc.get_message_by_name("Cell_Voltages_1")
        self.proto_voltages_2 = self.dbc.get_message_by_name("Cell_Voltages_2")
        self.proto_temperatures_1 = self.dbc.get_message_by_name("Temperatures_1")
        self.proto_temperatures_2 = self.dbc.get_message_by_name("Temperatures_2")
        self.proto_error = self.dbc.get_message_by_name("Error")
        self.proto_warning = self.dbc.get_message_by_name("Warning")

        # initialize error options supported by battery
        self.errors.update({signal.name: False for signal in self.proto_error.signals})
        self.errors.update({signal.name: False for signal in self.proto_warning.signals})

        # bat states
        self.charge_path_open = False
        self.discharge_path_open = False
        self.error = False

        self.skip_message_bool = False

    def battery_event_handling(self):
        self.battery_active = self.bat_states['wake_up']
        print(f'batstate = {self.battery_active}')

    def run(self):
        if self.initialized and self.battery_active:
            self.send_bat_state()
        elif self.initialized and not self.battery_active:
            self.initialized = False
            self.node_id = self.defualt_node_id

    def process_rx_msg(self, msg):
        if (msg.arbitration_id >> 8) & 0xFF == self.node_id:
            if msg.arbitration_id == 0x1BEEFF01:
                self.process_init_msg(msg)
            elif (msg.arbitration_id & 0x1BEE00FF) == 0x1BEE0002:
                self.process_set_state_msg(msg)
            else:
                try:
                    if self.initialized:
                        try:
                            raw_data = self.dbc.decode_message(msg.arbitration_id & 0x1BEE00FF, msg.data)
                        except KeyError as ke:
                            print(f'Cannot decode message with ID {hex(ke.args[0])}, ignoring')
                            return

                        for key, msg_id in raw_data.items():
                            if msg_id != 0:
                                name = self.dbc.get_message_by_frame_id(int(hex(msg_id), 16) | 0x1BEE0000).name
                                if name == "State":
                                    self.send_bat_state()
                                elif name == "Info":
                                    self.send_info()
                                elif name == "Serial_Number":
                                    self.send_serial_number()
                                elif name == "Cell_Voltages_1":
                                    self.send_cell_voltages(1)
                                elif name == "Cell_Voltages_2":
                                    self.send_cell_voltages(2)
                                elif name == "Temperatures_1":
                                    self.send_cell_temperatures(1)
                                elif name == "Temperatures_2":
                                    self.send_cell_temperatures(2)
                                elif name == "Error":
                                    self.send_error()
                                elif name == "Warning":
                                    self.send_warning()

                        self.rx_signal.emit([datetime.fromtimestamp(msg.timestamp).strftime('%Y-%m-%dT%H:%M:%SZ'), name])
                except Exception as e:
                    logger.exception(e)

    def process_init_msg(self, msg):
        if self.battery_active:
            self.node_id = self.dbc.decode_message(msg.arbitration_id, msg.data)["Node_ID"]
            self.initialized = True
            self.send_serial_number()

    def process_set_state_msg(self, msg):
        data = self.dbc.decode_message(msg.arbitration_id & 0x1bee00ff, msg.data)
        self.charge_path_open = data["Charge"]
        self.discharge_path_open = data["Discharge"]
        self.send_bat_state()

    def send_serial_number(self):
        data = self.serialnbr.to_bytes(self.proto_serial_number.length, byteorder="big")
        self.send_can_msg(self.proto_serial_number, data, self.node_id << 8)
        self.tx_signal.emit([datetime.now(), self.proto_serial_number.name, data])

    def send_bat_state(self):
        struct = {
            "Pack_Voltage": (self.voltage / 1000),
            "Connector_Voltage": (self.voltage / 1000) - 0.5,
            "Current": self.current / 1000,
            "SOC": self.soc,
            "Temperature_Cell_Min": self.temperature_min,
            "Temperature_Cell_Max": self.temperature_max,
            "Discharge": int(self.discharge_path_open),
            "Charge": int(self.charge_path_open),
            "Error": int(self.error)
        }
        data = self.proto_state.encode(struct)
        self.send_can_msg(self.proto_state, data, self.node_id << 8)
        self.tx_signal.emit([datetime.now(), self.proto_state.name, struct])

    def send_info(self):
        struct = {
            "SOH": self.soh,
            "Charge_Cycles": self.cycles,
            "Firmware_Version": 123
        }
        data = self.proto_info.encode(struct)
        self.send_can_msg(self.proto_info, data, self.node_id << 8)
        self.tx_signal.emit([datetime.now(), self.proto_info.name, struct])

    def send_cell_voltages(self, num):
        if num == 1:
            proto = self.proto_voltages_1
            data = proto.encode(dict([(key, value) for key,value in self.cell_voltages.items() if key in list(self.cell_voltages.keys())[:8]]))
        else:
            proto = self.proto_voltages_2
            data = proto.encode(dict([(key, value) for key,value in self.cell_voltages.items() if key in list(self.cell_voltages.keys())[8:]]))
        
        self.send_can_msg(proto, data, self.node_id << 8)
        self.tx_signal.emit([datetime.now(), proto, self.cell_voltages])

    def send_cell_temperatures(self, num):
        if num == 1:
            proto = self.proto_temperatures_1
            data = proto.encode(dict([(key, value) for key,value in self.cell_temperatures.items() if key in list(self.cell_temperatures.keys())[:8]]))
        else:
            proto = self.proto_temperatures_2
            data = proto.encode(dict([(key, value) for key,value in self.cell_temperatures.items() if key in list(self.cell_temperatures.keys())[8:]]))
        self.send_can_msg(proto, data, self.node_id << 8)
        self.tx_signal.emit([datetime.now(), proto, self.cell_temperatures])

    def send_warning(self):
        struct = {"Warning": 0}
        data = self.proto_warning.encode(struct)
        self.send_can_msg(self.proto_error, data, self.node_id << 8)
        self.tx_signal.emit([datetime.now(), self.proto_error, struct])

    def send_error(self):
        struct = {"Error": 0}
        data = self.proto_error.encode(struct)
        self.send_can_msg(self.proto_error, data, self.node_id << 8)
        self.tx_signal.emit([datetime.now(), self.proto_error, struct])
