import sys
from PyQt5 import QtWidgets
from gui import Gui
from battery_okai import BatteryOkai
from battery_gp import BatteryGreenpack
from battery_aes import BatteryAES
from battery_torrot import BatteryTorrot
import can
import can_setup
import logging
from abc import abstractmethod
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--battery', choices=["BatteryOkai", "BatteryGreenpack", "BatteryTorrot", "BatteryAES"], default="BatteryAES")
args = parser.parse_args()

if args.battery == "BatteryOkai":
    battery = BatteryOkai
elif args.battery == "BatteryGreenpack":
    battery = BatteryGreenpack
elif args.battery == "BatteryTorrot":
    battery = BatteryTorrot
elif args.battery == "BatteryAES":
    battery = BatteryAES

logger = logging.getLogger(__name__)
import os

DEBUG = False


app = QtWidgets.QApplication(sys.argv)

if DEBUG:
     can_bus = can.interface.Bus('test', bustype='virtual')
else:
    can_bus = can_setup.init(bitrate=500000, channel='can1')

battery = BatteryAES(can_bus)
gui = Gui(battery)
can_notifier = can.Notifier(can_bus, [battery])


if DEBUG:
    import threading
    import time
    test_bus = can.interface.Bus('test', bustype='virtual')

    def run():
        threading.Timer(0.5, run).start()
        msg = can.Message()
        msg.arbitration_id = 0x2294609  # bat_state_req
        test_bus.send(msg)
        time.sleep(0.02)    # important, virtual bus crashes if we don't have a small delay here
        msg.arbitration_id = 0x2294610  # voltage_current_req
        test_bus.send(msg)
        time.sleep(0.02)
        msg.arbitration_id = 0x2294615  # temperature_req
        test_bus.send(msg)
    run()
try:
    gui.show()
    app.exec_()
except Exception as e:
    logger.exception(e)
