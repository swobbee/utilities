from PyQt5 import uic
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import QMainWindow
from functools import partial
import time
import logging
import os
import sys
logger = logging.getLogger(__name__)
root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../")
sys.path.append(root_dir)

class Gui(QMainWindow):
    def __init__(self, battery):
        super(Gui, self).__init__()

        self.battery = battery

        # self.battery.add_tx_listener(self.tx_listener)
        # self.battery.add_rx_listener(self.rx_listener)

        self.battery.rx_signal.connect(self.rx_listener)
        self.battery.tx_signal.connect(self.tx_listener)


        self.skipMessageCounter = 0


        uic.loadUi(os.path.join(root_dir, "BatterieSimulator", "mainwindow.ui"), self)

        self.timerStart = QtCore.QTimer(self)
        self.timerStop = QtCore.QTimer(self)
        self.timerStart.timeout.connect(self.raiseSOC)
        self.timerStop.timeout.connect(self.lowerCurrent)

        self.pb_start.clicked.connect(self.startCharging)
        self.pb_change.clicked.connect(self.skip_messages)
        self.pb_stop.clicked.connect(self.stopCharging)

        # connect slider with box and update signal
        self.sliders = [self.hs_soc, self.hs_soh,
                        self.hs_voltage, self.hs_current,
                        self.hs_temperature_min, self.hs_temperature_max]

        self.sp_boxes = [self.sp_soc, self.sp_soh,
                         self.sp_voltage, self.sp_current,
                         self.sp_temperature_min, self.sp_temperature_max]

        for slider, box in zip(self.sliders, self.sp_boxes):
            slider.valueChanged.connect(partial(self.update, box))
            box.valueChanged.connect(partial(self.update, slider))

        self.le_serialnbr.textChanged.connect(partial(self.update, None))

        self.cb_errors.addItems(list(battery.errors.keys()))
        self.cb_msg.addItems(battery.msg_names)
        self.cb_errorState.currentIndexChanged.connect(partial(self.update, None))

        self.setInitialValues()

    def setInitialValues(self):
        self.le_serialnbr.setText(str(self.battery.serialnbr))
        self.sp_soc.setValue(self.battery.soc)
        self.sp_soh.setValue(self.battery.soh)
        self.sp_voltage.setValue(self.battery.voltage)
        self.sp_current.setValue(self.battery.current)
        self.sp_temperature_min.setValue(self.battery.temperature_min)
        self.sp_temperature_max.setValue(self.battery.temperature_max)

    def tx_listener(self, msg):
        try:
            self.tb_tx.append(f'{msg[0]}\t{msg[1]}\t{msg[2]}')
            self.tb_tx.moveCursor(QtGui.QTextCursor.End)

            logger.info('set message in tx')
        except Exception as e:
            logger.exception(e)
            print(e)

    def rx_listener(self, msg):
        try:
            self.tb_rx.append(f'{msg[0]}\t\t{msg[1]}')
            self.tb_rx.moveCursor(QtGui.QTextCursor.End)
            if self.battery.skip_message_bool:
                self.skipMessageCounter+= 1
                if self.skipMessageCounter == self.sp_skipm.value():
                    self.battery.skip_message(False)
                    self.skipMessageCounter = 0

            logger.info('set message in rx')
        except Exception as e:
            print(e)
            logger.exception(e)

    def stopCharging(self):
        self.timerStart.stop()
        self.sp_current.setValue(0)

    def startCharging(self):
        self.sp_current.setValue(3998)
        self.timerStart.start(10000)

    def raiseSOC(self):
        self.sp_soc.setValue(self.sp_soc.value()+1)
        if self.sp_soc.value() >= 100:
            self.timerStop.start(2000)

    def lowerCurrent(self):
        if self.sp_current.value() <= 0.500:
            self.timerStop.stop()
        else:
            self.sp_current.setValue(self.sp_current.value()-10)


    def skip_messages(self):
        self.battery.set_skip_message(skip=True, msg_name=self.cb_msg.currentText())





    @QtCore.pyqtSlot(int)
    def update(self, linked_source):
        try:
            source = self.sender()
            sig_name = source.objectName().split("_", 1)[1]

            if linked_source is not None:
                # soc, soh, current, voltage
                if type(source) == QtWidgets.QSlider:
                    value = int(source.value())
                else:
                    value = source.value()
                linked_source.setValue(value)
                setattr(self.battery, sig_name, value)

            else:
                # error or serial number
                if 'error' in source.objectName():
                    sig_name = self.cb_errors.currentText()
                    idx = self.cb_errors.currentIndex()
                    if idx is not 0:
                        value = True if source.currentText()=="True" else False
                        self.battery.errors.update({sig_name:value})
                        print(sig_name, value)
                    else:
                        [self.battery.errors.update({key:False}) for key in self.battery.errors.keys()]
                else:
                    try:
                        value = int(source.text())
                    except ValueError:
                        value = 0

                    setattr(self.battery, sig_name, value)




        except Exception as e:
            print(e)
            logger.exception(e)
