import cantools
import logging
from datetime import datetime
import os
import sys

root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../")
sys.path.append(root_dir)
from BatterieSimulator.battery import Battery


DBC_OKAI = r"../doc/interface_definitions/bat_gp.dbc"

class BatteryGreenpack(Battery):
    def __init__(self, can_bus):
        super(BatteryGreenpack, self).__init__(can_bus, run_period=0.1)

    def run(self):
        # todo send status messages
        pass

    def process_rx_msg(self, msg):
        pass


if __name__ == "__main__":
    import can

    battery_bus = can.interface.Bus('test', bustype='virtual')
    battery = BatteryGreenpack(battery_bus)

    can_notifier = can.Notifier(battery_bus, [BatteryGreenpack(battery_bus)])

    test_bus = can.interface.Bus('test', bustype='virtual')
    while True:
        print(test_bus.recv(1024))
