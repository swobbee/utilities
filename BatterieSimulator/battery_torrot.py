from battery import Battery
import cantools
import datetime

DBC_TORROT = r"../doc/interface_definitions/bat_torrot.dbc"

class BatteryTorrot(Battery):
    def __init__(self, can_bus):
        super(BatteryTorrot, self).__init__(can_bus, run_period=0.1)

        self.last_heartbeat_received_at = datetime.datetime.fromtimestamp(0)
        self.run_counter = 0

        self.dbc = cantools.database.load_file(DBC_TORROT)
        self.proto_general_1 = self.dbc.get_message_by_name("Bat2_General_1")
        self.proto_general_2 = self.dbc.get_message_by_name("Bat2_General_2")
        self.proto_general_3 = self.dbc.get_message_by_name("Bat2_General_3")
        self.proto_general_4 = self.dbc.get_message_by_name("Bat2_General_4")
        self.proto_temps_1 = self.dbc.get_message_by_name("Bat2_Temps_1")
        self.proto_temps_2 = self.dbc.get_message_by_name("Bat2_Temps_2")
        self.proto_serial_1 = self.dbc.get_message_by_name("Bat2_Serial_1")
        self.proto_serial_2 = self.dbc.get_message_by_name("Bat2_Serial_2")
        self.proto_status_1 = self.dbc.get_message_by_name("Bat2_Status_1")
        self.proto_status_2 = self.dbc.get_message_by_name("Bat2_Status_2")

        self.errors.update({signal.name: False for signal in self.proto_status_1.signals})
        self.errors.update({signal.name: False for signal in self.proto_status_2.signals})

        self.msg_names = [msg.name for msg in self.dbc.messages]

        self.init_runmethod()

    def run(self):
        if datetime.datetime.now() - self.last_heartbeat_received_at < datetime.timedelta(seconds=20):
            self.run_counter += 1

            self.send_temps_1()
            self.send_temps_2()
            self.send_general_1()
            self.send_general_2()
            self.send_general_3()
            self.send_general_4()
            self.send_status_1()
            self.send_status_2()
            if self.run_counter % 10 == 0:
                self.send_serial_1()
                self.send_serial_2()

    def process_rx_msg(self, msg):
        try:
            name = self.dbc.get_message_by_frame_id(msg.arbitration_id).name
        except KeyError as ke:
            print(f'received {hex(ke.args[0])}')
            return
        if name == "ECU_HeartBeat" or name =="ECUWake":
            self.last_heartbeat_received_at = datetime.datetime.now()
            print(f'received wake/heartbeat')

    def send_general_1(self):
        info = {
            "Bat2TotalVoltage": self.voltage,
            "Bat2TotalCurrent": self.current,
            "Bat2RemainingCap": 0,
            "Bat2RatedFCC": 100
        }
        data = self.proto_general_1.encode(info)
        self.send_can_msg(self.proto_general_1, data)
        self.tx_signal.emit([datetime.datetime.now(), self.proto_general_1.name, info])

    def send_general_2(self):
        info = {
            "Bat2CycleCount": 123,
            "Bat2PCBTemp": 25,
            "Bat2FetTemp": 25,
        }
        data = self.proto_general_2.encode(info)
        self.send_can_msg(self.proto_general_2, data)
        self.tx_signal.emit([datetime.datetime.now(), self.proto_general_2.name, info])

    def send_general_3(self):
        info = {
            "Bat2EstimatedFCC": 90,
            "Bat2EstRemainingCap": 80,
            "Bat2UserFCC": 90,
            "Bat2UserRemainingCap": 80
        }
        data = self.proto_general_3.encode(info)
        self.send_can_msg(self.proto_general_3, data)
        self.tx_signal.emit([datetime.datetime.now(), self.proto_general_3.name, info])

    def send_general_4(self):
        info = {
            "Bat2EstimatedSOC": self.soc,
            "Bat2UserSOC": self.soc,
        }
        data = self.proto_general_4.encode(info)
        self.send_can_msg(self.proto_general_4, data)
        self.tx_signal.emit([datetime.datetime.now(), self.proto_general_4.name, info])

    def send_temps_1(self):
        info = {
            "Bat2Temp_01": self.temperature_min,
            "Bat2Temp_02": self.temperature_max,
            "Bat2Temp_03": self.temperature_min,
            "Bat2Temp_04": self.temperature_max,
        }
        data = self.proto_temps_1.encode(info)
        self.send_can_msg(self.proto_temps_1, data)
        self.tx_signal.emit([datetime.datetime.now(), self.proto_temps_1.name, info])

    def send_temps_2(self):
        info = {
            "Bat2Temp_05": self.temperature_min,
        }
        data = self.proto_temps_2.encode(info)
        self.send_can_msg(self.proto_temps_2, data)
        self.tx_signal.emit([datetime.datetime.now(), self.proto_temps_2.name, info])

    def send_serial_1(self):
        info = {
            "Bat2SN_CH01": 1,
            "Bat2SN_CH02": 2,
            "Bat2SN_CH03": 3,
            "Bat2SN_CH04": 4,
            "Bat2SN_CH05": 5,
            "Bat2SN_CH06": 6,
            "Bat2SN_CH07": 7,
            "Bat2SN_CH08": 8,
        }
        data = self.proto_serial_1.encode(info)
        self.send_can_msg(self.proto_serial_1, data)
        self.tx_signal.emit([datetime.datetime.now(), self.proto_serial_1.name, info])

    def send_serial_2(self):
        info = {
            "Bat2SN_CH09": 9,
            "Bat2SN_CH10": 10,
            "Bat2SN_CH11": 11,
            "Bat2SN_CH12": 12,
            "Bat2SN_CH13": 13,
            "Bat2SN_CHK": 91,
        }
        data = self.proto_serial_2.encode(info)
        self.send_can_msg(self.proto_serial_2, data)
        self.tx_signal.emit([datetime.datetime.now(), self.proto_serial_2.name, info])

    def send_status_1(self):
        error_dict = {signal.name: self.errors[signal.name] for signal in self.proto_status_1.signals}
        data = self.proto_status_1.encode(error_dict)
        self.send_can_msg(self.proto_status_1, data)
        self.tx_signal.emit([datetime.datetime.now(), self.proto_status_1.name,
                             {key: value for key, value in error_dict.items() if value is True}])

    def send_status_2(self):
        error_dict = {signal.name: self.errors[signal.name] for signal in self.proto_status_2.signals}
        data = self.proto_status_2.encode(error_dict)
        self.send_can_msg(self.proto_status_2, data)
        self.tx_signal.emit([datetime.datetime.now(), self.proto_status_2.name,
                             {key: value for key, value in error_dict.items() if value is True}])
