from abc import abstractmethod
import threading
import can
from PyQt5 import QtCore
import logging

logger = logging.getLogger(__name__)

class Battery(can.Listener, QtCore.QObject):
    rx_signal = QtCore.pyqtSignal(list)
    tx_signal = QtCore.pyqtSignal(list)
    def __init__(self, can_bus, run_period=None):
        super(Battery, self).__init__()
        self.can_bus = can_bus
        self.run_period = run_period
        self.initialized = False
        self.battery_active = False

        # battery specific
        self.serialnbr = 0x123456789ABCDEF0
        self.soc = 20
        self.soh = 100
        self.voltage = 48000
        self.current = 0
        self.temperature_min = 20
        self.temperature_max = 21
        self.cycles = 123

        # battery specific error options are being set by child class,
        # key entries are the names of the errors as named in the DBC, values if the error is active
        self.errors = dict()  # dict[str, bool]

        self.rx_listeners = []
        self.tx_listeners = []

        if self.run_period is not None:
            self.init_runmethod()

    def __reset_bat_errors__(self):
        error_names = list(self.errors.keys())
        self.errors = {error: False for error in error_names}

    def init_runmethod(self):
        threading.Timer(self.run_period * 0.92, self.init_runmethod).start()
        self.run()

    def on_message_received(self, msg):
        try:
            for listener in self.rx_listeners:
                listener(msg)
        except Exception as e:
            logger.exception(e)

        self.process_rx_msg(msg)

    def send_can_msg(self, prototype, data, or_arbitration_id=0):
        try:
            msg = can.Message(
                is_extended_id=prototype.is_extended_frame,
                arbitration_id=prototype.frame_id,
                data=data
            )
            msg.arbitration_id |= or_arbitration_id

            for listener in self.tx_listeners:
                listener(msg)
            self.can_bus.send(msg)
        except Exception as e:
            logger.exception(e)

    def add_tx_listener(self, listener):
        self.tx_listeners.append(listener)

    def add_rx_listener(self, listener):
        self.rx_listeners.append(listener)

    @abstractmethod
    def run(self):
        raise NotImplementedError("Must override run method")

    @abstractmethod
    def process_rx_msg(self, msg: can.Message):
        raise NotImplementedError("Must override process_rxmsg method")
