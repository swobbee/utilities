import isotp
import time
import os
import can
import argparse
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


parser = argparse.ArgumentParser(description='Update multiple CMCUs in a row')
parser.add_argument("-B", '--BAUDRATE', type=int, help='Can Baudrate')
parser.add_argument("-I", '--CMCU_ID', type=int, help='Node ID of CMCU')
parser.add_argument("-N", '--CMCUs', type=int, help='Amount of CMCUs to be updated. Autogenerates Node IDs')
parser.add_argument("-F", '--Filepath', type=str, help='Path to .hex file')



bus = None
stack = None
cmd = 'sudo ifconfig can0 txqueuelen 1000'
os.system(cmd)

# try:
#     args = parser.parse_args()
#     if args.BAUDRATE:
#         BAUDRATE = args.BAUDRATE
#     if args.CMCU_ID:
#         CMCU_ID = args.CMCU_ID
#     if args.Filepath:
#         FILEPATH = args.Filepath
#     if args.CMCUs:
#         CMCUs = args.CMCUs
#     if len(args._get_args()) == 0:
#         CMCU_ID = 255
#         BAUDRATE = 250000
#         CMCUs = None
# except:
CMCU_ID = 1
BAUDRATE = 250000
CMCUs = None
FILEPATH = r"/home/pgoldmann/Dokumente/software_versions/CMCU/FWV2_11_15.hex"
max_errors = 5
error_counter = 0


def my_error_handler(error):
    global stack, bus, error_counter, max_errors
    error_counter += 1
    logger.warning('IsoTp error happened : %s - %s' % (error.__class__.__name__, str(error)))
    if error_counter >= max_errors:
        logger.error("max ammount of error exceeded. stopping update")


def init():
    global bus, stack

    # check if running windows or linux and initialize bus
    if os.name == "nt":
        bus = can.Bus(None, bustype="pcan", bitrate=BAUDRATE)
    else:
        from can.interfaces.socketcan import SocketcanBus
        bus = SocketcanBus(channel='can0')

    # init isotp library
    addr = isotp.Address(isotp.AddressingMode.Normal_29bits, rxid=_join_arbitration_id(True, CMCU_ID, 0xFF),
                         txid=_join_arbitration_id(False, CMCU_ID, 0xFF))
    stack = isotp.CanStack(bus, address=addr,error_handler=my_error_handler)


def reboot_to_bootloader():
    bus.send(can.Message(arbitration_id=_join_arbitration_id(False, CMCU_ID, 0x00), data=bytearray(b'\x00\x0a')))


def reboot_to_mainapp():
    bus.send(can.Message(arbitration_id=_join_arbitration_id(False, CMCU_ID, 0x00), data=bytearray(b'\x00\x09')))

def restart_cmcu():
    bus.send(can.Message(arbitration_id=_join_arbitration_id(False, CMCU_ID, 0x00), data=bytearray(b'\x00\x01')))


def send_firmware(filepath):
    global error_counter, max_errors
    result = False
    success = False
    num_lines = sum(1 for line in open(filepath))
    with open(filepath) as hexfile:
        for num, line in enumerate(hexfile):
            send_data(line)
            progress = int(((num+1)/num_lines)*100)
            if progress % 20 == 0:
                logger.info(f"{progress} %")
            if error_counter >= max_errors:
                result = True
                break

    # all transfered, wait for response
    while not result:
        msg = bus.recv()
        cmcu_id, message_id = _split_arbitration_id(msg.arbitration_id)
        if cmcu_id == CMCU_ID and message_id == 0xFE:
            success = True
            result = True

    if success and msg.data[0] == 0:
        logger.info(f"Transmission succeeded, CMCU with node id {CMCU_ID} is now copying the program to flash memory and will then reboot to main app")
    else:
        logger.warning(f"Transmission failed, CMCU with node id {CMCU_ID} is remaining in bootloader mode. Please check the hex file and connection.")

    return success


def send_data(hexline):
    stack.send(bytes.fromhex(hexline.strip()[1:]))
    while stack.transmitting():
        stack.process()


def cleanup():
    bus.shutdown()


def _join_arbitration_id(is_from_cmcu, device_id, message_id):
    arbitration_id = (device_id << 8) | message_id
    if is_from_cmcu:
        arbitration_id |= 0x1FF80000
    return arbitration_id


def _split_arbitration_id(arbitration_id):
    device_id = (arbitration_id >> 8) & 0xFF
    message_id = arbitration_id & 0xFF
    return device_id, message_id


def update(path_to_hex=None, ID=255):
    global CMCU_ID
    CMCU_ID = ID
    fp = FILEPATH if not path_to_hex else path_to_hex
    init()
    reboot_to_bootloader()
    time.sleep(1)
    success = send_firmware(fp)
    cleanup()
    return success

if __name__ == "__main__":
    if CMCUs:
        for id in range(CMCUs):
            id+=1
            CMCU_ID = id
            update()
    else:
        update()

