import isotp
import time
import os
import can
import argparse


parser = argparse.ArgumentParser(description='Update multiple CMCUs in a row')
parser.add_argument("-B", '--BAUDRATE', type=int, help='Can Baudrate')
parser.add_argument("-I", '--CMCU_ID', type=int, help='Node ID of CMCU')
parser.add_argument("-N", '--CMCUs', type=int, help='Amount of CMCUs to be updated. Autogenerates Node IDs')
parser.add_argument("-F", '--Filepath', type=str, help='Path to .hex file')



bus = None
stack = None
cmd = 'sudo ifconfig can0 txqueuelen 1000'
os.system(cmd)

args = parser.parse_args()
if args.BAUDRATE:
    BAUDRATE = args.BAUDRATE
if args.CMCU_ID:
    CMCU_ID = args.CMCU_ID
if args.Filepath:
    FILEPATH = args.Filepath
if args.CMCUs:
    CMCUs = args.CMCUs
if len(args._get_args()) == 0:
    CMCU_ID = 1
    BAUDRATE = 250000
    FILEPATH = "/home/pgoldmann/Dokumente/software_versions/CMCU/2_9_15.hex"
    CMCUs = None

signal_run = None   # used for ProductionTool


def init():
    global bus, stack

    # check if running windows or linux and initialize bus
    if os.name == "nt":
        bus = can.Bus(None, bustype="pcan", bitrate=BAUDRATE)
    else:
        from can.interfaces.socketcan import SocketcanBus
        bus = SocketcanBus(channel='can0')

    # init isotp library
    init_stack()


def init_stack():
    global stack, bus    # init isotp library
    addr = isotp.Address(isotp.AddressingMode.Normal_29bits, rxid=_join_arbitration_id(True, CMCU_ID, 0xFF),
                         txid=_join_arbitration_id(False, CMCU_ID, 0xFF))
    stack = isotp.CanStack(bus, address=addr)

def reboot_to_bootloader():
    bus.send(can.Message(arbitration_id=_join_arbitration_id(False, CMCU_ID, 0x00), data=bytearray(b'\x00\x0a')))


def reboot_to_mainapp():
    bus.send(can.Message(arbitration_id=_join_arbitration_id(False, CMCU_ID, 0x00), data=bytearray(b'\x00\x09')))

def restart_cmcu():
    bus.send(can.Message(arbitration_id=_join_arbitration_id(False, CMCU_ID, 0x00), data=bytearray(b'\x00\x01')))

def send_firmware(filepath):
    num_lines = sum(1 for line in open(filepath))
    with open(filepath) as hexfile:
        for num, line in enumerate(hexfile):
            send_data(line)
            progress = round(float(f'{((num+1)/num_lines)*100}'), 2)
            if signal_run:
                signal_run.emit(progress)  # emit progress in %
            else:
                print(progress)

    # all transfered, wait for response
    result = False
    while not result:
        msg = bus.recv()
        cmcu_id, message_id = _split_arbitration_id(msg.arbitration_id)
        if cmcu_id == CMCU_ID and message_id == 0xFE:
            result = True

    if msg.data[0] == 0:
        print("Transmission succeeded, CMCU is now copying the program to flash memory and will then reboot to main app")
    else:
        print("Transmission failed, system is remaining in bootloader mode. Please check the hex file.")


def send_data(hexline):
    stack.send(bytes.fromhex(hexline.strip()[1:]))
    while stack.transmitting():
        stack.process()


def cleanup():
    bus.shutdown()


def _join_arbitration_id(is_from_cmcu, device_id, message_id):
    arbitration_id = (device_id << 8) | message_id
    if is_from_cmcu:
        arbitration_id |= 0x1FF80000
    return arbitration_id


def _split_arbitration_id(arbitration_id):
    device_id = (arbitration_id >> 8) & 0xFF
    message_id = arbitration_id & 0xFF
    return device_id, message_id


def runUpdate(filepath, can_bus, ID = 255, signal_running=None):
    global bus, signal_run, CMCU_ID
    CMCU_ID = ID
    bus = can_bus
    signal_run = signal_running
    init_stack()
    reboot_to_bootloader()
    time.sleep(1)
    send_firmware(filepath)
    reboot_to_mainapp()
    time.sleep(1)


def update():
    init()
    reboot_to_bootloader()
    time.sleep(1)
    result = send_firmware(FILEPATH)
    cleanup()
    return result

# def getCmcuCanIds():
#
#     msg = bus.recv()
#     cmcu_id, message_id = _split_arbitration_id(msg.arbitration_id)
#
#     if message_id == 0x02:
#
#     if 'Serial' in key and 'Bat' not in key:
#         # check for cmcu serian number
#         if value not in SDict.keys():
#             SDict[value] = device_id
#             print(f'Received Serialnbr {value} from CMCU_ID {device_id} | {hex(message.arbitration_id)}')
#             CM_amount = len(SDict)
#             print(f'amount of CMs: {CM_amount}')
#         elif SDict[value] != device_id:
#             oldID = SDict[value]
#             SDict[value] = device_id
#             print(f'ID of Serialnbr {value} changed from {oldID} to {device_id} | {hex(message.arbitration_id)}')
#


if __name__ == "__main__":
    if CMCUs:
        for id in range(CMCUs):
            id+=1
            CMCU_ID = id
            update()
    else:
        update()
