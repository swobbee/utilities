import can
import os
import pathlib
import subprocess
import logging
from logging.handlers import TimedRotatingFileHandler
import time

cache = dict()


def check_can_bus_state(can_interface):
    can_interface = str(can_interface)
    cmd = "ip -d link show " + can_interface + " | grep 'state DOWN'"
    p = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=False)
    if p.returncode == 0:
        logger.warning("Can bus down")


def get_node_id(arbitration_id):
    return (arbitration_id >> 8) & 0xFF


def is_from_ipc(arbitration_id):
    node_id = get_node_id(arbitration_id)
    return arbitration_id & 0x1FF80000 == 0 and (node_id < 6 or node_id == 0xFF) and arbitration_id & 0xFF < 0x14


def on_receive_message(msg, logger):
    node_id = get_node_id(msg.arbitration_id)
    if msg.is_extended_id and (node_id <= 4 or node_id == 0xFF):
        if is_from_ipc(msg.arbitration_id) or msg.arbitration_id not in cache or cache[msg.arbitration_id] != msg.data:
            data = msg.data.hex()
            logger.info(f"{hex(msg.arbitration_id)}   [{' '.join(a + b for a, b in zip(data[::2], data[1::2]))}]")
            cache[msg.arbitration_id] = msg.data
        cache[msg.arbitration_id] = msg.data


if __name__ == "__main__":
    logging_dir = f"{os.path.expanduser('~')}/.cache/canlogger"
    pathlib.Path(logging_dir).mkdir(parents=True, exist_ok=True)    # creates directory if it doesn't exist

    logger = logging.getLogger("rotating_can_logger")
    handler = TimedRotatingFileHandler(f"{logging_dir}/canlog.log", when="H", interval=1, backupCount=24)
    formatter = logging.Formatter("[%(asctime)s.%(msecs)05d]  %(message)s", datefmt="%H:%M:%S")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    logger.setLevel(logging.INFO)

    network = can.Bus(interface='socketcan', channel='can0', receive_own_messages=True)

    can.Notifier(network, [lambda msg: on_receive_message(msg, logger)])

    while True:
        try:
            check_can_bus_state(0)
            time.sleep(1)
        except Exception as e:
            logger.log(e)

