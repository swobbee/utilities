from dataclasses import dataclass
import os
import jsonpickle
from config_handling import config_handling
from config_handling.json_objects import ConfigInt
from config_handling.json_objects import ConfigValueStr
from config_handling.json_objects import ConfigValueDict

DEFAULT_CONFIG_NAME = os.path.join('cmcu_debugger_init.json')


@dataclass()
class ConfigCMCU(config_handling.ConfigHandling):

    bus_id_config: ConfigInt
    node_id_dict_config: ConfigValueDict
    baud_rate_bps_config: ConfigInt
    CMCU_error_template_config: ConfigValueDict


    aaa_guide: str

    @property
    def bus_id(self):
        return self.bus_id_config.value

    @property
    def node_id_dict(self):
        return self.node_id_dict_config.value

    @property
    def baud_rate_bps(self):
        return self.baud_rate_bps_config.value

    @property
    def cmcu_error_template(self):
        return self.CMCU_error_template_config.value



def read_in(CONFIG_FILE_PATH=DEFAULT_CONFIG_NAME):
    with open(CONFIG_FILE_PATH) as file_link:
        json_str = file_link.read()
        jsonpickle.set_decoder_options('simplejson')
        jconfig = jsonpickle.decode(json_str)
    return jconfig
