
import os
import sys
import pathlib
import can

import subprocess
import cantools

script_dir = str(pathlib.Path(__file__).parent.resolve())
os.chdir(script_dir)

import loggerinterface

logger = loggerinterface.get_logger_by_name("can_debugger")
config_path = os.path.join(script_dir, "/config")


DBC_PATH = f'D:/git/interface_definitions/cmcu_ipc.dbc'
if os.path.isfile(DBC_PATH):
    logger.info(f'found EE_IPC_CMCU.dbc folder at {script_dir}"')
else:
    logger.warning(f'did not find EE_IPC_CMCU.dbc in folder at {script_dir}')


bus_id = 0
baud_rate_bps = 250000


BITPOS_IS_HIGH_PRIO = 28
BITPOS_IS_FROM_CMCU = 24
BITPOS_SLOT_NUM = 16
BITPOS_DEVICE_ID = 8
BITPOS_MESSAGE_ID = 0

def decode_message(message_id, data):
    global remote_node
    return remote_node.decode_message(
        message_id | (1 << BITPOS_IS_FROM_CMCU) | (1 << BITPOS_IS_HIGH_PRIO), data)

def print_error(e):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    logger.error(f'{e} error of type {exc_type} in {fname} line {exc_tb.tb_lineno}')


def __split_arbitration_id__(arbitration_id):
    is_high_prio = not ((arbitration_id >> BITPOS_IS_HIGH_PRIO) & 0x01)
    is_from_cmcu = (arbitration_id >> BITPOS_IS_FROM_CMCU) & 0x01
    slot_num = (arbitration_id >> BITPOS_SLOT_NUM) & 0x0F
    device_id = (arbitration_id >> BITPOS_DEVICE_ID) & 0xFF
    message_id = (arbitration_id >> BITPOS_MESSAGE_ID) & 0xFF
    return slot_num, device_id, message_id

def check_can_bus_state(can_interface):
    can_interface = str(can_interface)
    cmd = "ip -d link show " + can_interface + " | grep 'state DOWN'"
    p = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=False)
    if p.returncode == 0:
        logger.warning("Can bus down")


def __on_receiving_message__(message):
    try:
        slot_num, node_id, message_id = __split_arbitration_id__(message.arbitration_id)
        try:
            raw_data = decode_message(message_id | (1 << 24) | (1 << 28), message.data)
            for key in raw_data.keys():
                if "ERROR_" in key:
                    if raw_data[key]:
                        logger.error(f'{key}')
                elif "CMCU_State" in key:
                        if raw_data[key] == "ERROR":
                            logger.warning(f'{raw_data}')
                        else:
                            logger.info(f'{raw_data}')
                elif "Bat" in key:
                    logger.info(f'{raw_data}')
                    break
        except:
            logger.info(f'{node_id} {message_id}, {message}')
    except Exception as e:
        print_error(e)




if __name__ == "__main__":
    # create a bus instance
    # many other interfaces are supported as well (see below)

    # remote_node = cantools.database.load_file(DBC_PATH)
    # network = can.Bus(interface='socketcan',
    #              channel='can0',
    #              receive_own_messages=True)
    # can.Notifier(network, [__on_receiving_message__])
    #
    #
    #
    # while True:
    #    try:
    #         check_can_bus_state(bus_id)
    #    except Exception as e:
    #      print_error(e)
    a = 3






#
# cmcu_dev = cmcu_comm(
#             full_path_dbc_file=DBC_PATH,
#             can_bus=bus_id,
#             can_id=bus_id,
#             bitrate=baud_rate_bps,
#             instance_name='name')
# cmcu_dev.add_to_can_bus()
# cmcu_dev.add_listeners(__on_receiving_message__)
# logger.info(f"start listening to can bus with id {bus_id}, baudrate {baud_rate_bps}")
#
# try:
#     while True:
#         pass
#         # if not os.popen("ip addr ls dev can0").read():
#         #     logger.error("Can bus is down")
#         # else:
#         #     logger.info("can up")
#         # time.sleep(1)
# except Exception as e:
#     print_error(e)