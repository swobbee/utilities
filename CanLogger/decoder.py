import cantools
import can
import re

db = cantools.database.load_file(f"D:/git/interface_definitions/cmcu_ipc.dbc")

s = "can0  0C700100   [8]  18 02 0C 04 00 07 04 02"
s = re.sub(' +', ' ', s)
a = s.split(" ")

msgid = int(a[1], 16)
data = a[3:]
data = bytes([int(x, 16) for x in data])

print(db.decode_message(msgid & 0xFF | 0x1FF80000, data))
