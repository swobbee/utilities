import os
ipcs = os.popen("sudo arp-scan --localnet | grep Raspberry").read()
ipcs = ipcs.split("\n")
print(ipcs)

for ipc in ipcs:
    try:
        ip, mac, _ = ipc.split('\t')
        ns = os.popen(f"nslookup {ip}").read()
        ns = ns.split("\n")
        for line in ns:
            contents = line.split("\t")
            for content in contents:
                if "name" in content:
                    _string = content.split()
                    name = _string[2].split(".")[0]
                    print(f"Address: {ip} \t{name}")
    except:
        pass
