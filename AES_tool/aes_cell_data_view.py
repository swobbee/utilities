from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QMainWindow, QAction, qApp, QApplication, QWidget, QGridLayout, QGroupBox, QPushButton, \
    QVBoxLayout, QHBoxLayout, QLabel
from PyQt5.QtGui import QIcon, QFont, QPixmap
from PyQt5 import uic
from PyQt5.QtCore import Qt
import os
from collections import namedtuple
import time
import can
import traceback
import sys

class CelldataWindow(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(CelldataWindow, self).__init__(*args, **kwargs)
        uic.loadUi(r"aes_cell_data.ui", self)
        self.numberBatteries = 4
        self.amount_cells = 16
        self.init_cell_infos(self.numberBatteries)
        self.setWindowTitle("Cell Data Window")



    def show_cell_window(self):
        self.show()

    def close_cell_window(self):
        # self.close_window()
        self.close()

    def display_bat(self, numberbat):
        for i in range(4):
            group = getattr(self, f'gp_battery{i+1}')
            group.setHidden(True)

        for i in range(numberbat):
            group = getattr(self, f'gp_battery{i+1}')
            group.setHidden(False)


        self.setFixedSize(self.sizeHint())


    def init_cell_infos(self, numberBatteries):
        CellView = namedtuple('CellView',
                                 'cell_up volt1 temp1 cell_down volt2 temp2')
        self.battery_dict = {}
        for bat_nbr in range(1,numberBatteries+1):
            setattr(self, f'batt_{bat_nbr}', [])
            cell_list = getattr(self, f'batt_{bat_nbr}')
            self.battery_dict[f'batt_{bat_nbr}'] = cell_list
            grid_cell_data = getattr(self, f'grid_layout_bat{bat_nbr}')

            for cell_nbr in range(0, self.amount_cells):
                if cell_nbr == 0:
                    cell_up = getattr(self, f'batt{bat_nbr}_cell1')
                    cell_down = getattr(self, f'batt{bat_nbr}_cell9')
                    volt1 = getattr(self, f'bat{bat_nbr}_volt1')
                    volt2 = getattr(self, f'bat{bat_nbr}_volt2')
                    temp1 = getattr(self, f'bat{bat_nbr}_temp1')
                    temp2 = getattr(self, f'bat{bat_nbr}_temp2')


                    cell = CellView(cell_up, volt1, temp1, cell_down, volt2, temp2)
                else:
                    cell = CellView(QtWidgets.QLabel(), QtWidgets.QLineEdit(), QtWidgets.QLineEdit(),
                                    QtWidgets.QLabel(), QtWidgets.QLineEdit(), QtWidgets.QLineEdit())

                    # copy initial content
                    cell.cell_up.setText(
                        f'{cell_list[0].cell_up.text().split()[0]} {cell_nbr+1}')
                    cell.cell_down.setText(
                        f'{cell_list[0].cell_up.text().split()[0]} {cell_nbr+1}')
                    cell.volt1.setText(cell_list[0].volt1.text())
                    cell.temp1.setText(cell_list[0].temp1.text())
                    cell.volt2.setText(cell_list[0].volt2.text())
                    cell.temp2.setText(cell_list[0].temp2.text())

                cell_list.append(cell)

                cell_list[cell_nbr].cell_up.setAlignment(Qt.AlignCenter)
                cell_list[cell_nbr].cell_down.setAlignment(Qt.AlignCenter)
                cell_list[cell_nbr].volt1.setAlignment(Qt.AlignRight)
                cell_list[cell_nbr].volt2.setAlignment(Qt.AlignRight)
                cell_list[cell_nbr].temp1.setAlignment(Qt.AlignRight)
                cell_list[cell_nbr].temp2.setAlignment(Qt.AlignRight)


                if (cell_nbr < 8):
                    grid_cell_data.addWidget(cell_list[cell_nbr].cell_up, 0, cell_nbr+1)
                    grid_cell_data.addWidget(cell_list[cell_nbr].volt1, 1, cell_nbr+1)
                    grid_cell_data.addWidget(cell_list[cell_nbr].temp1, 2, cell_nbr+1)
                else:
                    grid_cell_data.addWidget(cell_list[cell_nbr].cell_down, 3, cell_nbr-8+1)
                    grid_cell_data.addWidget(cell_list[cell_nbr].volt2, 4, cell_nbr-8+1)
                    grid_cell_data.addWidget(cell_list[cell_nbr].temp2, 5, cell_nbr-8+1)

if __name__ == '__main__':
	import os
	path = os.path.split(__file__)[0]
	if path:
		os.chdir(path)
	app = QtWidgets.QApplication(sys.argv)

	mainwindow = CelldataWindow()
	app.exec()