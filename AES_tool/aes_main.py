from PyQt5 import QtWidgets
from pathlib import Path
import sys
import os
from aes_view import MainWindow
from aes_controller import Controller
import threading
import argparse

parser = argparse.ArgumentParser(description='Simulate the CMCU side of the AES Battery communication')
parser.add_argument("-F", '--Filepath', type=str, help='Path to .dbc file')

args = parser.parse_args()

if args.Filepath:
	FILEPATH = args.Filepath

elif os.path.isfile(os.path.join(os.path.dirname(os.getcwd()), "doc", "interface_definitions", "swobbee.dbc")):
	FILEPATH = os.path.join(os.path.dirname(os.getcwd()), "doc", "interface_definitions", "swobbee.dbc")
else:
	dbc_name = "swobbee.dbc"
	# determine if application is a script file or frozen exe
	if getattr(sys, 'frozen', False):
		application_path = os.path.dirname(sys.executable)
	elif __file__:
		application_path = os.path.dirname(__file__)

	FILEPATH = os.path.join(application_path, dbc_name)



if __name__ == '__main__':
	import os
	path = os.path.split(__file__)[0]
	if path:
		os.chdir(path)

	app = QtWidgets.QApplication(sys.argv)
	app.setStyle('Breeze')
	mainwindow = MainWindow()
	controller = Controller(FILEPATH)

	controller.setView(mainwindow)

	app.exec()