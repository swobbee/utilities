from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QMainWindow, QAction, qApp, QApplication, QWidget, QGridLayout, QGroupBox, QPushButton, \
    QVBoxLayout, QHBoxLayout, QLabel
from PyQt5.QtGui import QIcon, QFont, QPixmap
from PyQt5 import uic
from PyQt5.QtCore import Qt
from aes_cell_data_view import CelldataWindow
import os
from collections import namedtuple
import time
import can
import traceback
import sys



class MainWindow(QMainWindow):
    signal_command_set_id = QtCore.pyqtSignal(int)
    signal_command_set_state = QtCore.pyqtSignal(int, str)
    signal_command_send_msg = QtCore.pyqtSignal(int, str)
    signal_command_set_protocol = QtCore.pyqtSignal(int, int)
    # signal_send_IPC_Charge_Parameters = QtCore.pyqtSignal(int, int, int)
    # signal_send_IPC_Config = QtCore.pyqtSignal(str, str)
    # signal_send_IPC_Set_CAN_ID = QtCore.pyqtSignal(int)
    # signal_change_CAN_BaudRate = QtCore.pyqtSignal(int)
    # signal_req_serial_cmcu = QtCore.pyqtSignal(bool)
    # signal_req_serial_if = QtCore.pyqtSignal(bool)
    # signal_restart_listener = QtCore.pyqtSignal(int)

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        uic.loadUi("aes_mainwindow.ui", self)
        self.resize(1000, 400)
        self.numSlots = self.sp_numbat.value()
        self.show()
        self.battery_slots = []
        self.init_battery_slots(4)
        self.cell_data_info = CelldataWindow()
        self.setWindowTitle("AES Controller")

        ### connect widgets
        self.cb_celldetails.stateChanged.connect(self.show_celldata)
        self.display_bats_cell_data()
        self.sp_numbat.valueChanged.connect(self.show_batteries)
        self.sp_numbat.valueChanged.connect(self.display_bats_cell_data)
        self.pb_setNodeId.clicked.connect(self.setId_event)
        self.pb_setState.clicked.connect(self.setState_event)
        self.pb_setProtocol.clicked.connect(self.setProtocol_event)
        self.pb_send.clicked.connect(self.sendMessage_event)
        self.cb_msgs.currentIndexChanged.connect(self.add_to_text_edit)
        self.pb_clear.clicked.connect(self.clear_text_edit)
        self.clear_text_edit()


    def add_to_text_edit(self):
        msg_name = self.cb_msgs.currentText()
        text = self.te_msglist.toPlainText()
        self.pb_send.setEnabled(True)
        if (text != '') and (msg_name not in text):
            msg_name = f'{text}'+'\n'+f'{msg_name}'
            self.te_msglist.setPlainText(msg_name)
        elif msg_name in text:
            msg_name = text

        self.te_msglist.setPlainText(msg_name)

    def clear_text_edit(self):
        self.te_msglist.clear()
        self.pb_send.setEnabled(False)

    def update_data(self, signal_dict):
        try:
            device_id = signal_dict['device_id']
            if device_id > 0:
                raw_data = signal_dict['raw_data']
                msg_name = signal_dict['msg_name']
                bat_nr = device_id - 1
                slot = self.battery_slots[bat_nr]

                if msg_name.find('Serial_Number') == 0:
                    type = str(raw_data.get("Type"))
                    year = str(raw_data.get("Year"))
                    week_number = str(raw_data.get("Week_Number"))
                    incremental_number = str(raw_data.get("Incremental_Number"))

                    serial = type+year+week_number+incremental_number
                    if serial == "":
                        serial = "None"

                    slot.serial.setText(str(serial))

                if msg_name.find('Protocol') == 0:
                    protocol = raw_data.get("Protocol")
                    slot.protocol.setText(str(protocol))

                if msg_name.find('State') == 0:
                    pack_voltage = raw_data.get("Pack_Voltage")
                    connector_voltage = raw_data.get("Connector_Voltage")
                    soc = raw_data.get("SOC")
                    current = raw_data.get("Current")
                    temp_min = raw_data.get("Temperature_Cell_Min")
                    temp_max = raw_data.get("Temperature_Cell_Max")
                    discharge = raw_data.get("Discharge")
                    charge = raw_data.get("Charge")
                    error = raw_data.get("Error")

                    if discharge == 1:
                        state = "Discharge"
                    elif charge == 1:
                        state = "Charge"
                    elif error == 1:
                        state = "Error"
                    else:
                        state = "None"

                    slot.state.setText(state)
                    slot.soc.setValue(soc)
                    slot.pack_voltage.setText(str(pack_voltage) + ' V')
                    slot.connector_voltage.setText(str(connector_voltage) + ' V')
                    slot.current.setText(str(current) + ' A')
                    slot.temp_cell_max.setText(str(temp_max) + ' °C')
                    slot.temp_cell_min.setText(str(temp_min) + ' °C')

                if msg_name.find("Cell_Voltages_1") == 0:
                    for i in range(1, 9):
                        volt = raw_data.get(f"Cell_Voltage_0{i}")
                        self.cell_data_info.battery_dict[f'batt_{device_id}'][i-1].volt1.setText(str(volt))

                if msg_name.find("Temperatures_1") == 0:
                    for i in range(1, 9):
                        temp = raw_data.get(f"Temperature_0{i}")
                        self.cell_data_info.battery_dict[f'batt_{device_id}'][i-1].temp1.setText(str(temp))

                if msg_name.find("Cell_Voltages_2") == 0:
                    volt = raw_data.get(f"Cell_Voltage_09")
                    self.cell_data_info.battery_dict[f'batt_{device_id}'][8].volt2.setText(str(volt))
                    for i in range(10,17):
                        volt = raw_data.get(f"Cell_Voltage_{i}")
                        self.cell_data_info.battery_dict[f'batt_{device_id}'][i-1].volt2.setText(str(volt))

                if msg_name.find("Temperatures_2") == 0:
                    temp = raw_data.get(f"Temperature_09")
                    self.cell_data_info.battery_dict[f'batt_{device_id}'][8].temp2.setText(str(temp))
                    for i in range(10,17):
                        temp = raw_data.get(f"Temperature_{i}")
                        self.cell_data_info.battery_dict[f'batt_{device_id}'][i-1].temp2.setText(str(temp))

                if msg_name.find('Info') == 0:
                    soh = raw_data.get("SOH")
                    fwv = raw_data.get("Firmware_Version")
                    charge_cycles = raw_data.get("Charge_Cycles")

                    slot.soh.setValue(soh)
                    slot.fwv.setText(str(fwv))
                    slot.charge_cycles.setText(str(charge_cycles))

                if msg_name.find("Error") == 0:
                    slot.error.clear()
                    for key, value in raw_data.items():
                        if value != 0:
                            slot.error.addItem(key)

                elif msg_name.find("Warning") == 0:
                    slot.warning.clear()
                    for key, value in raw_data.items():
                        if value != 0:
                            slot.warning.addItem(key)


        except Exception as e:
            traceback.print_exc()

    def update_rx(self, data):
        self.tb_rx.append(data)

    def update_tx(self, data):
        self.tb_tx.append(data)

    def setId_event(self):
        node_id = self.sp_nid.value()
        self.signal_command_set_id.emit(node_id)

    def setState_event(self):
        node_id = self.sp_nid.value()
        state = self.cb_setState.currentText()
        self.signal_command_set_state.emit(node_id, state)

    def setProtocol_event(self):
        node_id = self.sp_nid.value()
        protocol = self.cb_setProtocol.currentIndex()
        self.signal_command_set_protocol.emit(node_id, protocol)

    def sendMessage_event(self):
        node_id = self.sp_nid.value()
        msg = self.te_msglist.toPlainText()
        if msg != '':
            self.signal_command_send_msg.emit(node_id, msg)

    def show_batteries(self):
        self.numSlots = self.sp_numbat.value()
        for battery in self.battery_slots:
            for widget in battery:
                widget.setHidden(True)

        for battery in self.battery_slots[:self.numSlots]:
            for widget in battery:
                widget.setHidden(False)

    def show_celldata(self):
        state = self.sender().isChecked()
        if state:
            self.cell_data_info.show_cell_window()
        else:
            self.cell_data_info.close_cell_window()

    def display_bats_cell_data(self):
        self.cell_data_info.display_bat(self.sp_numbat.value())

    def init_battery_slots(self, numSlots):
        BatteryView = namedtuple('CellView',
                                 'slot state serial fwv protocol soc soh pack_voltage connector_voltage current temp_cell_max temp_cell_min charge_cycles error warning')


        if len(self.battery_slots) == 0:
            self.battery_slot = QtWidgets.QLabel(self)
            self.battery_slot.setText('Slot 1')

            for i in range(numSlots):

                if i == 0:
                    slot = BatteryView(self.battery_slot, self.battery_state, self.battery_serial, self.battery_fwv,
                                       self.battery_protocol, self.battery_soc, self.battery_soh, self.battery_pack_voltage,
                                       self.battery_connector_voltage, self.battery_current, self.battery_temp_cell_max,
                                       self.battery_temp_cell_min, self.battery_charge_cycles, self.battery_error,
                                       self.battery_warning)
                else:
                    slot = BatteryView(QtWidgets.QLabel(self), QtWidgets.QLineEdit(self), QtWidgets.QLineEdit(self),
                                       QtWidgets.QLineEdit(self), QtWidgets.QLineEdit(self),
                                       QtWidgets.QProgressBar(self), QtWidgets.QProgressBar(self), QtWidgets.QLineEdit(self),
                                       QtWidgets.QLineEdit(self), QtWidgets.QLineEdit(self), QtWidgets.QLineEdit(self),
                                       QtWidgets.QLineEdit(self) , QtWidgets.QLineEdit(self),
                                       QtWidgets.QComboBox(self), QtWidgets.QComboBox(self))

                    # # copy style
                    # slot.slot.setStyleSheet(self.battery_slots[0].state.styleSheet())
                    # slot.state.setStyleSheet(self.battery_slots[0].state.styleSheet())
                    # slot.serial.setStyleSheet(self.battery_slots[0].state.styleSheet())
                    # slot.soc.setStyleSheet(self.battery_slots[0].soc.styleSheet())
                    # slot.soh.setStyleSheet(self.battery_slots[0].soh.styleSheet())
                    # slot.voltage.setStyleSheet(self.battery_slots[0].voltage.styleSheet())
                    # slot.current.setStyleSheet(self.battery_slots[0].voltage.styleSheet())
                    # slot.temp_cell_max.setStyleSheet(self.battery_slots[0].voltage.styleSheet())
                    # slot.temp_cell_min.setStyleSheet(self.battery_slots[0].voltage.styleSheet())


                    # copy initial content
                    slot.slot.setText(
                        f'{self.battery_slots[0].slot.text().split()[0]} {int(self.battery_slots[0].slot.text().split()[-1]) + i}')
                    slot.serial.setText(self.battery_slots[0].serial.text())
                    slot.state.setText(self.battery_slots[0].state.text())
                    slot.soc.setValue(self.battery_slots[0].soc.value())
                    slot.soh.setValue(self.battery_slots[0].soh.value())
                    slot.pack_voltage.setText(self.battery_slots[0].pack_voltage.text())
                    slot.connector_voltage.setText(self.battery_slots[0].pack_voltage.text())
                    slot.current.setText(self.battery_slots[0].current.text())
                    slot.temp_cell_max.setText(self.battery_slots[0].temp_cell_max.text())
                    slot.temp_cell_min.setText(self.battery_slots[0].temp_cell_min.text())




                self.battery_slots.append(slot)

                self.battery_slots[i].slot.setAlignment(Qt.AlignCenter)
                self.battery_slots[i].state.setAlignment(Qt.AlignLeft)
                self.battery_slots[i].serial.setAlignment(Qt.AlignRight)
                self.battery_slots[i].fwv.setAlignment(Qt.AlignRight)
                self.battery_slots[i].protocol.setAlignment(Qt.AlignRight)
                self.battery_slots[i].soc.setAlignment(Qt.AlignRight)
                self.battery_slots[i].soh.setAlignment(Qt.AlignRight)
                self.battery_slots[i].pack_voltage.setAlignment(Qt.AlignRight)
                self.battery_slots[i].connector_voltage.setAlignment(Qt.AlignRight)
                self.battery_slots[i].current.setAlignment(Qt.AlignRight)
                self.battery_slots[i].temp_cell_max.setAlignment(Qt.AlignRight)
                self.battery_slots[i].temp_cell_min.setAlignment(Qt.AlignRight)
                self.battery_slots[i].charge_cycles.setAlignment(Qt.AlignRight)
                #self.battery_slots[i].error.setAlignment(Qt.AlignRight)



                self.grid_battery_data.addWidget(self.battery_slots[i].slot, 0, 1 + i)
                self.grid_battery_data.addWidget(self.battery_slots[i].state, 1, 1 + i)
                self.grid_battery_data.addWidget(self.battery_slots[i].serial, 2, 1 + i)
                self.grid_battery_data.addWidget(self.battery_slots[i].fwv, 3, 1 + i)
                self.grid_battery_data.addWidget(self.battery_slots[i].protocol, 4, 1 + i)
                self.grid_battery_data.addWidget(self.battery_slots[i].soc, 5, 1 + i)
                self.grid_battery_data.addWidget(self.battery_slots[i].soh, 6, 1 + i)
                self.grid_battery_data.addWidget(self.battery_slots[i].pack_voltage, 7, 1 + i)
                self.grid_battery_data.addWidget(self.battery_slots[i].connector_voltage, 8, 1 + i)
                self.grid_battery_data.addWidget(self.battery_slots[i].current, 9, 1 + i)
                self.grid_battery_data.addWidget(self.battery_slots[i].temp_cell_max, 10, 1 + i)
                self.grid_battery_data.addWidget(self.battery_slots[i].temp_cell_min, 11, 1 + i)
                self.grid_battery_data.addWidget(self.battery_slots[i].charge_cycles, 12, 1 + i)
                self.grid_battery_data.addWidget(self.battery_slots[i].error, 13, 1 + i)
                self.grid_battery_data.addWidget(self.battery_slots[i].warning, 14, 1 + i)

                self.show_batteries()

        else:
            self.show_batteries()