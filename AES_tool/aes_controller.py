import os
import sys
from PyQt5 import QtCore
from PyQt5.QtCore import *
import can
import cantools
import aes_model as model
import inspect
import traceback

root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../")
sys.path.append(root_dir)
from ProductionTool import can_setup

### helper object to setup CAN RX callback
class ListenerSubclass(can.Listener):
	def __init__(self, on_receive):
		self._on_receive = on_receive
	def on_message_received(self, msg):
		self._on_receive(msg)


class Controller(QObject):
    receivedData = QtCore.pyqtSignal(dict)
    receivedMessage = QtCore.pyqtSignal(str)
    transmittedMessage = QtCore.pyqtSignal(str)
    model = model.Model()

    CMCU_CAN_ID = 255
    current_slot = 0

    def __init__(self, path):
        super().__init__()
        self.dbc_path = path
        ### initialize dbc and create model
        self.dbc = cantools.database.load_file(self.dbc_path)

        for message in self.dbc.messages:
            model_message = model.Message(message)

            for signal in message.signals:
                model_signal = model.Signal(signal, model_message)
                model_message.signals.append(model_signal)

            self.model.messages.append(model_message)

        
        self.can_bus = can_setup.init(bitrate=500000, channel='can0')
        #self.can_bus = can_setup.init(bitrate=500000, channel='virtual')
        self.can_notifier = can.Notifier(self.can_bus, [ListenerSubclass(self.receiveCANMessage)])

    def setView(self, view):
        view.cb_msgs.addItems([msg.name for msg in self.dbc.messages
                               if (msg.name != "Set_State") and (msg.name != "Init")])
        view.cb_setState.addItems([sig.name for sig in self.dbc.get_message_by_name('Set_State').signals])

        view.signal_command_set_id.connect(self.set_id)
        view.signal_command_set_state.connect(self.set_state)
        view.signal_command_set_protocol.connect(self.set_protocol)
        view.signal_command_send_msg.connect(self.send_message)

        self.receivedData.connect(view.update_data)
        self.receivedMessage.connect(view.update_rx)
        self.transmittedMessage.connect(view.update_tx)

    def restartCAN_BUS(self, baudrate):
        if (self.can_bus):
            self.can_bus.reset()
            self.can_bus.shutdown()
            self.can_notifier.stop()
            self.can_bus = can_setup.reinit(bitrate=baudrate)
            self.can_notifier = can.Notifier(self.can_bus, [ListenerSubclass(self.receiveCANMessage)])


    def receiveCANMessage(self, message):
        message_id = None
        self.receivedMessage.emit(str(message))
        try:
            data = {}
            _, device_id, message_id = self.split_arbitration_id(message.arbitration_id)
            raw_data = self.dbc.decode_message(0x1BEE0000 | message_id, message.data)
            msg_name = self.get_message_name(message_id)
            data["device_id"] = device_id
            data["raw_data"] = raw_data
            data["msg_name"] = msg_name
            self.receivedData.emit(data)
            self.receivedMessage.emit(str(message))

        except Exception as excp:
            try:
                msg = self.dbc.get_message_by_frame_id(message_id)
            except:
                msg = None
            error = ["ERROR while trying to process the following received message"]
            error.append(str(message))
            if message_id is None:
                error.append("failed to split arbitration id")
            elif msg:
                error.append("message name: %s" % msg.name)
            else:
                error.append("unknown message id 0x%x" % message_id)
            error.append(str(excp))
            error.append("")

            self.receivedMessage.emit("\n".join(error))

    def split_arbitration_id(self, arbitration_id):
        bee = (arbitration_id >> 13) & 0x7
        device_id = (arbitration_id >> 8) & 0xFF
        message_id = arbitration_id & 0xFF
        return bee, device_id, message_id

    def join_arbitration_id(self, node_id, message_id):
        arbitration_id = (0x1BEE0000 | (node_id << 8) | message_id)
        return arbitration_id

    def get_message_name(self, message_id):
        return self.dbc.get_message_by_frame_id(0x1BEE0000 | message_id).name
    
    def set_id(self, node_id):
        arbitration_id = self.join_arbitration_id(0xff, 0x01)
        message_prototype = self.dbc.get_message_by_frame_id(0x1beeff01)
        data_signals = {}
        data_signals.update({"Node_ID": node_id})
        data = message_prototype.encode(data_signals)
        canMessage = can.Message(arbitration_id=arbitration_id, data=data)
        self.can_bus.send(canMessage)
        self.transmittedMessage.emit(str(canMessage))


    def set_protocol(self, node_id, protocol):
        arbitration_id = self.join_arbitration_id(node_id, 0x04)
        message_prototype = self.dbc.get_message_by_frame_id(0x1BEE0004)
        data_signals = {}
        data_signals.update({"Protocol": protocol})
        data = message_prototype.encode(data_signals)
        canMessage = can.Message(arbitration_id=arbitration_id, data=data)
        self.can_bus.send(canMessage)
        self.transmittedMessage.emit(f'{canMessage} data: {data}')

    def set_state(self, node_id, state):
        discharge, charge = (0,0)
        if state == "Discharge":
            discharge = 1
        else:
            charge = 1
        arbitration_id = self.join_arbitration_id(node_id, 0x02)
        message_prototype = self.dbc.get_message_by_frame_id(0x1BEE0002)
        data_signals = {}
        data_signals.update({"Discharge": discharge})
        data_signals.update({"Charge": charge})
        data = message_prototype.encode(data_signals)
        canMessage = can.Message(arbitration_id=arbitration_id, data=data)
        self.can_bus.send(canMessage)
        self.transmittedMessage.emit(f'{canMessage} data: {data}')

    def send_message(self, node_id, msg_list):
        try:

            arbitration_id = self.join_arbitration_id(node_id, 0x03)
            message_prototype = self.dbc.get_message_by_frame_id(0x1BEE0003)
            data = []
            for msg_name in msg_list.split():
                msg_id = self.dbc.get_message_by_name(msg_name).frame_id
                data.append(msg_id & 0xFF)
            [data.append(0) for i in range(0, 8-len(data))]

            data_signals = {}
            for signal, value in zip(message_prototype.signals, data):
                data_signals.update({signal.name: value})
            data = message_prototype.encode(data_signals)
            canMessage = can.Message(arbitration_id=arbitration_id, data=data)
            self.can_bus.send(canMessage)
            self.transmittedMessage.emit(str(canMessage))
        except Exception as e:
            print(traceback.format_exc())
            print('ERROR SENDING')