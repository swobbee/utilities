import dash
import dash_core_components as dcc
import dash_html_components as html
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly
from collections import namedtuple

from model import Model

cmcu_ipc_dbc = r'D:\git\ipcsim2\doc\InterfaceDefinitions\cmcu_ipc.dbc'
okai_dbc = r'D:\git\ipcsim2\doc\InterfaceDefinitions\bat_okai.dbc'

logfile = r"C:/Users/m.brieske/Desktop/canlog_c1_2021-06-21_11-57-08.log"

Trace = namedtuple("Trace", ["x", "y", "signal_name"])

model = Model()
model.load(okai_dbc, logfile)

error_raw_1_curves = {}
error_raw_2_curves = {}
colors = plotly.colors.DEFAULT_PLOTLY_COLORS

app = dash.Dash(__name__)
app.layout = html.Div(style={'height': '95vh'}, children=[
    dcc.Dropdown(
        id="dfs-dropdown",
        options=[{"label": f"{model.get_message_name(signal.msgid)} - {signal.name}", "value": f"{signal.msgid}.{signal.name}"} for signal in model.get_decoded_signals()],
        multi=True,
        searchable=True
    ),
    dcc.Graph(id="graph", style={'height': 'inherit'}),
    html.Div(id="hidden-div", style={"display": "none"})
])

@app.callback(
    dash.dependencies.Output("graph", "figure"),
    [dash.dependencies.Input("dfs-dropdown", "value")])
def update(signals_selected):
    if not signals_selected:
        return go.Figure()

    traces = {}
    legends = set()
    error_raw_1_curves.clear()
    error_raw_2_curves.clear()

    fig = make_subplots(rows=len(signals_selected), cols=1, shared_xaxes=True)

    for plot_row, signal in enumerate(signals_selected):
        msgid, signal_name = signal.split(".")

        signaldata = model.data[int(msgid)][signal_name]
        for (node, slot), series in signaldata.items():
            x = series.index
            y = series
            traces[(node, slot, plot_row)] = Trace(x, y, signal_name)

    node_slot_tuples = sorted(traces.keys())
    for curve_number, (node, slot, plot_row) in enumerate(node_slot_tuples):
        name = f"CMCU {node} Slot {slot}"
        showlegend = (node, slot) not in legends
        legends.add((node, slot))
        trace = traces[(node, slot, plot_row)]

        if trace.signal_name == "CMCU_Bat_Error_1_Raw":
            error_raw_1_curves[curve_number] = (node, slot)
        elif trace.signal_name == "CMCU_Bat_Error_2_Raw":
            error_raw_2_curves[curve_number] = (node, slot)
        fig.append_trace(go.Scattergl(
                x=trace.x,
                y=trace.y,
                name=name,
                legendgroup=name,
                showlegend=showlegend,
                line={"color": colors[slot % len(colors)],
                      "shape": "hv"}),
            row=plot_row+1,
            col=1)

    fig.update_xaxes(showspikes=True, spikemode="across+marker")
    fig.update_yaxes(rangemode="tozero")
    fig.update_traces(xaxis=f"x{len(signals_selected)}")
    fig.update_layout({"uirevision": True, "clickmode": "event"})
    return fig

@app.callback(
    dash.dependencies.Output('hidden-div', 'children'),
    dash.dependencies.Input('graph', 'clickData'))
def display_click_data(clickData):
    error = 0
    if clickData is not None:
        curve_number = clickData["points"][0]["curveNumber"]
        if curve_number in error_raw_1_curves.keys():
            error = 1
        elif curve_number in error_raw_2_curves.keys():
            error = 2
    if error:
        node, slot = error_raw_1_curves[curve_number]
        batterytype = model.get_battery_type(node, slot, clickData["points"][0]["x"])
        intdata = clickData["points"][0]["y"]
        decoded = model.get_error_from_rawdata(batterytype, intdata, error)
        print(f"CMCU {node} slot {slot} ({batterytype}): {decoded}")
    return None


app.run_server(debug=True)
