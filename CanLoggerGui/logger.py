#!/usr/bin/env python3

import logging
import can
import can_setup
import datetime
import time
import argparse
from os import system
import atexit


def to_logline(timestamp, msg):
    field_strings = [str(timestamp)]
    if msg.is_extended_id:
        arbitration_id_string = "{0:08x}".format(msg.arbitration_id)
    else:
        arbitration_id_string = "{0:04x}".format(msg.arbitration_id)
    field_strings.append(arbitration_id_string.rjust(12, " "))

    field_strings.append("{0:2d}".format(msg.dlc))
    data_strings = []
    if msg.data is not None:
        for index in range(0, min(msg.dlc, len(msg.data))):
            data_strings.append("{0:02x}".format(msg.data[index]))
    if data_strings:  # if not empty
        field_strings.append(" ".join(data_strings).ljust(24, " "))
    else:
        field_strings.append(" " * 24)

    if (msg.data is not None) and (msg.data.isalnum()):
        field_strings.append("'{}'".format(msg.data.decode('utf-8', 'replace')))
    return ",    ".join(field_strings).strip()


class CanLogger(can.Listener):
    def __init__(self, logfile, channel, log_to_console=False):
        self.active = False
        self.init_localtime = None
        self.init_loggertimestamp = 0

        if channel is None:
            channel = ""
        elif channel.isdigit():
            channel = "c%s" % channel
        if channel:
            channel = "_%s" % channel

        if logfile is None:
            # no argument passed, using default value
            logfile = f"canlog{channel}_{datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.log"

        self.logger = logging.getLogger(logfile)
        self.logger.setLevel(logging.INFO)

        fh = logging.FileHandler(logfile)
        fh.setLevel(logging.INFO)
        self.logger.addHandler(fh)

        if log_to_console:
            sh = logging.StreamHandler()
            self.logger.addHandler(sh)

    def on_message_received(self, msg):
        if not self.active:
            return
        if not self.init_localtime:
            self.init_localtime = datetime.datetime.now()
            self.init_loggertimestamp = msg.timestamp
        timestamp = self.init_localtime + datetime.timedelta(seconds=msg.timestamp - self.init_loggertimestamp)

        self.logger.info(to_logline(timestamp, msg))


def exit_handler():
    can_notifier.stop()


if __name__ == "__main__":
    uexit = False

    parser = argparse.ArgumentParser()
    parser.add_argument("--channel", "-c", type=str)
    parser.add_argument("--outfile", "-o", type=str)
    parser.add_argument("--bitrate", "-b", type=int, default=250000)
    args = parser.parse_args()

    system(f"title USB CAN Logger - Logging on channel {args.channel}")

    logger = CanLogger(args.outfile, args.channel, log_to_console=False)

    try:
        can_bus = can_setup.init(bitrate=args.bitrate, channel=args.channel)
    except:
        can_bus = can_setup.init(bitrate=args.bitrate, channel="PCAN_USBBUS" + args.channel)
    can_notifier = can.Notifier(can_bus, [logger])

    print("Flushing bus...")

    # Set logger active after a one second so old messages on the bus get flushed and not logged
    time.sleep(0.5)
    logger.active = True

    atexit.register(exit_handler)
    print(f"Start logging on CAN USB channel {args.channel}, press Ctrl-C to stop and exit")

    while True:
        try:
            input()
        except KeyboardInterrupt:
            break
