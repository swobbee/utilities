import cantools
import pandas as pd
import datetime
from collections import namedtuple


cmcu_ipc_dbc = r'D:\git\ipcsim2\doc\InterfaceDefinitions\cmcu_ipc.dbc'
okai_dbc = r'D:\git\ipcsim2\doc\InterfaceDefinitions\bat_okai.dbc'

Signal = namedtuple("Signal", ["msgid", "name"])

class Model:
    def __init__(self):
        self.is_cmcu_protocol = False
        self.dbc = None
        self.data = {}

    def load(self, dbc, logfile):
        self.dbc = cantools.database.load_file(dbc)
        self.dbc.frame_ids = [message.frame_id for message in self.dbc.messages]
        self.is_cmcu_protocol = self.dbc.nodes[0].name == "CMCU"

        df = pd.read_csv(logfile, sep=",", names=["timestamp", "id", "dlc", "data"], dtype=str, engine="python")
        df["timestamp"] = df["timestamp"].map(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d %H:%M:%S.%f'))
        df["id"] = df["id"].map(lambda x: int.from_bytes(bytearray.fromhex(x), byteorder="big", signed=False))
        df["data"] = df["data"].map(lambda x: bytearray.fromhex(x) if isinstance(x, str) else [])

        # CMCU IPC dbc specific
        if self.is_cmcu_protocol:
            df["node"] = df["id"].map(lambda x: int(x >> 8 & 0xFF))
            df["slot"] = df["id"].map(lambda x: int(x >> 16 & 0xFF))
            df["id"] = df["id"].map(lambda x: int(x & 0x1F0000FF))

        unique_decodeable_msgids = [frame_id for frame_id in df["id"].unique() if frame_id in self.dbc.frame_ids]
        dfs = {msgid: df[df["id"] == msgid].reset_index(drop=True) for msgid in unique_decodeable_msgids}

        for msgid in unique_decodeable_msgids:
            self.data[msgid] = {signal.name: dict() for signal in self.dbc.get_message_by_frame_id(msgid).signals}
            decoded_cols = dfs[msgid]["data"].map(lambda x: self.dbc.decode_message(msgid, x, decode_choices=True))
            dfs[msgid] = pd.concat([dfs[msgid], pd.DataFrame(decoded_cols.tolist())], axis=1).drop(["id", "data", "dlc"], axis=1).set_index("timestamp")

            if self.is_cmcu_protocol:
                node_groups = dfs[msgid].groupby("node")
                node_dfs = {node: node_groups.get_group(node) for node in node_groups.groups}
                for node, df in node_dfs.items():
                    node_groups = df.groupby("slot")
                    for slotnum in node_groups.groups:
                        for signal in self.dbc.get_message_by_frame_id(msgid).signals:
                            series = node_groups.get_group(slotnum)[signal.name]
                            # series = series.loc[series.shift(-1) != series]
                            self.data[msgid][signal.name][(node, slotnum)] = series
            else:
                for signal in self.dbc.get_message_by_frame_id(msgid).signals:
                    series = dfs[msgid][signal.name]
                    self.data[msgid][signal.name][(0, 0)] = series

        if self.is_cmcu_protocol:
            # Merge CMCU_Slot_x_State messages into one message
            state_info_dfs = self.data[self.dbc.get_message_by_name("CMCU_State_Info").frame_id]
            state_info_dfs["CMCU_Slot_State"] = {}
            for slotnum in range(8):
                slot_dfs = state_info_dfs[f"CMCU_Slot_{slotnum}_State"]
                for (node, _), df in slot_dfs.items():
                    state_info_dfs["CMCU_Slot_State"][(node, slotnum)] = df
                del state_info_dfs[f"CMCU_Slot_{slotnum}_State"]

    def get_decoded_signals(self):
        return [Signal(msgid, name) for msgid in self.data.keys() for name in self.data[msgid].keys()]

    def get_message_name(self, msg_id):
        return self.dbc.get_message_by_frame_id(msg_id).name

    def get_battery_type(self, node, slot, time):
        return self.data[self.dbc.get_message_by_name("CMCU_Info").frame_id]["CMCU_Config_Battery_Type"][(node, 0)][-1]

    def get_error_from_rawdata(self, type, intdata, msgraw_num):
        ret = []
        if type == "OKAI":
            dbc = cantools.database.load_file(okai_dbc)
            errormapping = {1: "BMS_ErrorStatus1Resp", 2: "BMS_ErrorStatus2Resp"}
            bytedata = intdata.to_bytes(8, "big")
        else:
            return ret

        errormsg = dbc.get_message_by_name(errormapping[msgraw_num])
        decoded = dbc.get_message_by_name(errormapping[msgraw_num]).decode(bytedata)

        for signal in errormsg.signals:
            if decoded[signal.name] == 1:
                ret.append(signal.name)

        return ret
