================================================================================================
======                                                                                    ======
======  !!! Best copy folder to ipc and replace .dbc and .hex with the current files !!!  ======
======                                                                                    ======
================================================================================================

./CMCU_ID_SETTER

runs also if iot_forwarder is running.
1. will request all Serialnumbers
2. Will set all CMCUs to ID = 255
3. Will open all doors
4. for each door that will be closed, the id of the corresponding cmcu will be set
5. open door in order of which they have been closed (time inbetween ca. 2 sec)


----------------------------------------------------------------------------------------------------

./multi_cmcu_updater

!!! IOT_FORWARDER MUST BE turned OFF !!!
========================================

flags: -N: Number of CMCUs to be updated
       -F: Filepath

e.g. in the folder is the file app.hex and assume there are 5 CMCUs in the Sharingpoint with the IDs 1-5:

./multi_cmcu_updater -N 5 -F app.hex

fill run the update skript in a for loop from CMCU_ID = 1-5