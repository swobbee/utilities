#!/usr/bin/env python3

"""
usage:
    candump -tA can0 | ./parser.py
"""

import sys
import os
import argparse
import cantools
from cantools.subparsers.decode import logreader

ctrl = '[%sm'
color_msg_cmcu_regular = ctrl % '90'
color_msg_cmcu_irregular = ctrl % '34'
color_msg_ipc = ctrl % '33'
color_error = ctrl % '31'
color_reset = ctrl % ''


root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../../")
path2dbc =  os.path.join(root_dir, 'cmcu_ipc.dbc') #"doc/interface_definitions/cmcu_ipc.dbc"
DBC_PATH = os.path.join(root_dir, path2dbc)

prog = 'candump -tA can0 | ' + sys.argv[0]
arg_parser = argparse.ArgumentParser(prog)
arg_parser.add_argument('--line', '-l', choices=['y', 'n'])
arg_parser.add_argument('--path', '-p', default = DBC_PATH)
args = arg_parser.parse_args()

if args.path:
    DBC_PATH = args.path
dbc = cantools.database.load_file(DBC_PATH)
regular_cmcu_messages = [dbc.get_message_by_name('CMCU_State_Info')]
this_dbc_version = dbc.get_message_by_name("_DBC_VERSION").frame_id

def split_arbitration_id(arbitration_id):
    slot_num = (arbitration_id >> 16) & 0x7
    device_id = (arbitration_id >> 8) & 0xFF
    message_id = arbitration_id & 0xFF
    return slot_num, device_id, message_id

def get_color(msg):
    if msg.name.startswith('IPC_'):
        color = color_msg_ipc
    elif msg.name.startswith('CMCU_'):
        if msg in regular_cmcu_messages:
            color = color_msg_cmcu_regular
        else:
            color = color_msg_cmcu_irregular
    else:
        color = ''
    return color

def print_msg_to_terminal(msg, data, SLOT_NUM, CMCU_ID):
    global args
    if args.line == 'y':
        line = str(ts)+ f' CMCU_ID:{CMCU_ID} SLOT_ID:{SLOT_NUM} ' +f'{msg.name} '
        for key, value in data.items():
            line+=f'{key}:{value}'
            line+=' '
        line += f'message_id:{msg.frame_id}'
        print(line)
    else:
        print(f'CMCU_ID:{CMCU_ID}; SLOT_ID:{SLOT_NUM}')
        print('\t'+f'{msg.name}')
        for key, value in data.items():
            print('\t\t' + f'{key}:{value}')
        print('\n')

if __name__=='__main__':
    print(f"using dbc version {this_dbc_version}")
    parser = logreader.Parser(sys.stdin)
    for line, frame in parser.iterlines(keep_unknowns=True):
        line = line.strip()
        if frame is not None:
            try:
                arbitration_id = frame.frame_id
                data = frame.data
                SLOT_NUM, CMCU_ID, message_id = split_arbitration_id(arbitration_id)
                if arbitration_id >= 0x00100000:
                    # the most significant part of the arbitration_id of a message sent by the CMCU
                    # is some kind of checksum of the serial number to ensure that different CMCUs
                    # use different arbitration_ids even if they have the same node id.
                    # Set it to the value which is saved in the dbc file.
                    # This checksum is guaranteed to start with a 1 so that IPC messages have a higher
                    # priority on the CAN bus.
                    message_id |= 0x1FF80000
                # else:
                #     message_id = arbitration_id

                try:
                    msg = dbc.get_message_by_frame_id(message_id)
                except Exception as e:
                    raise ValueError("failed to find message with ID 0x%x: %r" % (message_id, e))
                raw_data = msg.decode(data)
                color = get_color(msg)
                print(color, end='')
                print(line)
                print_msg_to_terminal(msg, raw_data, SLOT_NUM, CMCU_ID)
                print(color_reset, end='')

                if msg.name == "CMCU_Info":
                    cmcu_dbc_version = raw_data['DBC_Version']
                    if cmcu_dbc_version != this_dbc_version:
                        err = f"dbc versions don't match, CMCU uses {cmcu_dbc_version}, this program uses {this_dbc_version}"
                        print(color_error + err + color_reset)
                        print("\n")

            except Exception as e:
                print(color_error)
                print(line)
                print(e)
                print(color_reset)

        else:
            print(color_error + str(line) + color_reset)
