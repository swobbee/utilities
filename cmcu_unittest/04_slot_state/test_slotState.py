import time
import pytest
import can
import logging
import os
import sys
import json

from datetime import datetime
root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../../")
sys.path.append(root_dir)
from cmcu_unittest.hardwareSimulations.cmcu_simulation import CMCU_DEVICE, return_test_config
from cmcu_unittest.hardwareSimulations.BatteryHandling import BatteryHandling
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
CMCU_Settings, Test_Parameters = return_test_config()

class TestCmcuState:
    baud_rate_bps = CMCU_Settings.get("baud_rate_bps")
    bus_id = CMCU_Settings.get("bus_id")
    node_id = CMCU_Settings.get("node_id")
    batt_number = CMCU_Settings.get("batt_number")
    FWV = CMCU_Settings.get("FWV")
    dbc_version = CMCU_Settings.get("dbc_version")
    IF_board = CMCU_Settings.get("IF_board")
    batt_type = CMCU_Settings.get("batt_type")
    charger_type = CMCU_Settings.get("charger_type")
    Serialnumber = CMCU_Settings.get("Serialnumber")
    IF_Serialnumber = CMCU_Settings.get("IF_Serialnumber")
    Charger_Serialnumber = CMCU_Settings.get("Charger_Serialnumber")

    def setup_class(self):

        self.recv_timeout = Test_Parameters.get("recv_timeout")
        self.test_slots = Test_Parameters.get("test_slots")[0]
        self.max_testing_time_s = Test_Parameters.get("max_testing_time_s")
        self.catch_error_time_window_s = Test_Parameters.get("catch_error_time_window_s")
        self.cmcu_heart_beat_frequency_s = Test_Parameters.get("cmcu_heartbeat_hz")
        
        self.setup_time = time.time()
        self.heart_beat_time_delta = None
        self.last_heartbeat_timestamp = None
        
        
        self.first_hb_skipped = False

        self.start_time_of_test_case = False

        self.over_current_mA = 20e3  # mA

        self.cmcu_device = CMCU_DEVICE(interface_board_type = self.IF_board, baud_rate_bps=self.baud_rate_bps)
        self.battery_handler = BatteryHandling(batt_type=self.batt_type)
        #self.cmcu_device.bus.send_IPC_Config(self.charger_type, self.batt_type)
        

    def setup_method(self):
        time.sleep(0.1)
        self.cmcu_device.set_initial_state()

    def teardown_method(self):
        try: 
            self.battery_handler.remove_battery()
        except:
            pass

    def test_SS_001_slots_in_init_state(self):
        reset_done = False
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
                if msg_name == 'CMCU_Info':
                    reset_done = True
                elif msg_name == 'CMCU_State_Info' and reset_done == True:
                    slot_states = [msg_signals.get(value) for value in msg_signals if "Slot" in value]
                    active_slots = slot_states[:self.batt_number]
                    assert all(slot_state == 'UNKNOWN' for slot_state in active_slots)
                    break
            except Exception as e:
                logger.exception(e)
        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')

    def test_SS_002_enabled_slots_after_init_state(self):
        init_done = False
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
            _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
            if msg_name == 'CMCU_State_Info' and init_done:
                slot_states = [msg_signals.get(value) for value in msg_signals if "Slot" in value]
                active_slots = slot_states[:self.batt_number]
                assert all(slot_state != 'UNKNOWN' for slot_state in active_slots)
                break
            elif msg_signals.get('CMCU_State') == 'INIT':
                init_done = True

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')

    def test_SS_003_disabled_slots_after_init_state(self):
        init_done = False
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
                if msg_name == 'CMCU_State_Info' and init_done:
                    slot_states = [msg_signals.get(value) for value in msg_signals if "Slot" in value]
                    inactive_slots = slot_states[self.batt_number:]
                    assert all(slot_state == 'DISABLED' for slot_state in inactive_slots)
                    break
                elif msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True
            except Exception as e:
                logger.exception(e)

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')

    def test_SS_004_slot_storage_after_init_state(self):
        self.battery_handler.insert_battery()
        init_done = False
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_name == 'CMCU_State_Info' and init_done:
                    active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][:self.batt_number]
                    test_slot_state = active_slots[self.test_slots]
                    assert test_slot_state == "STORAGE"
                    break
                elif msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True
            except Exception as e:
                logger.exception(e)
        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
        

    def test_SS_005_slot_empty_after_init_state(self):
        init_done = False
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
                if msg_name == 'CMCU_State_Info' and init_done:
                    slot_states = [msg_signals.get(value) for value in msg_signals if "Slot" in value]
                    active_slots = slot_states[:self.batt_number]
                    assert all(slot_state == 'EMPTY' for slot_state in active_slots)
                    break
                elif msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True
            except Exception as e:
                logger.exception(e)
        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')


    @pytest.mark.skipif(dbc_version >= 20,
                reason="requires dbc v. < 20")
    def test_SS_006_slot_error_after_init_state(self):
        self.battery_handler.insert_battery()
        # create any error from raw error
        error_name = list(self.battery_handler.battery.errors.keys())[0]
        self.battery_handler.battery.errors[error_name] = True
        init_done = False
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
                if msg_name == 'CMCU_State_Info' and init_done:
                    active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][:self.batt_number]
                    test_slot_state = active_slots[self.test_slots]
                    assert test_slot_state == "ERROR"
                    break
                elif msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True
            except Exception as e:
                logger.exception(e)
        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
        
 
    def test_SS_007_slot_start_charging_state(self):
        # 1. send charge cmd on door closed
        # 2. assert if charging started on test_slot

        self.battery_handler.insert_battery()
        init_done = False
        charge_cmd_sent = False
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)


                if (msg_signals.get('CMCU_State')=='OK_DOOR_CLOSED') and not charge_cmd_sent:
                    active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][:self.batt_number]
                    if not 'STORAGE' in active_slots:
                        raise AssertionError(f'No Battery in Slot present')
                        break
                    else:
                        self.cmcu_device.bus.send_IPC_Cmd('CHARGE_START', slotNum=0)
                        charge_cmd_sent = True

                elif msg_name == 'CMCU_State_Info' and charge_cmd_sent:
                    active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][:self.batt_number]
                    test_slot_state = active_slots[self.test_slots]
                    assert test_slot_state == "CHARGING"
                    break
            except Exception as e:
                logger.exception(e)
        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
        

    def test_SS_008_slot_end_charging_state(self):
        # 1. do a reset and wait for init to be done
        # 2. send charge cmd on door closed to test_slot
        # 3. check if charging started
        # 4. if charging started: stop charging
        # 5. assert if test_slot switched from charging to storage

        self.battery_handler.insert_battery()
        charge_cmd_sent = False
        charging_started = False
        charging_stop_cmd_sent = False
        init_done = False
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True

                elif msg_name == 'CMCU_State_Info' and init_done:

                    if (msg_signals.get('CMCU_State')=='OK_DOOR_CLOSED') and not charge_cmd_sent:
                        # test if battery is present
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][:self.batt_number]
                        test_slot_state = active_slots[self.test_slots]
                        if test_slot_state != 'STORAGE':
                            raise AssertionError(f'Slot State != STORAGE; current State is: {test_slot_state}')
                            break
                        else:
                            self.cmcu_device.bus.send_IPC_Cmd('CHARGE_START', slotNum=self.test_slots)
                            charge_cmd_sent = True

                    elif msg_name == 'CMCU_State_Info' and charge_cmd_sent and not charging_started:
                        # test if charging started
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][:self.batt_number]
                        test_slot_state = active_slots[self.test_slots]
                        if test_slot_state == "CHARGING":
                            charging_started = True

                    elif msg_name == 'CMCU_State_Info' and charging_started and not charging_stop_cmd_sent:
                        # send stop charging if not done yet
                        self.cmcu_device.bus.send_IPC_Cmd('CHARGE_STOP', slotNum=self.test_slots)
                        charging_stop_cmd_sent = True

                    elif msg_name == 'CMCU_State_Info' and charging_stop_cmd_sent:
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][:self.batt_number]
                        test_slot_state = active_slots[self.test_slots]
                        assert test_slot_state == "STORAGE"
                        break
            except Exception as e:
                logger.exception(e)

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
        


    @pytest.mark.skipif(dbc_version >= 20,
            reason="requires dbc v. < 20")
    def test_SS_009_slot_state_change_on_error(self):
        # 1. wait for cmcu init
        # 2. check if battery is present in test slot
        # 3. provoke error on test slot
        # 4. request bat data
        # 5. check if state switched from storage to error

        self.battery_handler.insert_battery()
        init_done = False
        battery_present = False
        slot_error_provoked = False
        bat_data_requested = False
        states_in_error_window = []
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True

                elif msg_name == 'CMCU_State_Info' and init_done and not battery_present:
                    active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][:self.batt_number]
                    test_slot_state = active_slots[self.test_slots]
                    if test_slot_state == "STORAGE":
                        battery_present = True

                elif battery_present and not slot_error_provoked:
                    error_name = list(self.battery_handler.battery.errors.keys())[0]
                    self.battery_handler.battery.errors[error_name] = True
                    slot_error_provoked = True

                elif slot_error_provoked and not bat_data_requested:
                    self.cmcu_device.bus.send_IPC_Cmd('REQUEST_BATDATA', slotNum=self.test_slots)
                    bat_data_requested = True

                elif msg_name == 'CMCU_State_Info' and bat_data_requested:
                    active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                :self.batt_number]
                    states_in_error_window.append(active_slots[self.test_slots])

                    # todo max. response time for error?
                    if len(states_in_error_window) >= self.catch_error_time_window_s:
                        assert states_in_error_window[-1] == "ERROR"
                        break
            except Exception as e:
                logger.exception(e)

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
        

    def test_SS_010_slot_error_triggers_error_type(self):
        ## Todo: assert statement!
        
        # 1. wait for cmcu init
        # 2. check if battery is present in test slot
        # 3. provoke error on test slot
        # 4. request bat data
        # 5. check if state switched from storage to error
        # 6. assert error reason
        

        
        self.battery_handler.insert_battery()
        init_done = False
        battery_present = False
        slot_error_provoked = False
        bat_data_requested = False
        states_in_error_window = []
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                slot_num, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True

                elif msg_name == 'CMCU_State_Info' and init_done and not battery_present:
                    active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][:self.batt_number]
                    test_slot_state = str(active_slots[self.test_slots])
                    if test_slot_state.find("STORAGE") != -1:
                        battery_present = True

                elif battery_present and not slot_error_provoked:
                    error_name = list(self.battery_handler.battery.errors.keys())[0]
                    self.battery_handler.battery.errors[error_name] = True
                    slot_error_provoked = True

                elif slot_error_provoked and not bat_data_requested:
                    self.cmcu_device.bus.send_IPC_Cmd('REQUEST_BATDATA', slotNum=self.test_slots)
                    bat_data_requested = True

                elif msg_name == 'CMCU_Slot_Error' and bat_data_requested and slot_num == self.test_slots:
                    assert (msg_signals.get("CMCU_Slot_Error")=="ERROR_BATTERY_STATUSCODE") or (msg_signals.get("CMCU_SLOT_ERROR_BATTERY_STATUSCODE")==1)
                    break
            except Exception as e:
                logger.exception(e)

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
        


    @pytest.mark.skipif(dbc_version >= 20,
            reason="requires dbc v. < 20")
    def test_SS_011_slot_end_charging_on_error_state(self):
        # 1. do a reset and wait for init to be done
        # 2. send charge cmd on door closed to test_slot
        # 3. check if charging started
        # 4. if charging started: create slot error (set charging current to 20 Ampere)
        # 5. assert if test_slot switched from charging to error within the last three heatbeats

        self.battery_handler.insert_battery()
        charge_cmd_sent = False
        charging_started = False
        slot_error_provoked = False
        init_done = False
        states_in_error_window = []
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True

                elif msg_name == 'CMCU_State_Info' and init_done:

                    if (msg_signals.get('CMCU_State') == 'OK_DOOR_CLOSED') and not charge_cmd_sent:
                        # test if battery is present
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                    :self.batt_number]
                        test_slot_state = active_slots[self.test_slots]
                        if test_slot_state != 'STORAGE':
                            raise AssertionError(f'Slot State != STORAGE; current State is: {test_slot_state}')
                            break
                        else:
                            self.cmcu_device.bus.send_IPC_Cmd('CHARGE_START', slotNum=self.test_slots)
                            charge_cmd_sent = True

                    elif msg_name == 'CMCU_State_Info' and charge_cmd_sent and not charging_started:
                        # test if charging started
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                    :self.batt_number]
                        test_slot_state = active_slots[self.test_slots]
                        if test_slot_state == "CHARGING":
                            # add check of gpio pin here
                            charging_started = True

                    elif msg_name == 'CMCU_State_Info' and charging_started and not slot_error_provoked:
                        # provoke slot error
                        self.battery_handler.battery.current = self.over_current_mA
                        slot_error_provoked = True

                    elif msg_name == 'CMCU_State_Info' and slot_error_provoked:
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                    :self.batt_number]
                        states_in_error_window.append(active_slots[self.test_slots])

                        #todo max. response time for error?
                        if len(states_in_error_window) >= self.catch_error_time_window_s:
                            assert states_in_error_window[-1] == "ERROR"
                            break
            except Exception as e:
                logger.exception(e)
        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
        

    @pytest.mark.skipif(dbc_version < 20,
            reason="requires dbc v. >= 20")
    def test_SS_012_slot_end_charging_on_error_state(self):
        # 1. do a reset and wait for init to be done
        # 2. send charge cmd on door closed to test_slot
        # 3. check if charging started
        # 4. if charging started: create slot error (set charging current to 20 Ampere)
        # 5. assert if test_slot switched from charging to error within the last three heatbeats

        self.battery_handler.insert_battery()
        charge_cmd_sent = False
        charging_started = False
        slot_error_provoked = False
        init_done = False
        states_in_error_window = []
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True

                elif msg_name == 'CMCU_State_Info' and init_done:

                    if (msg_signals.get('CMCU_State') == 'OK_DOOR_CLOSED') and not charge_cmd_sent:
                        # test if battery is present
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                    :self.batt_number]
                        test_slot_state = active_slots[self.test_slots]
                        if test_slot_state != 'STORAGE':
                            raise AssertionError(f'Slot State != STORAGE; current State is: {test_slot_state}')
                            break
                        else:
                            self.cmcu_device.bus.send_IPC_Cmd('CHARGE_START', slotNum=self.test_slots)
                            charge_cmd_sent = True

                    elif msg_name == 'CMCU_State_Info' and charge_cmd_sent and not charging_started:
                        # test if charging started
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                    :self.batt_number]
                        test_slot_state = active_slots[self.test_slots]
                        if test_slot_state == "CHARGING":
                            charging_started = True

                    elif msg_name == 'CMCU_State_Info' and charging_started and not slot_error_provoked:
                        # provoke slot error
                        self.battery_handler.battery.current = self.over_current_mA
                        slot_error_provoked = True

                    elif msg_name == 'CMCU_State_Info' and slot_error_provoked:
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                    :self.batt_number]
                        states_in_error_window.append(active_slots[self.test_slots])

                        #todo max. response time for error?
                        if len(states_in_error_window) >= self.catch_error_time_window_s:
                            assert states_in_error_window[-1] == "ERROR_STORAGE"
                            break
            except Exception as e:
                logger.exception(e)

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
        





    def test_SS_013_slot_state_error_no_change_on_error_change(self):
        # 1. wait for cmcu init
        # 2. check if battery is present in test slot
        # 3. provoke error on test slot
        # 4. request bat data
        # 5. check if state switched from storage to error
        # 6. provoke second error
        # 7. request bat data
        # 8. check if state is still error

        self.battery_handler.insert_battery()
        init_done = False
        battery_present = False
        slot_error_1_provoked = False
        first_error_recv = False
        slot_error_2_provoked = False
        bat_data_requested_1 = False
        bat_data_requested_2 = False
        states_in_error_window = []
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True
                    logger.debug("init done")

                # check for present battery
                elif msg_name == 'CMCU_State_Info' and init_done and not battery_present:
                    active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][:self.batt_number]
                    test_slot_state = active_slots[self.test_slots]
                    if test_slot_state == "STORAGE":
                        battery_present = True
                        logger.debug("battery present")

                # provoke first error
                elif battery_present and not slot_error_1_provoked:
                    error_name = list(self.battery_handler.battery.errors.keys())[0]
                    self.battery_handler.battery.errors[error_name] = True
                    slot_error_1_provoked = True
                    logger.debug("slot_error_1_provoked")

                # request bat data first time
                elif slot_error_1_provoked and not bat_data_requested_1 and not first_error_recv:
                    self.cmcu_device.bus.send_IPC_Cmd('REQUEST_BATDATA', slotNum=self.test_slots)
                    bat_data_requested_1 = True
                    logger.debug("bat_data_requested_1")


                # check if slot is in error
                elif (msg_name == 'CMCU_State_Info') and bat_data_requested_1 and not first_error_recv:
                    active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                :self.batt_number]
                    states_in_error_window.append(active_slots[self.test_slots])

                    # todo max. response time for error?
                    if len(states_in_error_window) >= self.catch_error_time_window_s:
                        if states_in_error_window[-1].name.find("ERROR") != -1:
                            first_error_recv = True
                            logger.debug("first_error_recv")


                # provoke second error
                elif battery_present and not slot_error_2_provoked and first_error_recv:
                    error_name = list(self.battery_handler.battery.errors.keys())[1]
                    self.battery_handler.battery.errors[error_name] = True
                    slot_error_2_provoked = True
                    logger.debug("slot_error_2_provoked")

                # req bat data again
                elif slot_error_2_provoked and not bat_data_requested_2 and first_error_recv:
                    self.cmcu_device.bus.send_IPC_Cmd('REQUEST_BATDATA', slotNum=self.test_slots)
                    bat_data_requested_2 = True
                    states_in_error_window=[]
                    logger.debug("bat_data_requested_2")

                # check if still in error mode
                elif msg_name == 'CMCU_State_Info' and slot_error_2_provoked and bat_data_requested_2:
                    active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                :self.batt_number]
                    states_in_error_window.append(active_slots[self.test_slots])

                    # todo max. response time for error?
                    if len(states_in_error_window) >= self.catch_error_time_window_s:
                        assert states_in_error_window[-1].name.find("ERROR") != -1
                        break
            except Exception as e:
                logger.exception(e)


        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
        

    def test_SS_014_slot_reset_command_triggers_slot_init(self):
        ## basically test ss_011 with a reset at the end
        # 1. do a reset and wait for init to be done
        # 2. send charge cmd on door closed to test_slot
        # 3. check if charging started
        # 4. if charging started: create slot error (set charging current to 20 Ampere)
        # 5. check if test_slot switched from charging to error within the last heatbeats
        # 6. send slot reset to test slot
        # 5. check if test_slot switched from error to storrage within the last heatbeats

        
        self.battery_handler.insert_battery()
        reset_sent = False
        charge_cmd_sent = False
        charging_started = False
        slot_error_provoked = False
        error_state_present = False
        init_done = False
        states_in_error_window = []
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True

                elif msg_name == 'CMCU_State_Info' and init_done:

                    if (msg_signals.get('CMCU_State') == 'OK_DOOR_CLOSED') and not charge_cmd_sent:
                        # test if battery is present
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                    :self.batt_number]
                        test_slot_state = active_slots[self.test_slots]
                        if test_slot_state != 'STORAGE':
                            raise AssertionError(f'Slot State != STORAGE; current State is: {test_slot_state}')
                            break
                        else:
                            self.cmcu_device.bus.send_IPC_Cmd('CHARGE_START', slotNum=self.test_slots)
                            charge_cmd_sent = True

                    elif msg_name == 'CMCU_State_Info' and charge_cmd_sent and not charging_started:
                        # test if charging started
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                    :self.batt_number]
                        test_slot_state = active_slots[self.test_slots]
                        if test_slot_state == "CHARGING":
                            charging_started = True
                            print("charging started")

                    elif msg_name == 'CMCU_State_Info' and charging_started and not slot_error_provoked:
                        # provoke slot error
                        self.battery_handler.battery.current = self.over_current_mA
                        slot_error_provoked = True

                    elif msg_name == 'CMCU_State_Info' and slot_error_provoked and not error_state_present:
                        # check if error state is present
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                    :self.batt_number]
                        states_in_error_window.append(active_slots[self.test_slots])

                        #todo max. response time for error?
                        if len(states_in_error_window) >= self.catch_error_time_window_s:
                            error_state_present = states_in_error_window[-1].name.find("ERROR") != -1

                        
                    elif error_state_present and not reset_sent:
                        # reset slot
                        self.cmcu_device.bus.send_IPC_Cmd('BATTERY_RESET', slotNum=self.test_slots)
                        reset_sent = True

                    
                    elif msg_name == 'CMCU_State_Info' and reset_sent:
                        # assert if error was reset
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                    :self.batt_number]
                        states_in_error_window.append(active_slots[self.test_slots])
                        #todo max. response time for error?
                        if len(states_in_error_window) >= self.catch_error_time_window_s:
                            assert states_in_error_window[-1].name.find("STORAGE") != -1
                            break

            except Exception as e:
                logger.exception(e)

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')                          


    # @pytest.mark.skip(reason="Not implemented yet")
    # def test_SS_015_slot_end_charging_on_error_does_not_affect_other_slots(self):
    #     raise AssertionError('not implemented yet')


    # @pytest.mark.skipif(dbc_version < 20,
    #     reason="requires dbc v. >= 20")
    # def test_SS_016_slot_error_storage_reset_after_battery_removal(self):
    #     raise AssertionError('not implemented yet')








