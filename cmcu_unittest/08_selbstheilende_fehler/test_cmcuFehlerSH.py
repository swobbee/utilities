import time
import pytest
import logging
import os
import sys

from datetime import datetime
root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../../")
sys.path.append(root_dir)
from cmcu_unittest.hardwareSimulations.cmcu_simulation import CMCU_DEVICE, return_test_config 

logger = logging.getLogger(__name__)

CMCU_Settings, Test_Parameters = return_test_config()

class TestCmcuState:
    baud_rate_bps = CMCU_Settings.get("baud_rate_bps")
    bus_id = CMCU_Settings.get("bus_id")
    node_id = CMCU_Settings.get("node_id")
    batt_number = CMCU_Settings.get("batt_number")
    FWV = CMCU_Settings.get("FWV")
    dbc_version = CMCU_Settings.get("dbc_version")
    IF_board = CMCU_Settings.get("IF_board")
    batt_type = CMCU_Settings.get("batt_type")
    charger_type = CMCU_Settings.get("charger_type")
    Serialnumber = CMCU_Settings.get("Serialnumber")
    IF_Serialnumber = CMCU_Settings.get("IF_Serialnumber")
    Charger_Serialnumber = CMCU_Settings.get("Charger_Serialnumber")


    def setup_class(self):
        self.max_testing_time_ms = Test_Parameters.get("max_testing_time_s")
        self.cmcu_device = CMCU_DEVICE(interface_board_type = self.IF_board, baud_rate_bps=self.baud_rate_bps)
        self.cmcu_device.bus.send_IPC_Config(self.charger_type, self.batt_type)
        
    def setup_method(self):
        time.sleep(0.1)
        self.cmcu_device.set_initial_state()

    def test_CFSH_001_selfhealing_error_door_open(self):
        init_done = False
        error_set = False
        error_received = False
        error_resolved = False
        self.cmcu_device.__set_door_open__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_ms):
            try:
                msg = self.cmcu_device.bus.network.recv()
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True
                    logger.debug("init_done")

                elif msg_name == 'CMCU_State_Info' and init_done and not error_set:
                    self.cmcu_device.__set_door_error__()
                    error_set = True
                    logger.debug("error_set")

                elif msg_name == 'CMCU_State_Info' and init_done and error_set and not error_resolved:
                    if msg_signals.get('CMCU_State') == 'ERROR_DOOR_OPEN':
                        self.cmcu_device.__set_door_open__()
                        error_resolved = True
                        logger.debug("error_resolved")
                
                elif msg_name == 'CMCU_State_Info' and init_done and error_set and error_resolved:
                    assert msg_signals.get("CMCU_State") == "OK_DOOR_OPEN"
                    break

            except Exception as e:
                logger.exception(e)

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_ms):
            raise AssertionError(f'Time out............')
    

    def test_CFSH_002_selfhealing_error_door_closed(self):
        init_done = False
        error_set = False
        error_received = False
        error_resolved = False
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_ms):
            try:
                msg = self.cmcu_device.bus.network.recv()
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True
                    logger.debug("init_done")

                elif msg_name == 'CMCU_State_Info' and init_done and not error_set:
                    self.cmcu_device.__set_door_error__()
                    error_set = True
                    logger.debug("error_set")

                elif msg_name == 'CMCU_State_Info' and init_done and error_set and not error_resolved:
                    if msg_signals.get('CMCU_State') == 'ERROR_DOOR_CLOSED':
                        self.cmcu_device.__set_door_closed__()
                        error_resolved = True
                        logger.debug("error_resolved")
                
                elif msg_name == 'CMCU_State_Info' and init_done and error_set and error_resolved:
                    assert msg_signals.get("CMCU_State") == "OK_DOOR_CLOSED"
                    break

            except Exception as e:
                logger.exception(e)

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_ms):
            raise AssertionError(f'Time out............')