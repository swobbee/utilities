import time
import os
import sys

root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../")
sys.path.append(root_dir)

import cmcu_unittest.hardwareSimulations.cmcu_simulation as cmcu
class Door:
    def __init__(self, cmcu_bus):
        self.cmcu_bus = cmcu_bus
        pass
    def __set_door_state__(self, state):
        # state can be open, closed, error
        if state == 'open':
            self.cmcu_bus.send_IPC_Cmd("DOOR_OPEN")
        elif state == 'closed':
            cmcu.__set_door_closed__()
        elif state == 'error':
            cmcu.__set_door_error__()
        