import os
import sys
import logging
from cmcu_unittest.hardwareSimulations.GPIOs import GPIOs
logger = logging.getLogger(__name__)


class Interface_DEVICE(GPIOs):
    def __init__(self, interface_board_type='CN4'):
        super(Interface_DEVICE, self).__init__()
        self.interface_type = interface_board_type
        self.temp_ext = {}
        self.relais = {}
        self.locks = {}
        self.batteries = {}
        # do lookup on json file what to load for interface
        # mapping table, pins, hardware

    def __setup_gpios__(self):
        if self.interface_type.find("CN4") != -1:
            self.__init_CN4_gpios__()
        elif self.interface_type.find("CN8") != -1:
            self.__init_CN8_gpios__()
        elif self.interface_type.find("CN1") != -1:
            self.__init_CN1_gpios__()

    def __setup_cn4__(self):
        self.diconnect_all_temperatures()

    def connect_temp_ext_x(self, channel, status):
        state = 1 if status == 'on' else 0
        self.__set_if_temp_x_ext_connection__(cahnnel, state)

    def connect_all_temperatures(self):
        self.__set_if_temp_ext_connected__()

    def disconnect_all_temperatures(self):
        self.__set_if_temp_ext_disconnected__()

