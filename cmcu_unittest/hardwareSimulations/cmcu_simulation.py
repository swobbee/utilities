import os
import sys
import logging
import json

root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../../")
sys.path.append(root_dir)

import cmcu_unittest.hardwareSimulations.cmcu_can_comm as cmcu_can_comm
from cmcu_unittest.hardwareSimulations.GPIOs import GPIOs
from cmcu_unittest.hardwareSimulations.interface_simulation import Interface_DEVICE
logger = logging.getLogger(__name__)


DBC_path = os.path.join(root_dir, 'doc', 'interface_definitions', r"cmcu_ipc.dbc")
# /home/pi/Documents/utilities/cmcu_unittest/dbc18/cmcu_ipc.dbc'

class CMCU_DEVICE(GPIOs):
    def __init__(self, interface_board_type = 'CN4', dbc_path=DBC_path, bus_id=0, can_bus_type=None, node_id=0, baud_rate_bps=500000, endianness='little'):
        super(CMCU_DEVICE, self).__init__()

        self.if_board_type = interface_board_type
        self.interface = Interface_DEVICE(self.if_board_type)

        self.bus = None
        self.bus_id = bus_id 
        self.node_id = node_id
        self.baud_rate_bps = int(baud_rate_bps)
        self.dbc_path = dbc_path

        self.__start_can_bus__()
        self.__initialize_cmcu_bus__()
        self.__init_cmcu_gpios__()
        self.interface.__setup_gpios__()

        self.set_initial_state()

    def return_used_dbc_version(self):
        return self.bus.remote_node.get_message_by_name("_DBC_VERSION").frame_id

    def __start_can_bus__(self):
        state = os.system(f'ip addr | grep "can{self.bus_id}" | grep  "state UP"')
        if state != 0:
            try:
                return_value = int(os.system(f"sudo /sbin/ip link set can{self.bus_id} up type can bitrate {self.baud_rate_bps}"))
                if not return_value:
                    logger.warning('CAN-Bus started')
                else:
                    logger.error('Can bus did not start')
                    #raise AssertionError('can bus did not start')
            except Exception as exc:
                logger.exception(exc)


    def create_CanOpen_device(self):
        return cmcu_can_comm.cmcu_comm(
            full_path_dbc_file=self.dbc_path,
            can_bus=self.bus_id,
            can_id=self.node_id,
            bitrate=self.baud_rate_bps,
            instance_name=None
        )

    def __initialize_cmcu_bus__(self):
        self.bus = self.create_CanOpen_device()
        self.bus.add_to_can_bus()


    def set_initial_state(self):
        self.__set_door_closed__()
        self.__set_temp_ext_connected__()
        self.__setup_interface__()
        
    def __setup_interface__(self):
        pass


def return_test_config(config_path=r'/home/pi/utilities/cmcu_unittest/pytest_config.json'):
    # Opening JSON file
    f = open(config_path)

    # returns JSON object as
    # a dictionary
    CONFIGDATA = json.load(f)
    # Closing file
    f.close()
    CMCU_Settings = CONFIGDATA.get("CMCU_Settings")
    Test_Parameters = CONFIGDATA.get("Test_Parameters")
    return CMCU_Settings, Test_Parameters

print(5)