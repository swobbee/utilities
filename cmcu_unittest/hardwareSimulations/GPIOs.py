from abc import abstractmethod
import time
from datetime import datetime
import os
import sys
import json
import RPi.GPIO as GPIO

filepath = r'/home/pi/utilities/cmcu_unittest/hardwareSimulations/gpio_mapping.json' 
# Opening JSON file
f = open(filepath)

# returns JSON object as
# a dictionary
DATA = json.load(f)
# Closing file
f.close()


''' DOOR '''
c_closed = 1
c_open = 0

door_status_list = (DATA["CMCU"]["door_latch"], DATA["CMCU"]["door_lock"])
door_open = (c_open, c_closed)  
door_closed = (c_closed, c_open) 
door_error = (c_closed, c_closed)
door_manipulated = (c_open, c_open)

'''Temp Ext'''
connected = 1
disconnected = 0

'''Relais'''
ON = 1
OFF = 0



class GPIOs():

    def __init__(self):
        
        self.pin_map = None
        self.bat_states = None
        self.rev_channel_map = None
        self.relais_states = None
        self.lock_states = None
        # cleanup 
        GPIO.setwarnings(False)
        self.__cleanup__()
        GPIO.setmode(GPIO.BCM)

        # Batteries

    ############################################################################
    ### GPIO SETUP ###
    def __init_cmcu_gpios__(self):
        # door
        GPIO.setup(DATA["CMCU"]["door_pwr"], GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(DATA["CMCU"]["door_lock"], GPIO.OUT)
        GPIO.setup(DATA["CMCU"]["door_latch"], GPIO.OUT)
        GPIO.add_event_detect(DATA["CMCU"]["door_pwr"], GPIO.RISING, callback = self.doorOpenTriggerd, bouncetime=500)

        # temp ext 
        GPIO.setup(DATA["CMCU"]["temp_ext"], GPIO.OUT)

    def __init_CN1_gpios__(self):
        self.rev_channel_map = {v: k for k, v in DATA["CN1"].items()}
        self.bat_states = {}
        for function, pin in DATA["CN1"].items(): 
            GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
            GPIO.add_event_detect(pin, GPIO.BOTH, callback = self.battery_event, bouncetime=500)
            self.bat_states[function]=GPIO.input(pin)

    def __init_CN4_gpios__(self):
        # Relays
        self.relais_states = {}
        for slot, pin in DATA["CN4"]["relais"].items(): 
            GPIO.setup(pin, GPIO.IN)
            GPIO.add_event_detect(pin, GPIO.BOTH, callback = self.charging_event, bouncetime=500)

            self.relais_states[slot]=GPIO.input(pin)
        # Locks
        self.lock_states = {}
        # for slot, pin in DATA["CN4"]["locks"].items(): 
        #     GPIO.setup(pin, GPIO.IN)
        #     GPIO.add_event_detect(pin, GPIO.BOTH, callback = self.lock_event, bouncetime=500)
        #     self.lock_states[slot]=GPIO.input(pin)
        # IF-temp EXT
        self.temp_pins = {}
        for slot, pin in DATA["CN4"]["temp_ext"].items(): 
            GPIO.setup(pin, GPIO.OUT)
            GPIO.output(pin, 0)
            self.temp_pins[slot] = pin

    def __init_CN8_gpios__(self):
        self.__init_CN4_gpios__()
        # Relays
        self.relais_states = {}
        for slot, pin in DATA["CN8"]["relais"].items(): 
            GPIO.setup(pin, GPIO.IN)
            GPIO.add_event_detect(pin, GPIO.BOTH, callback = self.charging_event, bouncetime=500)
            self.relais_states[slot]=GPIO.input(pin)
        # Locks
        self.lock_states = {}
        # for slot, pin in DATA["CN8"]["locks"].items(): 
        #     GPIO.setup(pin, GPIO.IN)
        #     GPIO.add_event_detect(pin, GPIO.BOTH, callback = self.lock_event, bouncetime=500)
        #     self.lock_states[slot]=GPIO.input(pin)
        # IF-temp EXT
        for slot, pin in DATA["CN8"]["temp_ext"].items(): 
            GPIO.setup(pin, GPIO.OUT)
            GPIO.output(pin, 0)
            self.temp_pins[slot] = pin

    ############################################################################

    ### CMCU FUNCTIONS ###
     ## Callback evetns ##

    def doorOpenTriggerd(self, channel):
        if GPIO.input(DATA["CMCU"]["door_pwr"]) == 1:
            self.__set_door_open__()
    
    ## door functions ##

    def __set_door_open__(self):
        GPIO.output(door_status_list, door_closed)
        
    def __set_door_closed__(self): 
        GPIO.output(door_status_list, door_open)
        
    def __set_door_error__(self):
        GPIO.output(door_status_list, door_error)

    def __set_door_manipulated__(self):
        GPIO.output(door_status_list, door_manipulated)


   ## Temperature Functions ##
    
    def __set_temp_ext_connected__(self):
        GPIO.output(DATA["CMCU"]["temp_ext"], connected)

    def __set_temp_ext_disconnected__(self):
        GPIO.output(DATA["CMCU"]["temp_ext"], disconnected)

    def __set_temp_ext_tempertature__(resistance_value):
        # todo set poti value
        pass

    ############################################################################   
    
    ### IF FUNCTIONS ###
    ## Callback evetns ##

    @abstractmethod
    def battery_event_handling(self):
        raise NotImplementedError("Must override run method")

    def battery_event(self, channel):
        key = self.rev_channel_map[channel]
        self.bat_states[key] = GPIO.input(channel)
        self.battery_event_handling()

    def charging_event(self, channel):
        self.relais_states[channel] = GPIO.input(channel)

    def lock_event(self, channel):
        self.lock_states[channel] = GPIO.input(channel)

    ## Temperature Functions ##

    def __set_if_temp_x_ext_connection__(self, channel, state):
        GPIO.output(channel, state)

    def __set_if_temp_ext_disconnected__(self):
        for channel in self.temp_pins.values():
            GPIO.output(channel, disconnected)

    def __set_if_temp_ext_connected__(self):
        for channel in self.temp_pins.values():
            GPIO.output(channel, connected)
    
    def __set_if_temp_x_ext_tempertature__(resistance_value):
        # todo set poti value
        pass


    ############################################################################

    def __cleanup__(self):
        GPIO.cleanup()

# cmcu = GPIOs()
# cmcu.__init_CN1_gpios__()
# while True:
#     time.sleep(0.005)
# if __name__ == '__main__':°
#     try:
#         # print("programm start")
#         __set_door_closed__()
#         GPIO.output(temp_ext, 1)
#         while 1:
#             time.sleep(0.01)
#             cmd = int(input("enter int: "))
#             # print(f'input was {cmd}; {type(cmd)}')
#             if cmd == 1:
#                 __set_door_open__()
#             elif cmd == 2:
#                 __set_door_closed__()
#             elif cmd == 3:
#                 __set_door_manipulated__()
#             elif cmd == 4:
#                 __set_door_error__()

#     except KeyboardInterrupt:
#         GPIO.cleanup()
#         # print("programm stopped and cleaned")


