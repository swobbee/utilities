import random
import time
import logging
import can
import cantools
import sys
import traceback

logger = logging.getLogger(__name__)

BITPOS_SLOT_NUM = 16
BITPOS_DEVICE_ID = 8
BITPOS_MESSAGE_ID = 0

BITPOS_SLOT_NUM = 16
BITPOS_DEVICE_ID = 8
BITPOS_MESSAGE_ID = 0

class ListenerSubclass(can.Listener):
	def __init__(self, on_receive):
		self._on_receive = on_receive

	def on_message_received(self, msg):
		self._on_receive(msg)


class cmcu_comm:

	def __init__(self, full_path_dbc_file=None, can_bus=0, can_bus_type=None, can_id=0, bitrate=500000, endianness='little',
				 instance_name=None):
		if can_bus_type is None:
			self.can_bus = "can" + str(can_bus)
		else:
			self.can_bus = can_bus_type
		self.can_id = can_id
		self.bitrate = bitrate
		self.full_path_dbc_file = full_path_dbc_file
		self.endianness = endianness  # little or big
		self.linux = True
		self.device_name = None
		# self.linux = sys.platform == "linux" or sys.platform == "linux2" if linux is None else bool(linux),
		self.bus_type = None
		self.network = None
		self.remote_node = None
		self.cmd = None
		self.init_status = None
		self.model = None
		self.instance_name = instance_name

	def add_to_can_bus(self, batt_number=8, bus_type=None):

		self.bus_type = bus_type if bus_type is not None else ('socketcan' if self.linux else 'pcan')

		self.network = can.Bus(channel=self.can_bus, bustype=self.bus_type, bitrate=self.bitrate)

		self.remote_node = cantools.database.load_file(self.full_path_dbc_file)
		self.cmd = self.remote_node.get_message_by_name("IPC_Cmd")

		self.model = Model()
		for message in self.remote_node.messages:
			model_message = Message(message)

			for signal in message.signals:
				# prepare signals for all the slots
				if signal.name.find('CMCU_Bat') == 0:
					name = signal.name
					for i in range(batt_number):
						signal.name = name[:8] + str(i) + name[8:]
						model_signal = Signal(signal, model_message)
						model_message.signals.append(model_signal)
					signal.name = name
				else:
					model_signal = Signal(signal, model_message)
					model_message.signals.append(model_signal)

			self.model.messages.append(model_message)

	def add_listeners(self, func):
		can.Notifier(self.network, [ListenerSubclass(func)])

	def echo_CAN_msg(self, can_msg_id, can_msg_data):
		try:
			msg = can.Message(arbitration_id=can_msg_id, data=can_msg_data)
			# print(f'\n{hex(can_msg_id)}\n')
			self.network.send(msg)
		except Exception as err:
			logger.error(err)

	def decode_message(self, message_id, data):
		return self.remote_node.decode_message(message_id | 0x1FF80000, data)

	def get_message_name(self, message_id):
		return self.remote_node.get_message_by_frame_id(0x1FF80000 | message_id).name


	def split_decoded_msg(self, message):
		slot_num, node_id, message_id = self.__split_arbitration_id__(message.arbitration_id)
		target_batt_name = "batt_" + str(slot_num + 1)
		try:
			msg_name = self.get_message_name(message_id)
		except:
			pass
		try:
			if message.is_extended_id:
				msg_signals = self.decode_message(message_id, message.data) # contains full msg
				timestamp = message.timestamp
				logger.debug(f"Slot num: {slot_num} {msg_signals}")
				return slot_num, node_id, message_id, msg_signals, msg_name, timestamp
		except Exception as err:
			logger.exception(err)


	def __split_arbitration_id__(self, arbitration_id):
		slot_num = (arbitration_id >> BITPOS_SLOT_NUM) & 0x7
		device_id = (arbitration_id >> BITPOS_DEVICE_ID) & 0xFF
		message_id = arbitration_id & 0xFF
		return slot_num, device_id, message_id

	def __join_arbitration_id__(self, device_id, slot_num, message_id):
		arbitration_id = (slot_num << 16) | (device_id << 8) | message_id
		return arbitration_id

	def send_IPC_Cmd(self, value, slotNum=-1):
		message_model = self.model.getMessageByName('IPC_Cmd')
		message_model.getSignalByName('Cmd').value = value
		self.transmitCANMessage(message_model, slotNum)


	# def send_IPC_Set_CAN_ID(self, id):
	# 	# this is currently set via the broadcast ID 0
	# 	message_model = self.model.getMessageByName('IPC_Set_CAN_ID')
	# 	message_model.getSignalByName('IPC_Config_CAN_ID').value = id
	# 	self.transmitCANMessage(message_model)

	def send_IPC_Charge_Parameters_1(self, voltage_max_mV=50000, current_max_mA=4000, current_min_mA=20):
		message_model = self.model.getMessageByName('IPC_Charge_Parameters')
		message_model.getSignalByName('IPC_Charge_Voltage_Max').value = voltage_max_mV
		message_model.getSignalByName('IPC_Charge_Current_Max').value = current_max_mA
		message_model.getSignalByName('IPC_Charge_Current_Min').value = current_min_mA
		self.transmit_CAN_message(message_model)

	def send_IPC_Charge_Parameters_2(self, soc_max=100, temp_bat_max=45, temp_charger_max=60,
									 soc_max_mask=1, temp_bat_max_mask=1, temp_charger_max_mask=1):
		message_model = self.model.getMessageByName('IPC_Charge_Parameters_2')
		message_model.getSignalByName('IPC_Charge_SOC_Max').value = soc_max
		message_model.getSignalByName('IPC_Charge_Temp_Bat_Max').value = temp_bat_max
		message_model.getSignalByName('IPC_Charge_Temp_Charger_Max').value = temp_charger_max
		message_model.getSignalByName('IPC_Charge_SOC_Max_Mask').value = soc_max_mask
		message_model.getSignalByName('IPC_Charge_Temp_Bat_Max_Mask').value = temp_bat_max_mask
		message_model.getSignalByName('IPC_Charge_Temp_Charger_Max_Mask').value = temp_charger_max_mask
		self.transmit_CAN_message(message_model)

	def send_IPC_Config(self, ChargerType, BatteryType):
		message_model = self.model.getMessageByName('IPC_Set_Config')
		message_model.getSignalByName('CMCU_Config_Charger_Type').value = ChargerType
		message_model.getSignalByName('CMCU_Config_Battery_Type').value = BatteryType
		self.transmitCANMessage(message_model)

	def transmitCANMessage(self, message_model, slot_num=-1):
		try:
		
			message_prototype = self.remote_node.get_message_by_name(message_model.name)
			data_signals = {}
			for signal in message_model.signals:
				data_signals.update({signal.name: signal.value})
			data = message_prototype.encode(data_signals)
			if slot_num == -1:
				slot_num = 0
			canMessage = can.Message(
				arbitration_id=self.__join_arbitration_id__(self.can_id, slot_num, message_prototype.frame_id), data=data)
			self.network.send(canMessage)
		except Exception as err:
			print(traceback.format_exc())
			logger.exception(err)


class Model:
	def __init__(self):
		self.messages = list()

	def getMessageByName(self, name):
		for message in self.messages:
			if message.name == name:
				return message
		return None

	def getSignalByName(self, name):
		for message in self.messages:
			for signal in message.signals:
				if signal.name == name:
					return signal
		return None


class Message:
	def __init__(self, dbc_message):
		self.name = dbc_message.name
		self.value = None
		self.signals = list()

		if self.name.find('CMCU_') != -1:
			self.readOnly = True
		else:
			self.readOnly = False

	def getSignalByName(self, name):
		for signal in self.signals:
			if signal.name == name:
				return signal
		return None

	def package_payload(self, payload):
		for signal in payload:
			self.getSignalByName(signal).value = payload[signal]
		#         return signal
		# return None


class Signal:
	def __init__(self, dbc_signal, parent_model):
		self.name = dbc_signal.name

		if hasattr(dbc_signal, 'choices'):
			self.choices = dbc_signal.choices
		else:
			self.choices = None
		if hasattr(dbc_signal, 'unit'):
			self.unit = dbc_signal.unit
		else:
			self.unit = None

		self.value = None

		# readOnly if parent message was readOnly
		self.readOnly = parent_model.readOnly

	def getChoicesIndexByValue(self, value):
		if self.choices:
			for choice_index_key, choice_value in self.choices.items():
				if choice_value == value:
					return choice_index_key

		return None
