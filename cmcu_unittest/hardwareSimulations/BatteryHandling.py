from BatterieSimulator.battery_okai import BatteryOkai
from BatterieSimulator.battery_gp import BatteryGreenpack
import BatterieSimulator.can_setup as can_setup
import can

class BatteryHandling():

    def __init__(self, batt_type, channel='can1'):
        self.batt_type = batt_type
        self.channel = channel
        self.battery = None


    def reset_battery(self):
        self.remove_battery()
        del(self.battery)
        self.insert_battery()

    def insert_battery(self):
        self.can_bus = can_setup.init(bitrate=250000, channel=self.channel)
        self.battery = BatteryOkai(self.can_bus)
        self.can_notifier = can.Notifier(self.can_bus, [self.battery])

    def remove_battery(self):
        if self.battery != None:
            self.can_notifier.stop()
            self.can_bus.shutdown()
            self.battery.__reset_bat_errors__()