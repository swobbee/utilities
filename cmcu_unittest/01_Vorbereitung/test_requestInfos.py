import time
import pytest
import logging
import os
import sys
from datetime import datetime
root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../../")
sys.path.append(root_dir)
from cmcu_unittest.hardwareSimulations.cmcu_simulation import CMCU_DEVICE, return_test_config

logger = logging.getLogger(__name__)

CMCU_Settings, Test_Parameters = return_test_config()

class TestOnCmcuStart:
    baud_rate_bps = CMCU_Settings.get("baud_rate_bps")
    bus_id = CMCU_Settings.get("bus_id")
    node_id = CMCU_Settings.get("node_id")
    batt_number = CMCU_Settings.get("batt_number")
    FWV = CMCU_Settings.get("FWV")
    dbc_version = CMCU_Settings.get("dbc_version")
    IF_board = CMCU_Settings.get("IF_board")
    batt_type = CMCU_Settings.get("batt_type")
    charger_type = CMCU_Settings.get("charger_type")
    Serialnumber = CMCU_Settings.get("Serialnumber")
    IF_Serialnumber = CMCU_Settings.get("IF_Serialnumber")
    Charger_Serialnumber = CMCU_Settings.get("Charger_Serialnumber")


    def setup_class(self):
        self.setup_time = time.time()
        self.max_testing_time_s = Test_Parameters.get("max_testing_time_s")
        self.cmcu_heart_beat_frequency_s = Test_Parameters.get("cmcu_heartbeat_hz")


    def setup_method(self):
        time.sleep(0.1)
        self.cmcu_device = CMCU_DEVICE(interface_board_type = self.IF_board, baud_rate_bps=self.baud_rate_bps)
        self.cmcu_device.bus.send_IPC_Config(self.charger_type, self.batt_type)

    def test_request_serial_serial(self):
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('REQUEST_CMCUSERIAL')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv()
            slot_num, node_id, message_id, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
            if msg_name == 'CMCU_Serial':
                recvSerial = msg_signals.get("CMCU_Serial_Number")

                assert int(self.Serialnumber) == recvSerial
                break

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')


    def test_request_firmware(self):
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('REQUEST_CMCUINFO')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv()
            slot_num, node_id, message_id, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
            if msg_name == 'CMCU_Info':
                tmp_FW_major = msg_signals.get("CMCU_FW_Version_Major")
                tmp_FW_minor = msg_signals.get("CMCU_FW_Version_Minor")
                tmp_FW_patch = msg_signals.get("CMCU_FW_Version_Patch")
                recv_FW_version = f'{tmp_FW_major}_{tmp_FW_minor}_{tmp_FW_patch}'

                assert recv_FW_version == self.FWV
                break

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')


    def test_request_dbc(self):
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('REQUEST_CMCUINFO')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv()
            slot_num, node_id, message_id, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
            if msg_name == 'CMCU_Info':
                recvDBC_version = msg_signals.get("DBC_Version")
                assert recvDBC_version == self.dbc_version
                break

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')



    def test_request_batt_and_charger(self):
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('REQUEST_CMCUINFO')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv()
            slot_num, node_id, message_id, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
            if msg_name == 'CMCU_Info':
                recvBatt_type = msg_signals.get("CMCU_Config_Battery_Type")
                recvCharger_type = msg_signals.get("CMCU_Config_Charger_Type")
                assert (self.batt_type == recvBatt_type) & (self.charger_type == recvCharger_type)
                break

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')



    def test_request_if_board(self):
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('REQUEST_CMCUINFO')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv()
            slot_num, node_id, message_id, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
            if msg_name == 'CMCU_Info':
                recvIF_board = msg_signals.get("CMCU_Interface_Type")

                assert self.IF_board == recvIF_board
                break

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')

    def test_request_serial_if_baord(self):
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('REQUEST_IFSERIAL')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv()
            slot_num, node_id, message_id, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
            if msg_name == 'CMCU_IF_Serial':
                recvIF_serial = msg_signals.get("CMCU_IF_Serial_Number")

                assert int(self.IF_Serialnumber) == recvIF_serial
                break

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')

    def test_request_serial_charger(self):
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('REQUEST_CHARGERSERIAL')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv()
            slot_num, node_id, message_id, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
            if msg_name == 'CMCU_Charger_Serial' and self.charger_type == "RELAY":
                assert False
                break
            elif msg_name == 'CMCU_Charger_Serial':
                recvCharger_serial = msg_signals.get("CMCU_Serial_Number")
                assert self.Charger_Serialnumber == recvCharger_serial
                break
            elif msg_name == 'CMCU_Message_NAK':
                reason = msg_signals.get('CMCU_NAK_Reason')
                assert reason == "NOT_AVAILABLE"
                break

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')






