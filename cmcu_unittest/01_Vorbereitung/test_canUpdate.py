from multiprocessing.pool import ThreadPool
import time
import pytest
import logging
import sys
import os
import argparse


from datetime import datetime
root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../../")
sys.path.append(root_dir)
from cmcu_unittest.hardwareSimulations.cmcu_simulation import return_test_config 
from CmcuUpdater.multi_cmcu_updater import update

CMCU_Settings, Test_Parameters = return_test_config()
versions_nummer = CMCU_Settings.get("FWV")

path2hex = f'/home/pi/utilities/CmcuUpdater/hex_files/{versions_nummer}.hex'
logger = logging.getLogger(__name__)

bus = None

class TestCmcuCanUpdate:

    def setup_class(self):
        self.max_testing_time_S = 20
        self.start_time_of_test_case = None

    def setup_methode(self):
        os.system(f"sudo /sbin/ip link set can0 down")
        os.system(f"sudo /sbin/ip link set can0 up type can bitrate 250000")


    def test_successfull_update_1(self):        
        try:
            self.start_time_of_test_case = time.time()
            pool = ThreadPool(processes=1)
            async_result = pool.apply_async(update, (path2hex,))  # tuple of args for foo
            while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_S):
                return_val = async_result.get()
                if return_val != None:
                    assert return_val == True
                    break
            if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_S):
                pool.terminate()
                raise AssertionError(f'Time out............')
        except Exception as err:
            logger.exception(err)
            pool.terminate()
            raise AssertionError(f'Exception case')

    def test_successfull_update_2(self):
        try:
            time.sleep(15)
            self.start_time_of_test_case = time.time()
            pool = ThreadPool(processes=2)
            async_result = pool.apply_async(update, (path2hex,))  # tuple of args for foo
            while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_S):
                return_val = async_result.get()
                if return_val != None:
                    assert return_val == True
                    break
            if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_S):
                pool.terminate()
                raise AssertionError(f'Time out............')
        except Exception as err:
            logger.exception(err)
            pool.terminate()
            raise AssertionError(f'Exception case')

