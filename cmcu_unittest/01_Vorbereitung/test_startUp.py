import numpy as np
from datetime import datetime
import sys
import os
import logging
import pytest
import time

root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../../")
sys.path.append(root_dir)
from cmcu_unittest.hardwareSimulations.cmcu_simulation import CMCU_DEVICE, return_test_config

logger = logging.getLogger(__name__)

CMCU_Settings, Test_Parameters = return_test_config()


class TestOnCmcuStart:
    baud_rate_bps = CMCU_Settings.get("baud_rate_bps")
    bus_id = CMCU_Settings.get("bus_id")
    node_id = CMCU_Settings.get("node_id")
    batt_number = CMCU_Settings.get("batt_number")
    FWV = CMCU_Settings.get("FWV")
    dbc_version = CMCU_Settings.get("dbc_version")
    IF_board = CMCU_Settings.get("IF_board")
    batt_type = CMCU_Settings.get("batt_type")
    charger_type = CMCU_Settings.get("charger_type")
    Serialnumber = CMCU_Settings.get("Serialnumber")
    IF_Serialnumber = CMCU_Settings.get("IF_Serialnumber")
    Charger_Serialnumber = CMCU_Settings.get("Charger_Serialnumber")

    def setup_class(self):
        self.max_testing_time_s = Test_Parameters.get("max_testing_time_s")
        self.amount_of_ts_to_average =Test_Parameters.get("ts_average_amount")
        self.cmcu_heart_beat_frequency_s = Test_Parameters.get("cmcu_heartbeat_hz")
        
       
    def setup_method(self):
        time.sleep(0.1)
        self.cmcu_device = CMCU_DEVICE(interface_board_type = self.IF_board, baud_rate_bps=self.baud_rate_bps)
        self.cmcu_device.set_initial_state()

    def test_VOS_000_using_the_correct_dbc_version(self):
        used_dbc_version = self.cmcu_device.return_used_dbc_version()
        if self.dbc_version == used_dbc_version:
            assert True
        else:
            AssertionError(f"configured dbc Version is {self.dbc_version}, used dbc version is {used_dbc_version}")

    def test_VOS_001_cmcu_heartbeat(self):
        timestamp_list = []
        first_hb_skipped = False
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv()
            slot_num, node_id, message_id, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(
                msg)
            if msg_name == 'CMCU_State_Info':
                if first_hb_skipped:
                    # skip first heartbeat, since it is once send on startup outside of periodic timestamp function
                    timestamp_list.append(timestamp)
                    if len(timestamp_list) == self.amount_of_ts_to_average:
                        assert np.diff(timestamp_list).mean().round() == 1
                        break
                else:
                    first_hb_skipped = True

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')

    def test_VOS_002_firmware_on_start(self):
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv()
            slot_num, node_id, message_id, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(
                msg)
            if msg_name == 'CMCU_Info':
                tmp_FW_major = msg_signals.get("CMCU_FW_Version_Major")
                tmp_FW_minor = msg_signals.get("CMCU_FW_Version_Minor")
                tmp_FW_patch = msg_signals.get("CMCU_FW_Version_Patch")
                recv_FW_version = f'{tmp_FW_major}_{tmp_FW_minor}_{tmp_FW_patch}'

                assert recv_FW_version == self.FWV
                break

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')

    def test_VOS_003_dbc_on_start(self):
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv()
            slot_num, node_id, message_id, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(
                msg)
            if msg_name == 'CMCU_Info':
                recvDBC_version = msg_signals.get("DBC_Version")
                assert recvDBC_version == self.dbc_version
                break

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')

    def test_VOS_004_batt_and_charger_on_start(self):
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv()
            slot_num, node_id, message_id, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(
                msg)
            if msg_name == 'CMCU_Info':
                recvBatt_type = msg_signals.get("CMCU_Config_Battery_Type")
                recvCharger_type = msg_signals.get("CMCU_Config_Charger_Type")
                assert (self.batt_type == recvBatt_type) & (
                    self.charger_type == recvCharger_type)
                break

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')

    def test_VOS_005_if_board_type_on_start(self):
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv()
            slot_num, node_id, message_id, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(
                msg)
            if msg_name == 'CMCU_Info':
                recvIF_board = msg_signals.get("CMCU_Interface_Type")

                assert self.IF_board == recvIF_board
                break

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')

    def test_VOS_006_serial_on_start(self):
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv()
            slot_num, node_id, message_id, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(
                msg)
            if msg_name == 'CMCU_Serial':
                recvSerial = msg_signals.get("CMCU_Serial_Number")

                assert int(self.Serialnumber) == recvSerial
                break

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
            

    def test_VOS_006_ambient_temperature_connected_ping(self):
        first_temp_hb_skipped = False
        temperature_list = []
        timestamp_list = []

        self.cmcu_device.__set_temp_ext_connected__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')

        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv()
            _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(
                msg)
            if msg_name == 'CMCU_State_Info':
                if first_temp_hb_skipped:
                    # skip first heartbeat, since it is once send on startup outside of periodic timestamp function
                    recvTemperature = msg_signals.get("CMCU_Temp_Ext")
                    temperature_list.append(recvTemperature)
                    timestamp_list.append(timestamp)
                    if len(timestamp_list) == self.amount_of_ts_to_average:
                        assert (np.diff(timestamp_list).mean().round() == self.cmcu_heart_beat_frequency_s) and (
                            np.mean(temperature_list).round() != -30)
                        break
                else:
                    first_temp_hb_skipped = True
        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')

    def test_VOS_007_ambient_temperature_not_connected_ping(self):
        temperature_list = []
        timestamp_list = []
        first_temp_hb_skipped = False

        self.cmcu_device.__set_temp_ext_disconnected__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')

        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv()
            slot_num, node_id, message_id, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(
                msg)
            if msg_name == 'CMCU_State_Info':
                if first_temp_hb_skipped:
                    # skip first heartbeat, since it is once send on startup outside of periodic timestamp function
                    recvTemperature = msg_signals.get("CMCU_Temp_Ext")
                    temperature_list.append(recvTemperature)
                    timestamp_list.append(timestamp)
                    if len(timestamp_list) == self.amount_of_ts_to_average:
                        assert (np.diff(timestamp_list).mean().round() == self.cmcu_heart_beat_frequency_s) and \
                               (np.mean(temperature_list).round() == -30)
                        break
                else:
                    first_temp_hb_skipped = True
        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
