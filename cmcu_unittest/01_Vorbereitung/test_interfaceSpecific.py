import numpy as np
from datetime import datetime
import sys
import os
import logging
import pytest
import time
import json

root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../../")
sys.path.append(root_dir)
from cmcu_unittest.hardwareSimulations.cmcu_simulation import CMCU_DEVICE, return_test_config

CMCU_Settings, Test_Parameters = return_test_config()

logger = logging.getLogger(__name__)


class TestOnCmcuStart:
    baud_rate_bps = CMCU_Settings.get("baud_rate_bps")
    bus_id = CMCU_Settings.get("bus_id")
    node_id = CMCU_Settings.get("node_id")
    batt_number = CMCU_Settings.get("batt_number")
    FWV = CMCU_Settings.get("FWV")
    dbc_version = CMCU_Settings.get("dbc_version")
    IF_board = CMCU_Settings.get("IF_board")
    batt_type = CMCU_Settings.get("batt_type")
    charger_type = CMCU_Settings.get("charger_type")
    Serialnumber = CMCU_Settings.get("Serialnumber")
    IF_Serialnumber = CMCU_Settings.get("IF_Serialnumber")
    Charger_Serialnumber = CMCU_Settings.get("Charger_Serialnumber")

    def setup_class(self):
        self.cmcu_heart_beat_frequency_s = Test_Parameters.get("cmcu_heartbeat_hz")
        self.max_testing_time_ms = Test_Parameters.get("max_testing_time_s")          
        self.amount_of_ts_to_average =Test_Parameters.get("ts_average_amount")         
        

    def setup_method(self):
        time.sleep(0.1)
        self.cmcu_device = CMCU_DEVICE(interface_board_type = self.IF_board, baud_rate_bps=self.baud_rate_bps)


    def test_VOIF_001_if_temperatures_connected_on_start(self):
        self.start_time_of_test_case = time.time()
        self.cmcu_device.interface.connect_all_temperatures()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_ms):
            msg = self.cmcu_device.bus.network.recv()
            temperature_list = []
            _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(
                msg)
            if msg_name == 'CMCU_IF_Temperatures':
                for slot in range(len(msg_signals)):
                    recvTemperature = msg_signals.get(
                        f"CMCU_Temp_IF_Ext_{slot}")
                    temperature_list.append(recvTemperature)

                if -30 in temperature_list[:self.batt_number]:
                    # are any of the connected sensors -30?
                    raise AssertionError(
                        f'connected Sensor is -30°C\n{temperature_list}')
                elif np.mean(temperature_list[self.batt_number:]) != -30:
                    # are any of the not connected sensors not -30?
                    raise AssertionError(
                        f'not connected Sensor is not -30°C\n{temperature_list}')
                else:
                    assert True
                break

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_ms):
            raise AssertionError(f'Time out............')

    def test_VOIF_002_if_temperatures_not_connected_on_start(self):
        self.start_time_of_test_case = time.time()
        self.cmcu_device.interface.disconnect_all_temperatures()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_ms):
            msg = self.cmcu_device.bus.network.recv()
            temperature_list = []
            _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(
                msg)
            if msg_name == 'CMCU_IF_Temperatures':
                for slot in range(len(msg_signals)):
                    recvTemperature = msg_signals.get(
                        f"CMCU_Temp_IF_Ext_{slot}")
                    temperature_list.append(recvTemperature)

                if np.mean(temperature_list) != -30:
                    # are any of the not connected sensors not -30?
                    raise AssertionError(
                        f'Sensor not -30°C although no sensor should be connected\n{temperature_list}')
                    break
                else:
                    assert True
                    break

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_ms):
            raise AssertionError(f'Time out............')

    def test_VOIF_003_if_temperatures_on_change_1hz_max(self):
        raise AssertionError("Not implemented yet")
        self.start_time_of_test_case = time.time()
        self.cmcu_device.interface.disconnect_all_temperatures()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_ms):
            msg = self.cmcu_device.bus.network.recv()
            temperature_list = []
            _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(
                msg)
            if msg_name == 'CMCU_IF_Temperatures':
                pass

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_ms):
            raise AssertionError(f'Time out............')