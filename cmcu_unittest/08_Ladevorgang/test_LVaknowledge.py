import time
import pytest
import numpy as np
import logging
import os
import sys

from datetime import datetime
root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../../")
sys.path.append(root_dir)
from cmcu_unittest.hardwareSimulations.cmcu_simulation import CMCU_DEVICE, return_test_config 
from cmcu_unittest.hardwareSimulations.BatteryHandling import BatteryHandling  

logger = logging.getLogger(__name__)

CMCU_Settings, Test_Parameters = return_test_config()



initial_bat_data =      {
                            "CMCU_Bat_Serial":
                                {"CMCU_Bat_Serial_Number":878082288},
                            "CMCU_Bat_Info":
                                {
                                    "CMCU_Bat_SOH":100,
                                    "CMCU_Bat_FW_Version":0,
                                    "CMCU_Bat_Charge_Cycles":1
                                },

                            "CMCU_Bat_State":
                                {
                                "CMCU_Bat_Voltage":48000,
                                "CMCU_Bat_Current":0,
                                "CMCU_Bat_SOC":20,
                                "CMCU_Bat_Temp_Cell_Min":20.0,
                                "CMCU_Bat_Temp_Cell_Max":21.0,
                                "CMCU_Bat_State_Output_Enabled":1,
                                "CMCU_Bat_State_Input_Enabled":0,
                                "CMCU_Bat_State_Deep_Discharge":0,  
                                "CMCU_Bat_State_Error":0
                                }
                        }



serialnbr = 0x123456789ABCDEF0
soc = 20
soh = 100
voltage = 48000
current = 0
temperature_min = 20
temperature_max = 21
cycles = 10

class TestLadevorgang:
    baud_rate_bps = CMCU_Settings.get("baud_rate_bps")
    bus_id = CMCU_Settings.get("bus_id")
    node_id = CMCU_Settings.get("node_id")
    batt_number = CMCU_Settings.get("batt_number")
    FWV = CMCU_Settings.get("FWV")
    dbc_version = CMCU_Settings.get("dbc_version")
    IF_board = CMCU_Settings.get("IF_board")
    batt_type = CMCU_Settings.get("batt_type")
    charger_type = CMCU_Settings.get("charger_type")
    Serialnumber = CMCU_Settings.get("Serialnumber")
    IF_Serialnumber = CMCU_Settings.get("IF_Serialnumber")
    Charger_Serialnumber = CMCU_Settings.get("Charger_Serialnumber")

    def setup_class(self):
        self.amount_of_ts_to_average =Test_Parameters.get("ts_average_amount")
        self.max_testing_time_s = Test_Parameters.get("max_testing_time_s")
        self.test_slots = Test_Parameters.get("test_slots")[0]
        self.recv_timeout = Test_Parameters.get("recv_timeout")

        
    def setup_method(self):
        time.sleep(0.1)
        self.cmcu_device = CMCU_DEVICE(interface_board_type = self.IF_board, baud_rate_bps=self.baud_rate_bps)
        self.battery_handler = BatteryHandling(batt_type=self.batt_type)

    def teardown_method(self):
        self.battery_handler.remove_battery()

    def set_battery_data(self):
        self.battery_handler.battery.serialnbr = serialnbr
        self.battery_handler.battery.soc = soc
        self.battery_handler.battery.soh = soh
        self.battery_handler.battery.voltage = voltage
        self.battery_handler.battery.current = current
        self.battery_handler.battery.temperature_min = temperature_min
        self.battery_handler.battery.temperature_max = temperature_max
        self.battery_handler.battery.cycles = cycles
        


    
    def test_LVAK_001_Batterydata_after_init(self):

        ### evtl insert battery data in connect battery refractoren und den kompletten block als insert_battery in eine funktion packen
        self.cmcu_device.__set_door_open__()
        self.battery_handler.insert_battery()
        self.set_battery_data()
        self.cmcu_device.__set_door_closed__()
        ######################################




        init_done = False
        all_msg_received = False
        
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_name == 'CMCU_State_Info' and msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True

            except:
                pass

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
        self.battery_handler.remove_battery()