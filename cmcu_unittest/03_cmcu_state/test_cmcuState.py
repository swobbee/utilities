import time
import pytest
import logging
import os
import sys

from datetime import datetime
root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../../")
sys.path.append(root_dir)
from cmcu_unittest.hardwareSimulations.cmcu_simulation import CMCU_DEVICE, return_test_config 

logger = logging.getLogger(__name__)

CMCU_Settings, Test_Parameters = return_test_config()

class TestCmcuState:
    baud_rate_bps = CMCU_Settings.get("baud_rate_bps")
    bus_id = CMCU_Settings.get("bus_id")
    node_id = CMCU_Settings.get("node_id")
    batt_number = CMCU_Settings.get("batt_number")
    FWV = CMCU_Settings.get("FWV")
    dbc_version = CMCU_Settings.get("dbc_version")
    IF_board = CMCU_Settings.get("IF_board")
    batt_type = CMCU_Settings.get("batt_type")
    charger_type = CMCU_Settings.get("charger_type")
    Serialnumber = CMCU_Settings.get("Serialnumber")
    IF_Serialnumber = CMCU_Settings.get("IF_Serialnumber")
    Charger_Serialnumber = CMCU_Settings.get("Charger_Serialnumber")


    def setup_class(self):
        self.max_testing_time_ms = Test_Parameters.get("max_testing_time_s")
        self.cmcu_device = CMCU_DEVICE(interface_board_type = self.IF_board, baud_rate_bps=self.baud_rate_bps)
        self.cmcu_device.bus.send_IPC_Config(self.charger_type, self.batt_type)
        
    def setup_method(self):
        time.sleep(0.1)
        self.cmcu_device.set_initial_state()

    def test_CS_001_cmcu_init_state(self):
        reset_done = False
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_ms):
            try:
                msg = self.cmcu_device.bus.network.recv()
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
                if msg_name == 'CMCU_Info':
                    reset_done = True

                elif msg_name == 'CMCU_State_Info' and reset_done == True:
                    assert msg_signals.get('CMCU_State') == 'INIT'
                    break
            except Exception as e:
                logger.exception(e)

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_ms):
            raise AssertionError(f'Time out............')

    def test_CS_002_cmcu_state_on_system_start_no_error_door_closed(self):
        init_done = False
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_ms):
            try:
                msg = self.cmcu_device.bus.network.recv()
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_name == 'CMCU_State_Info' and init_done:
                    assert msg_signals.get('CMCU_State') == 'OK_DOOR_CLOSED'
                    break
                elif msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True
            except Exception as e:
                logger.exception(e)
        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_ms):
            raise AssertionError(f'Time out............')

    def test_CS_003_cmcu_state_on_system_start_no_error_door_open(self):
        self.cmcu_device.__set_door_open__()
        time.sleep(1)
        init_done = False
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_ms):
            try:
                msg = self.cmcu_device.bus.network.recv()
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_name == 'CMCU_State_Info' and init_done:
                    assert msg_signals.get('CMCU_State') == 'OK_DOOR_OPEN'
                    break
                elif msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True
            except Exception as e:
                logger.exception(e)

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_ms):
            raise AssertionError(f'Time out............')

    @pytest.mark.skipif(dbc_version >= 20,
                reason="requires dbc v. < 20")
    def test_CS_004_cmcu_state_on_system_start_with_error(self):
        init_done = False
        self.start_time_of_test_case = time.time()
        self.cmcu_device.__set_door_error__()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_ms):
            try:
                msg = self.cmcu_device.bus.network.recv()
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_name == 'CMCU_State_Info' and init_done:
                    assert msg_signals.get('CMCU_State') == 'ERROR'
                    break
                elif msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True
            except Exception as e:
                logger.exception(e)

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_ms):
            raise AssertionError(f'Time out............')

    @pytest.mark.skipif(dbc_version < 20,
                    reason="requires dbc v. >= 20")
    def test_CS_005_cmcu_state_on_system_start_with_error_door_closed(self):
        init_done = False
        self.cmcu_device.__set_door_closed__()
        self.start_time_of_test_case = time.time()
        self.cmcu_device.__set_door_error__()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_ms):
            try:
                msg = self.cmcu_device.bus.network.recv()
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True
                elif msg_name == 'CMCU_State_Info' and init_done:
                    assert msg_signals.get('CMCU_State') == 'ERROR_DOOR_CLOSED'
                    break
            except Exception as e:
                logger.exception(e)

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_ms):
            raise AssertionError(f'Time out............')
        self.cmcu_device.__set_door_closed__()


    @pytest.mark.skipif(dbc_version < 20,
                reason="requires dbc v. >= 20")
    def test_CS_006_cmcu_state_on_system_start_with_error_door_open(self):
        init_done = False
        error_set = False
        reset_done = False
        self.cmcu_device.__set_door_open__()
        
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_ms):
            try:
                msg = self.cmcu_device.bus.network.recv()
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
                
                if msg_signals.get('CMCU_State') == 'OK_DOOR_OPEN' and not error_set:
                    self.cmcu_device.__set_door_error__()
                    error_set = True
                elif msg_signals.get('CMCU_State') == 'ERROR_DOOR_OPEN' and not reset_done and error_set:
                    self.cmcu_device.bus.send_IPC_Cmd('RESET')
                    reset_done = True
                elif msg_signals.get('CMCU_State') == 'INIT' and not init_done:
                    init_done = True
                elif msg_name == 'CMCU_State_Info' and init_done:
                    assert msg_signals.get('CMCU_State').name.find("ERROR")!=0
                    break
            except Exception as e:
                logger.exception(e)

        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_ms):
            raise AssertionError(f'Time out............')

