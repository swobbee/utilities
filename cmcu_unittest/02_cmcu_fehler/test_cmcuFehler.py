import time
import pytest
import logging
import os
import sys

from datetime import datetime
root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../../")
sys.path.append(root_dir)
from cmcu_unittest.hardwareSimulations.cmcu_simulation import CMCU_DEVICE, return_test_config
from cmcu_unittest.hardwareSimulations.BatteryHandling import BatteryHandling

logger = logging.getLogger(__name__)

CMCU_Settings, Test_Parameters = return_test_config()

class TestCmcuFehler:
    baud_rate_bps = CMCU_Settings.get("baud_rate_bps")
    bus_id = CMCU_Settings.get("bus_id")
    node_id = CMCU_Settings.get("node_id")
    batt_number = CMCU_Settings.get("batt_number")
    FWV = CMCU_Settings.get("FWV")
    dbc_version = CMCU_Settings.get("dbc_version")
    IF_board = CMCU_Settings.get("IF_board")
    batt_type = CMCU_Settings.get("batt_type")
    charger_type = CMCU_Settings.get("charger_type")
    Serialnumber = CMCU_Settings.get("Serialnumber")
    IF_Serialnumber = CMCU_Settings.get("IF_Serialnumber")
    Charger_Serialnumber = CMCU_Settings.get("Charger_Serialnumber")


    def setup_class(self):
        self.max_testing_time_s = Test_Parameters.get("max_testing_time_s")
        self.cmcu_heart_beat_frequency_s = Test_Parameters.get("cmcu_heartbeat_hz") 
        self.recv_timeout = Test_Parameters.get("recv_timeout_s") 
        self.test_slots = Test_Parameters.get("test_slots")[0]

    def setup_method(self):
        time.sleep(0.1)
        self.cmcu_device = CMCU_DEVICE(interface_board_type = self.IF_board, baud_rate_bps=self.baud_rate_bps)
        self.cmcu_device.bus.send_IPC_Config(self.charger_type, self.batt_type)

    def test_CF_001_cmcu_error_type_on_error(self):
        # set error charger no type, when test done reset charger type to config charger
        # expected error: CMCU_ERROR_CHARGER_NO_TYPE
        reset_done = False
        init_done = False
        start_time_of_test_case = time.time()
        self.cmcu_device.__set_door_closed__()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv()
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
    
                if msg_signals.get('CMCU_State') == 'INIT' and not init_done:
                    self.cmcu_device.bus.send_IPC_Config('UNCONFIGURED', self.batt_type)
                    init_done = True

                if msg_name=='CMCU_Error' and init_done:
                    assert msg_signals.get("CMCU_ERROR_CHARGER_NO_TYPE") == 1
                    break
            except Exception as e:
                logger.exception(e)                   

        if ((time.time() - start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')

    def test_CF_002_cmcu_stays_in_error_state_on_error_change(self):
        init_done = False
        error_set = False
        start_time_of_test_case = time.time()
        self.cmcu_device.__set_door_closed__()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv()
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
    
                if msg_signals.get('CMCU_State') == 'INIT' and not init_done:
                    self.cmcu_device.bus.send_IPC_Config('UNCONFIGURED', self.batt_type)
                    init_done = True

                elif msg_signals.get("CMCU_ERROR_CHARGER_NO_TYPE") == 1 and init_done:
                    error_set = True
                    self.cmcu_device.__set_door_error__()

                elif msg_name == 'CMCU_State_Info' and error_set:
                    assert msg_signals.get('CMCU_State').name.find('ERROR') != -1
                    break                     
            except Exception as e:
                logger.exception(e)
        if ((time.time() - start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')

    def test_CF_003_cmcu_leaves_error_state_on_reset_if_no_active_error_present(self):
        error_reset_sent = False
        init_done = False
        door_state_received = False
        start_time_of_test_case = time.time()
        self.cmcu_device.__set_door_closed__()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv()
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
    
                if msg_signals.get('CMCU_State') == 'INIT' and not init_done and not error_reset_sent:
                    self.cmcu_device.__set_door_manipulated__()
                    init_done = True

                elif (msg_signals.get('CMCU_State').name.find('ERROR') != -1) and init_done and not error_reset_sent:
                    self.cmcu_device.__set_door_closed__()
                    self.cmcu_device.bus.send_IPC_Cmd("RESET")
                    error_reset_sent = True
                
                elif msg_name == 'CMCU_State_Info' and error_reset_sent:
                    if not msg_signals.get('CMCU_State') == "INIT":
                        assert msg_signals.get('CMCU_State') == "OK_DOOR_CLOSED"
                        break            
            except Exception as e:
                logger.exception(e)
        if ((time.time() - start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
    
    def test_CF_004_cmcu_stays_in_error_state_on_reset_if_a_active_error_is_present(self):
        error_reset_sent = False
        init_done = False
        door_state_received = False
        start_time_of_test_case = time.time()
        self.cmcu_device.__set_door_closed__()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv()
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
    
                if msg_signals.get('CMCU_State') == 'INIT' and not init_done and not error_reset_sent:
                    self.cmcu_device.__set_door_manipulated__()
                    init_done = True

                elif (msg_signals.get('CMCU_State').name.find('ERROR') != -1) and init_done and not error_reset_sent:
                    self.cmcu_device.bus.send_IPC_Cmd("RESET")
                    error_reset_sent = True
                
                elif msg_name == 'CMCU_State_Info' and error_reset_sent:
                    if not msg_signals.get('CMCU_State') == "INIT":
                        assert msg_signals.get('CMCU_State').name.find('ERROR') != -1
                        break            
            except Exception as e:
                logger.exception(e)
        if ((time.time() - start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
    
    def test_CF_005_cmcu_stays_in_error_state_on_reset_if_a_different_error_is_present(self):
        error_reset_sent = False
        init_done = False
        door_state_received = False
        start_time_of_test_case = time.time()
        self.cmcu_device.__set_door_closed__()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv()
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)
    
                if msg_signals.get('CMCU_State') == 'INIT' and not init_done and not error_reset_sent:
                    self.cmcu_device.__set_door_manipulated__()
                    init_done = True

                elif (msg_signals.get('CMCU_State').name.find('ERROR') != -1) and init_done and not error_reset_sent:
                    self.cmcu_device.__set_door_error__()
                    self.cmcu_device.bus.send_IPC_Cmd("RESET")
                    error_reset_sent = True
                
                elif msg_name == 'CMCU_State_Info' and error_reset_sent:
                    if not msg_signals.get('CMCU_State') == "INIT":
                        assert msg_signals.get('CMCU_State').name.find('ERROR') != -1
                        break            
            except Exception as e:
                logger.exception(e)
        if ((time.time() - start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
    
    def test_CF_006_cmcu_stop_all_charging_if_error_is_present(self):
        self.battery_handler = BatteryHandling(batt_type=self.batt_type)
        self.battery_handler.insert_battery()
        init_done = False
        charge_cmd_sent = False
        charging_started = False
        error_set = False
        self.cmcu_device.__set_door_closed__()
        start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while ((time.time() - start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if (msg_signals.get('CMCU_State')=='OK_DOOR_CLOSED') and not charge_cmd_sent:
                    active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][:self.batt_number]
                    if not 'STORAGE' in active_slots:
                        raise AssertionError(f'No Battery in Slot present')
                        break
                    else:
                        self.cmcu_device.bus.send_IPC_Cmd('CHARGE_START', slotNum=0)
                        charge_cmd_sent = True

                elif msg_name == 'CMCU_State_Info' and charge_cmd_sent and not charging_started:
                    active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][:self.batt_number]
                    test_slot_state = active_slots[self.test_slots]
                    if test_slot_state == "CHARGING":
                        charging_started = True

                elif charging_started and not error_set:
                    self.cmcu_device.__set_door_manipulated__()
                    error_set = True
                
                elif msg_name == 'CMCU_State_Info' and error_set:
                    active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][:self.batt_number]
                    test_slot_state = active_slots[self.test_slots]
                    if test_slot_state == "CHARGING":
                        assert False
                    else:
                        assert True
                    break

            except Exception as e:
                logger.exception(e)

        if ((time.time() - start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')