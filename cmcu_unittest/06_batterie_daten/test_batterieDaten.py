import time
import pytest
import numpy as np
import logging
import os
import sys

from datetime import datetime
root_dir = os.path.abspath(os.path.realpath(__file__) + "/../../../")
sys.path.append(root_dir)
from cmcu_unittest.hardwareSimulations.cmcu_simulation import CMCU_DEVICE, return_test_config 
from cmcu_unittest.hardwareSimulations.BatteryHandling import BatteryHandling  

logger = logging.getLogger(__name__)

CMCU_Settings, Test_Parameters = return_test_config()



initial_bat_data =      {
                            "CMCU_Bat_Serial":
                                {"CMCU_Bat_Serial_Number":878082288},
                            "CMCU_Bat_Info":
                                {
                                    "CMCU_Bat_SOH":100,
                                    "CMCU_Bat_FW_Version":0,
                                    "CMCU_Bat_Charge_Cycles":1
                                },

                            "CMCU_Bat_State":
                                {
                                "CMCU_Bat_Voltage":48000,
                                "CMCU_Bat_Current":0,
                                "CMCU_Bat_SOC":20,
                                "CMCU_Bat_Temp_Cell_Min":20.0,
                                "CMCU_Bat_Temp_Cell_Max":21.0,
                                "CMCU_Bat_State_Output_Enabled":1,
                                "CMCU_Bat_State_Input_Enabled":0,
                                "CMCU_Bat_State_Deep_Discharge":0,  
                                "CMCU_Bat_State_Error":0
                                }
                        }



serialnbr = 0x123456789ABCDEF0
soc = 20
soh = 100
voltage = 48000
current = 0
temperature_min = 20
temperature_max = 21
cycles = 10

class TestBatteryData:
    baud_rate_bps = CMCU_Settings.get("baud_rate_bps")
    bus_id = CMCU_Settings.get("bus_id")
    node_id = CMCU_Settings.get("node_id")
    batt_number = CMCU_Settings.get("batt_number")
    FWV = CMCU_Settings.get("FWV")
    dbc_version = CMCU_Settings.get("dbc_version")
    IF_board = CMCU_Settings.get("IF_board")
    batt_type = CMCU_Settings.get("batt_type")
    charger_type = CMCU_Settings.get("charger_type")
    Serialnumber = CMCU_Settings.get("Serialnumber")
    IF_Serialnumber = CMCU_Settings.get("IF_Serialnumber")
    Charger_Serialnumber = CMCU_Settings.get("Charger_Serialnumber")

    def setup_class(self):
        self.amount_of_ts_to_average =Test_Parameters.get("ts_average_amount")
        self.max_testing_time_s = Test_Parameters.get("max_testing_time_s")
        self.test_slots = Test_Parameters.get("test_slots")[0]
        self.recv_timeout = Test_Parameters.get("recv_timeout")

        
    def setup_method(self):
        time.sleep(0.1)
        self.cmcu_device = CMCU_DEVICE(interface_board_type = self.IF_board, baud_rate_bps=self.baud_rate_bps)
        self.battery_handler = BatteryHandling(batt_type=self.batt_type)

    def teardown_method(self):
        self.battery_handler.remove_battery()

    def set_battery_data(self):
        self.battery_handler.battery.serialnbr = serialnbr
        self.battery_handler.battery.soc = soc
        self.battery_handler.battery.soh = soh
        self.battery_handler.battery.voltage = voltage
        self.battery_handler.battery.current = current
        self.battery_handler.battery.temperature_min = temperature_min
        self.battery_handler.battery.temperature_max = temperature_max
        self.battery_handler.battery.cycles = cycles
        

    def test_BD_001_Batterydata_after_init(self):
        self.cmcu_device.__set_door_open__()
        self.battery_handler.insert_battery()
        self.set_battery_data()
        self.cmcu_device.__set_door_closed__()


        init_done = False
        all_msg_received = False
        # serial_received = False
        # bat_info_received = False
        # bat_state_received = False
        # bat_data_received = False

        boolian_list = []
        
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_name == 'CMCU_State_Info' and msg_signals.get('CMCU_State') == 'INIT':
                    init_done = True

                if init_done:
                    for key in initial_bat_data.keys():
                        if msg_name ==  key:
                            for signal in msg_signals:
                                if msg_signals[signal] == initial_bat_data[key][signal]:
                                    boolian_list.append(True)
                                else:
                                    boolian_list.append(False)

                        if msg_name == "CMCU_Bat_State":
                            all_msg_received = True
                if all_msg_received:
                    assert all(boolian_list)
                    break
            except:
                pass



        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
        self.battery_handler.remove_battery()

    
    def test_BD_002_request_Batterydata(self):
        self.battery_handler.insert_battery()
        req_bat_data_sent = False
        self.cmcu_device.__set_door_open__()
        self.start_time_of_test_case = time.time()

        boolian_list = []

        all_msg_received = False
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            try:
                msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
                slot_num, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

                if msg_name == 'CMCU_State_Info' and not req_bat_data_sent:
                    self.cmcu_device.bus.send_IPC_Cmd('REQUEST_BATDATA', slotNum=self.test_slots)
                    req_bat_data_sent = True

                # if msg_name 
                if req_bat_data_sent and msg_name in list(initial_bat_data.keys()):
                    for key in initial_bat_data.keys():
                        if msg_name ==  key:
                            for signal in msg_signals:
                                if msg_signals[signal] == initial_bat_data[key][signal]:
                                    boolian_list.append(True)
                                else:
                                    boolian_list.append(False)

                        if msg_name == "CMCU_Bat_State":
                            all_msg_received = True
                if all_msg_received:
                    assert all(boolian_list)
                    break
            except:
                pass

            


        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
        self.battery_handler.remove_battery()

    def test_BD_003_Batterydata_state_change_empty_to_storage(self):
        # 1. open door
        # 2. check if slot is empty
        # 3. insert battery
        # 4. assert Batdata
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        boolian_list = []
        init_done = False
        empty_slot_recognized = False
        bat_inserted = False
        bat_data_received = False
        all_msg_received = False

        self.start_time_of_test_case = time.time()
        
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
            _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

            if msg_name == 'CMCU_State_Info' and msg_signals.get('CMCU_State') == 'INIT':
                init_done = True
            
            if init_done and msg_name == 'CMCU_State_Info' and not empty_slot_recognized:
                slot_states = [msg_signals.get(value) for value in msg_signals if "Slot" in value]
                active_slots = slot_states[:self.batt_number]
                empty_slot_recognized = all(slot_state == 'EMPTY' for slot_state in active_slots)

                if empty_slot_recognized:
                    self.cmcu_device.__set_door_open__()
                    self.battery_handler.insert_battery()
                    bat_inserted = True

            if bat_inserted and msg_name in list(initial_bat_data.keys()):
                for key in initial_bat_data.keys():
                    if msg_name ==  key:
                        for signal in msg_signals:
                            if msg_signals[signal] == initial_bat_data[key][signal]:
                                boolian_list.append(True)
                            else:
                                boolian_list.append(False)

                    if msg_name == "CMCU_Bat_State":
                        all_msg_received = True
            if all_msg_received:
                assert all(boolian_list)
                break




        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
        

        
    def test_BD_004_Batterydata_heartbeat_on_charging(self):
        # 1. open door
        # 2. inert Battery
        # 3. close door
        # 4. start charging
        # 5. check heartbeats
        self.cmcu_device.__set_door_open__()
        self.battery_handler.insert_battery()
        self.cmcu_device.__set_door_closed__()
        init_done = False
        charging_started = False
        charge_cmd_sent = False
        timestamp_list = []
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
            _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

            if msg_name == 'CMCU_State_Info' and msg_signals.get('CMCU_State') == 'INIT':
                init_done = True

            elif msg_name == 'CMCU_State_Info' and init_done and not charging_started:

                    if (msg_signals.get('CMCU_State') == 'OK_DOOR_CLOSED') and not charge_cmd_sent:
                        # test if battery is present
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                    :self.batt_number]
                        test_slot_state = active_slots[self.test_slots]
                        if test_slot_state != 'STORAGE':
                            raise AssertionError(f'Slot State != STORAGE; current State is: {test_slot_state}')
                            break
                        else:
                            self.cmcu_device.bus.send_IPC_Cmd('CHARGE_START', slotNum=self.test_slots)
                            charge_cmd_sent = True

                    elif msg_name == 'CMCU_State_Info' and charge_cmd_sent and not charging_started:
                        # test if charging started
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                    :self.batt_number]
                        test_slot_state = active_slots[self.test_slots]
                        if test_slot_state == "CHARGING":
                            charging_started = True
            
            elif charging_started:
                if msg_name == 'CMCU_Bat_State':
                    timestamp_list.append(timestamp)
                    if len(timestamp_list) == self.amount_of_ts_to_average:
                        assert np.diff(timestamp_list).mean().round() == 1
                        break
 



        



        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
        self.battery_handler.remove_battery()

    def test_BD_005_Battery_raw_error_forwarding(self):
        self.cmcu_device.__set_door_open__()
        self.battery_handler.insert_battery()
        self.cmcu_device.__set_door_closed__()
        init_done = False
        bat_data_received = False
        slot_error_provoked = False
        bat_data_requested = False
        
        self.start_time_of_test_case = time.time()
        self.cmcu_device.bus.send_IPC_Cmd('RESET')
        while  ((time.time() - self.start_time_of_test_case) <= self.max_testing_time_s):
            msg = self.cmcu_device.bus.network.recv(timeout=self.recv_timeout)
            _, _, _, msg_signals, msg_name, timestamp = self.cmcu_device.bus.split_decoded_msg(msg)

            if msg_name == 'CMCU_State_Info' and msg_signals.get('CMCU_State') == 'INIT':
                init_done = True

            elif msg_name == 'CMCU_State_Info' and init_done and not slot_error_provoked:
                    if (msg_signals.get('CMCU_State') == 'OK_DOOR_CLOSED'):
                        # test if battery is present
                        active_slots = [msg_signals.get(value) for value in msg_signals if "Slot" in value][
                                    :self.batt_number]
                        test_slot_state = active_slots[self.test_slots]
                        if test_slot_state != 'STORAGE':
                            raise AssertionError(f'Slot State != STORAGE; current State is: {test_slot_state}')
                            break
                        else:
                            error_name = list(self.battery_handler.battery.errors.keys())[0]
                            self.battery_handler.battery.errors[error_name] = True
                            slot_error_provoked = True
            elif slot_error_provoked and not bat_data_requested:
                self.cmcu_device.bus.send_IPC_Cmd('REQUEST_BATDATA', slotNum=self.test_slots)
                bat_data_requested = True
            
            elif bat_data_requested:
                if msg_name.find("Raw") != -1:
                    assert True
                    break



        if ((time.time() - self.start_time_of_test_case) > self.max_testing_time_s):
            raise AssertionError(f'Time out............')
        self.battery_handler.remove_battery()