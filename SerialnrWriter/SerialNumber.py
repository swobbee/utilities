
import os
import sys

def print_error(e):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(f'{e} error of type {exc_type} in {fname} line {exc_tb.tb_lineno}')

class Serialnr():

    serialParts = []
    serialNumber = ''

    def __init__(self, name, length, position):
        self.serialParts.append(self)
        self.name = name
        self.length = length  # amount digits the part should have
        self.position = position  # where the part is supposed to be possitioned within the serialnr.
        self.value = int(0)
        self.serialRepresentation = None

    def setValue(self, value):
        self.value = int(value)

    def getValue(self):
        return self.value

    def convert2Serial(self):
        # self.serialRepresentation = int.to_bytes(self.value,
        #                                          length=self.length,
        #                                          byteorder='big',
        #                                          signed=False)
        self.serialRepresentation = str(self.value).zfill(self.length)


    def setSerial(self, value):
        try:
            self.setValue(value)
            self.convert2Serial()

        except Exception as e:
            self.setValue(0)
            self.convert2Serial()
            # todo raise value error

    def getSerial(self):
        return(self.serialRepresentation)


    @classmethod
    def createSerialnr(cls):
        ''' sorts the serial parts by given order and combines them to one serial number '''
        try:
            import operator
        except ImportError:
              keyfun = lambda x: x.count
        else:
            keyfun = operator.attrgetter("position")

        Serialnr.serialNumber = ''
        sortedParts = sorted(Serialnr.serialParts, key=keyfun, reverse=False)
        try:
            for part in sortedParts:
                Serialnr.serialNumber += part.serialRepresentation
                print(part.name, part.serialRepresentation)
            Serialnr.serialNumber = int(Serialnr.serialNumber)
        except Exception as e:
            print_error(e)