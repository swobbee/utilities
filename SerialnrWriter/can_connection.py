import can
import cantools
import can_setup

def split_arbitration_id(arbitration_id):
	is_high_prio 	= not ((arbitration_id >> 28) & 0x01)
	is_from_cmcu 	= (arbitration_id >> 24) & 0x01
	slotNum			= (arbitration_id >> 16) & 0xFF
	device_id 		= (arbitration_id >> 8) & 0xFF
	message_id 		= arbitration_id & 0xFF
	return slotNum, device_id, message_id

def join_arbitration_id(device_id, slotNum, message_id):
	is_high_prio = 0
	arbitration_id = (is_high_prio << 28) | (0 << 24) | (slotNum << 16) | (device_id << 8 ) | message_id
	return arbitration_id

def restartCAN_BUS(baudrate):
    global can_bus
    if (can_bus):
        can_bus.reset()
        can_bus.shutdown()
        can_bus = can_setup.reinit(bitrate=baudrate)

def startCAN_BUS():
    global can_bus
    can_bus = can_setup.init(bitrate=250000)