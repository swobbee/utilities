import os
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QMainWindow, QAction, qApp, QApplication, QWidget, QGridLayout, QGroupBox, QPushButton, QVBoxLayout, QHBoxLayout, QLabel
from PyQt5.QtGui import QIcon, QFont, QPixmap
from PyQt5 import uic
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt
from datetime import date
from SerialNumber import Serialnr
import csv
import can
import cantools
import model
import can_setup
import sys
import pandas as pd
import os



def print_error(e):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(f'{e} error of type {exc_type} in {fname} line {exc_tb.tb_lineno}')

def split_arbitration_id(arbitration_id):
	is_high_prio 	= not ((arbitration_id >> 28) & 0x01)
	is_from_cmcu 	= (arbitration_id >> 24) & 0x01
	slotNum			= (arbitration_id >> 16) & 0xFF
	device_id 		= (arbitration_id >> 8) & 0xFF
	message_id 		= arbitration_id & 0xFF
	return slotNum, device_id, message_id

class ListenerSubclass(can.Listener):
    def __init__(self, on_receive):
        self._on_receive = on_receive

    def on_message_received(self, msg):
        self._on_receive(msg)


class MainWindow(QMainWindow):
    model = model.Model()
    print_to_console = QtCore.pyqtSignal(str)


    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        uic.loadUi("mainwindow2.ui", self)
        today = date.today()
        self.transmit_clicked_bool = False
        self.date_string = today.strftime("%d.%m.%y")

        self.tablePath = r"M:\SOFTWARE\Eigene Anwendungen\CMCU_SerienNr_Tool\SN\Seriennummern.csv"
        #self.tablePath = r"C:\Users\p.goldmann\PycharmProjects\utilities\SerialnrWriter\table.csv"

        self.l_filepath.setText(f'{self.l_filepath.text()}\t{self.tablePath}')

        self.colnames    = "Serial Date Board HWV pDate manuf runningnr".split()
        self.type        = Serialnr('BoardType', 2, 0)
        self.hwv         = Serialnr('Hardwarenr.', 2, 1)
        self.month       = Serialnr('Monat', 2, 2)
        self.year        = Serialnr('Jahr', 2, 3)
        self.best        = Serialnr('Bestuecker', 2, 4)
        self.rn_index    = Serialnr('laufendeNr', 4, 5)

        self.type.setSerial(self.cb_cmd.currentIndex())
        self.hwv.setSerial(self.sp_hwv.text())
        self.best.setSerial(self.cb_best.currentIndex())
        self.year.setSerial(self.de_date.text().split('.')[1][1:]) # cutoff millenia
        self.month.setSerial(self.de_date.text().split('.')[0])



        self.pb_chooseFile.clicked.connect(self.chooseFilePath)
        self.pb_transmit.setEnabled(False)
        if not self.cb_req_serial.isChecked():
            self.pb_req_serial.setVisible(False)
            self.l_srialnr_req.setVisible(False)
        # self.timer = QtCore.QTimer(self)  # set up your QTimer
        # self.timer.timeout.connect(self.turnOffLed)  # connect it to your update function
        # self.turnOffLed()

        self.connectWidgets2Functions()
        self.initCAN()
        self.show()


    def chooseFilePath(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file', 'M:\\')
        self.tablePath = fname[0]
        text = self.l_filepath.text().split('\t')[0]
        self.l_filepath.setText(f"{text}\t{self.tablePath}")


    # def turnOffLed(self):
    #     print("timer feuert")
    #     self.led.setChecked(False)
    #
    # def turnOnLed(self, color="green"):
    #     self.led.setStyleSheet(f"""
    #                                QRadioButton::indicator:checked {{background - color:{color}}};
    #                                                                 {{border:1px solid black;}}
    #                                """)
    #     self.led.setChecked(True)
    #     self.timer.start(3)  # set it to timeout in 3000 ms

    def initCAN(self):
        global can_bus
        self.dbc = cantools.database.load_file(r"cmcu_admin.dbc")

        for message in self.dbc.messages:
            model_message = model.Message(message)
            for signal in message.signals:
                model_signal = model.Signal(signal, model_message)
                model_message.signals.append(model_signal)
            self.model.messages.append(model_message)

        self.can_bus = can_setup.init(bitrate=250000)#, channel="PCAN_USBBUS2")
        self.can_notifier = can.Notifier(self.can_bus, [ListenerSubclass(self.receiveCANMessage)])


    def connectWidgets2Functions(self):
        self.pb_gen_serial.clicked.connect(self.generate_serial_clicked)
        self.pb_transmit.clicked.connect(self.transmit_clicked)
        self.pb_req_serial.clicked.connect(self.req_serial_clicked)
        self.cb_best.currentIndexChanged.connect(self.cb_best_changed)
        self.cb_cmd.currentIndexChanged.connect(self.cb_type_changed)
        self.de_date.dateChanged.connect(self.de_changed)
        self.sp_hwv.textChanged.connect(self.le_changed)
        self.print_to_console.connect(self.print_to_console_fkt)


    def print_to_console_fkt(self, text):
        self.tb_infos.append(text)

    def le_changed(self):
        sender = self.sender()
        try:
            self.hwv.setSerial(sender.text())
        except:
            sender.setText('')

    def cb_best_changed(self):
        value = self.sender().currentIndex()
        if value > 0:
            self.best.setSerial(value)

    def cb_type_changed(self):
        value = self.sender().currentIndex()
        if value > 0:
            self.type.setSerial(value)


    def de_changed(self):
        date_list = self.sender().text().split('.')
        self.month.setSerial(date_list[0])
        self.year.setSerial(date_list[1][1:])
            
                
    def req_serial_clicked(self):
        if "board" not in self.cb_cmd.currentText().lower() and "eeprom" not in self.cb_cmd.currentText().lower():
            if "cmcu" in self.cb_cmd.currentText().lower():
                value = 0xC  # req. cmcu serial
            else:
                value = 0xD  # req. if serial
            self.transmitCANMessage("IPC_Cmd", value)
        else:
            self.print_to_console.emit("choose a valid PCB to request a serialnumber!\n")



    def generate_serial_clicked(self):
        if not self.cb_cmd.currentIndex() == 0 \
                and not self.cb_best.currentIndex() == 0:
            self.setUniqueRunningIndex()
            Serialnr.createSerialnr()
            self.l_srialnr.setText(str(Serialnr.serialNumber))
            self.pb_transmit.setEnabled(True)
            text = f'Generated Serialnumber: {Serialnr.serialNumber} for \n'+\
                   f'Board Type:\t{self.cb_cmd.currentText()} \n'+\
                   f'HWV:\t{self.sp_hwv.text()} \n' + \
                   f'Best. Datum:\t{self.de_date.text()} \n'+\
                   f'Bestuecker:\t{self.cb_best.currentText()} \n' +\
                   f'laufende nr.:\t{self.rn_index.getValue()} \n'+\
                   f'------------- \n'+\
                   f'press Transmit button to safe Infos in Database and \n'+\
                   f'transmit Serial number to PCB \n'+\
                   f'------------------------------------------------------------'
            self.print_to_console.emit(text)
        else:
            self.print_to_console.emit("\nChoose a valid PCB and supplyer!\n")


    def transmit_clicked(self):
        cmd = self.cb_cmd.currentText()
        #value = int.from_bytes(Serialnr.serialNumber, byteorder='big', signed=False)
        value = Serialnr.serialNumber
        if cmd == "CMCU":
            msg_name = 'IPC_Set_Serial_Cmcu'
        elif 'EEPROM' in cmd:
            msg_name = 'IPC_Reset_EEPROM'
        else:
            msg_name = 'IPC_Set_Serial_If'

        self.transmit_clicked_bool = True

        success = self.transmitCANMessage(msg_name, value)
        if success:
            self.pb_transmit.setEnabled(False)
        else:
            self.print_to_console.emit("\n not successfull")


    def transmitCANMessage(self, msg_name, value):
        try:
            message_model_name = msg_name
            message_model = self.model.getMessageByName(message_model_name)
            message_prototype = self.dbc.get_message_by_name(message_model.name)
            data_signals = {}
            for signal in message_model.signals:
                # signal.value = int.to_bytes(value, byteorder='big', length=8)
                data_signals.update({signal.name: value})

            data = message_prototype.encode(data_signals)
            canMessage = can.Message(arbitration_id=message_prototype.frame_id,
                                     data=data)
            self.can_bus.send(canMessage)

            return 1
        except Exception as e:
            print('ERROR SENDING')
            print_error(e)
            return 0

    def receiveCANMessage(self, message):
        try:
            slotNum, device_id, message_id = split_arbitration_id(message.arbitration_id)
            raw_data = self.dbc.decode_message(message_id | (1 << 24) | (1 << 28), message.data)
            for key in raw_data.keys():
                if "Serial_Number" in key:
                    # value = int.to_bytes(raw_data[key], byteorder="big", length=8)
                    value = raw_data[key]
                    if self.transmit_clicked_bool:
                        if value == Serialnr.serialNumber:
                            self.save_to_file()
                            self.l_srialnr_req.setText(str(value))
                            self.print_to_console.emit(f'-> Serialnumber was written successfully')
                            self.print_to_console.emit('=================================\n\n')
                        else:
                            self.print_to_console.emit(f'-> received wrong Serialnumber !!!')
                            self.print_to_console.emit('=================================\n\n')
                        self.transmit_clicked_bool = False
                    else:
                        self.print_to_console.emit(f'-> received Serialnumber from PCB: {value}\n')
                        self.print_to_console.emit('=================================\n\n')
                        self.l_srialnr_req.setText(str(value))



        except Exception as e:
            print_error(e)
            print(message)

    def save_to_file(self):
        with open(self.tablePath, 'a', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=' ',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)

            writer.writerow([Serialnr.serialNumber, self.date_string,
                             self.cb_cmd.currentText(), self.sp_hwv.text(),
                             self.de_date.text(), self.cb_best.currentText(),
                             self.rn_index.getValue()
                             ])

            self.print_to_console.emit('-> saved info to database\n')


    def setUniqueRunningIndex(self):
        try:
            df = pd.read_csv(self.tablePath, dtype=object, sep=' ', names=self.colnames, usecols=[0,1,2,3,4,5,6])

            board   = self.cb_cmd.currentText()
            hwv     = self.sp_hwv.text()
            date    = self.de_date.text()
            best    =self.cb_best.currentText()

            cond1   = (df['Board'] == board)
            cond2   = (df['HWV'] == hwv)
            cond3   = (df['pDate'] == date)
            cond4   = (df['manuf'] == best)

            partial_df = df[cond1 & cond2 & cond3 & cond4]

            if partial_df.empty:
                # first board that meets conditions
                self.rn_index.setSerial(1)
            else:
                try:
                    partial_df['runningnr'] = partial_df['runningnr'].str.split(';').str[0].astype(int)
                except Exception as e:
                    print(e)
                current_rn_index = partial_df['runningnr'].astype(int).max()
                self.rn_index.setSerial(current_rn_index+1)
        except Exception as e:
            print(e)



