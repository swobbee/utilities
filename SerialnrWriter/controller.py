import os
from PyQt5 import QtCore
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import can
import cantools
from pprint import pprint

from datetime import datetime
import time, threading

import model
import can_setup


def split_arbitration_id(arbitration_id):
    is_high_prio = not ((arbitration_id >> 28) & 0x01)
    is_from_cmcu = (arbitration_id >> 24) & 0x01
    slotNum = (arbitration_id >> 16) & 0xFF
    device_id = (arbitration_id >> 8) & 0xFF
    message_id = arbitration_id & 0xFF
    return slotNum, device_id, message_id


def join_arbitration_id(device_id, slotNum, message_id):
    is_high_prio = 0
    arbitration_id = (is_high_prio << 28) | (0 << 24) | (slotNum << 16) | (device_id << 8) | message_id
    return arbitration_id


### helper object to setup CAN RX callback
class ListenerSubclass(can.Listener):
    def __init__(self, on_receive):
        self._on_receive = on_receive

    def on_message_received(self, msg):
        self._on_receive(msg)


class Controller(QObject):
    receivedData = QtCore.pyqtSignal(list)
    transmittedData = QtCore.pyqtSignal(list)

    model = model.Model()

    def __init__(self):
        super().__init__()
        self.incoming_msg = []
        self.outgoing_msg = []

        self.substring1 = "CMCU_"
        self.substring2 = "Req"


        ### initialize dbc and create model
        self.dbc = cantools.database.load_file(r'C:\Users\p.goldmann\Documents\interface_definitions\EE_ADMIN_CMCU.dbc')

        for message in self.dbc.messages:
            model_message = model.Message(message)
            if message.name.find("CMCU")==0:
                name = message.name
                self.incoming_msg.append(name)

            else:
                name = message.name
                self.outgoing_msg.append(name)


            for signal in message.signals:
                if signal.name.find('CMCU') == 0:
                    model_signal = model.Signal(signal, model_message)
                    model_message.signals.append(model_signal)
                else:
                    model_signal = model.Signal(signal, model_message)
                    model_message.signals.append(model_signal)
            self.model.messages.append(model_message)

        self.can_bus = can_setup.init(bitrate=250000, channel="PCAN_USBBUS1")
        self.can_notifier = can.Notifier(self.can_bus, [ListenerSubclass(self.receiveCANMessage)])

    def restartCAN_BUS(self, baudrate):
        if (self.can_bus):
            self.can_bus.reset()
            self.can_bus.shutdown()
            self.can_notifier.stop()
            self.can_bus = can_setup.reinit(bitrate=baudrate)
            can.Notifier(self.can_bus, [ListenerSubclass(self.receiveCANMessage)])

    def setView(self, view):
        view.initModel(self.model)
        self.receivedData.connect(view.receive_request)
        self.transmittedData.connect(view.respond_to_request)

    def receiveCANMessage(self, message):
        message_id = None
        try:
            msg = self.dbc.get_message_by_frame_id(message.arbitration_id)
            self.responde_to_msg(msg.name)
            self.receivedData.emit([message.timestamp, msg.name, msg])
        except Exception as e:
            print(e)

    def responde_to_msg(self, msg):
        msg = msg.replace(self.substring1, "")
        msg = msg.replace(self.substring2, "")
        for msgs in self.outgoing_msg:
            if msg in msgs:
                self.transmitCANMessage(msgs)


    @QtCore.pyqtSlot(str)
    def transmitCANMessage(self, msg_name):
        try:
            message_model_name = msg_name
            message_model = self.model.getMessageByName(message_model_name)
            message_prototype = self.dbc.get_message_by_name(message_model.name)
            data_signals = {}
            for signal in message_model.signals:
                if signal.value == None: signal.value = 0
                data_signals.update({signal.name: signal.value})

            data = message_prototype.encode(data_signals)

            canMessage = can.Message(arbitration_id=message_prototype.frame_id,
                                     data=data)
            self.can_bus.send(canMessage)
            self.transmittedData.emit([canMessage.timestamp, message_model_name, data_signals])
        except Exception as e:
            print('ERROR SENDING')
            print(e)



