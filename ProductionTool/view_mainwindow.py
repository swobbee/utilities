import sys
import os

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QMainWindow, QAction, qApp, QApplication, QWidget, QGridLayout, QGroupBox, QPushButton, \
    QVBoxLayout, QHBoxLayout, QLabel
from PyQt5.QtGui import QIcon, QFont, QPixmap
from CmcuUpdater.updater_newBootloader import runUpdate
from PyQt5 import uic
from PyQt5.QtCore import Qt
import os
# from view_cansignalwidget import CANSignalWidget
# from view_canmessagewidget import CANMessageWidget
from collections import namedtuple
import time
# import model
import can
import traceback
import json
import sys
FW_PATH = r"M:\SOFTWARE\Eigene Anwendungen\CM_CMCU_TEST\firmware"
FW_PATH = r"/home/pgoldmann/Dokumente/software_versions/CMCU"
FW_PATH = os.path.join(os.getcwd())

BATINIT_PATH = r"M:\SOFTWARE\Eigene Anwendungen\CM_CMCU_TEST\battery_data.json"
BATINIT_PATH = os.path.join(os.getcwd(), "battery_data.json")
BATDATA = {}


class MainWindow(QMainWindow):
    signal_command_charger_start = QtCore.pyqtSignal(int)
    signal_command_charger_stop = QtCore.pyqtSignal(int)
    signal_command_set_lock = QtCore.pyqtSignal(int, bool)
    signal_send_IPC_Charge_Parameters = QtCore.pyqtSignal(int, int, int)
    signal_send_IPC_Config = QtCore.pyqtSignal(str, str)
    signal_send_IPC_Set_CAN_ID = QtCore.pyqtSignal(int)
    signal_change_CAN_BaudRate = QtCore.pyqtSignal(int)
    signal_req_serial_cmcu = QtCore.pyqtSignal(bool)
    signal_req_serial_if = QtCore.pyqtSignal(bool)
    signal_restart_listener = QtCore.pyqtSignal(int)
    signal_request_bat_data = QtCore.pyqtSignal(int)

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        uic.loadUi("mainwindow.ui", self)
        self.resize(1000, 400)

        self.installEventFilter(self)
        self.terminal.setVisible(False)
        self.group_charger.setVisible(False)
        self.pb_setCanId.setVisible(False)
        self.show()
        self.readBatInit()

        self.cb_firmware.addItem("None")
        self.cb_firmware.addItems([file for file in os.listdir(FW_PATH) if file.endswith('.hex')])
        self.init_battery_slots(8)

        self.button_send_IPC_Config.clicked.connect(self.transmit_IPC_Config)
        self.pb_changeBaudrate.clicked.connect(self.change_CAN_BaudRate)
        self.pb_setCanId.clicked.connect(self.change_CAN_Id)
        self.pb_setCanId.setToolTip('Id can only be set if CM-Door is closed!')

        self.pb_firmwareUpdate.setEnabled(False)
        self.pb_changeBaudrate.setEnabled(False)

        self.charger_set.clicked.connect(self.transmit_charger_parameters)

        self.cb_firmware.currentIndexChanged.connect(self.firmware_en)
        self.IPC_Config_BaudRate_ID.currentIndexChanged.connect(self.baud_en)

        self.le_serialCmcu.textChanged.connect(self.checkSerial)
        self.le_serialIf.textChanged.connect(self.checkSerial)

        self.IPC_Config_Battery_Type.currentIndexChanged.connect(self.setBatData)


    def setBatData(self):
        self.readBatInit()
        BatteryType = self.IPC_Config_Battery_Type.currentText()
        try:
            current = BATDATA[BatteryType]['current']
            voltage = BATDATA[BatteryType]['voltage']
            current_min = BATDATA[BatteryType]['current_min']

            self.charger_voltage_max.setValue(voltage / 1000)
            self.charger_current_max.setValue(current / 1000)
            self.charger_current_min.setValue(current_min / 1000)
        except:
            current = 0
            voltage = 0
            current_min = 0
        self.signal_send_IPC_Charge_Parameters.emit(voltage, current, current_min)

    def firmware_en(self):
        # set fw button active
        enable = False
        if self.cb_firmware.currentIndex() != 0:
            enable = True
        self.pb_firmwareUpdate.setEnabled(enable)

    def baud_en(self):
        # set baud button active
        enable = False
        if self.IPC_Config_BaudRate_ID.currentIndex() != 0:
            enable = True
        self.pb_changeBaudrate.setEnabled(enable)

    def doFirmwareUpdate(self, bus, notifier):
        path = os.path.join(FW_PATH, self.cb_firmware.currentText())
        self.cb_firmware.setCurrentIndex(0)
        self.worker = WorkerThread(path=path, bus=bus, notifier=notifier, device_id=self.sb_setCanId.value())
        self.worker.start()
        self.worker.finished.connect(self.update_done)
        self.worker.finished.connect(lambda: self.signal_restart_listener.emit(250000))
        self.showProgressInfo()
        self.worker.update_progress.connect(self.updateFwuProgress)


    def showProgressInfo(self):
        self.msgBox = QtWidgets.QMessageBox()
        self.msgBox.setWindowTitle('UPDATE PROGRESS')
        l = self.msgBox.layout()
        l.itemAtPosition(l.rowCount() - 1, 0).widget().hide()
        self.progress = QtWidgets.QProgressBar()
        l.addWidget(self.progress)
        self.msgBox.show()

    def updateFwuProgress(self, val):
        # update the progressbar for fw update
        self.progress.setValue(val)

    def update_done(self):
        # show
        self.msgBox.close()
        QtWidgets.QMessageBox.information(self, "Done", "Update finished successfully")

    ### GUI update ----------------------------------------
    def addToTerminal(self, text):
        # is_error = self.isError(text)
        # if is_error:
        #     fmt = self.terminal.currentCharFormat()
        #     fmt.setForeground(QtCore.Qt.red)
        #     self.terminal.setCurrentCharFormat(fmt)
        self.terminal.append(text)
        self.terminal.ensureCursorVisible()
        self.terminal.verticalScrollBar().setValue(self.terminal.verticalScrollBar().maximum())
        # if is_error:
        #     fmt.clearForeground()
        #     self.terminal.setCurrentCharFormat(fmt)

    def init_battery_slots(self, numSlots):
        BatteryView = namedtuple('BatteryView', 'slot state serial locker charger soc soh voltage current temp_cell_max temp_cell_min temp_charger')
        self.battery_slots = []
        for i in range(numSlots):

            if i == 0:
                slot = BatteryView(self.battery_slot, self.battery_state, self.battery_serial, self.battery_locker, self.battery_charger,
                                   self.battery_soc, self.battery_soh, self.battery_voltage, self.battery_current,
                                   self.battery_temp_cell_max, self.battery_temp_cell_min, self.battery_temp_charger)
            else:
                slot = BatteryView(QtWidgets.QLabel(self), QtWidgets.QLineEdit(self), QtWidgets.QLineEdit(self),
                                   QtWidgets.QPushButton(self), QtWidgets.QPushButton(self), QtWidgets.QProgressBar(self),
                                   QtWidgets.QProgressBar(self), QtWidgets.QLineEdit(self), QtWidgets.QLineEdit(self),
                                   QtWidgets.QLineEdit(self), QtWidgets.QLineEdit(self), QtWidgets.QLineEdit(self))

                # copy style
                slot.slot.setStyleSheet(self.battery_slots[0].state.styleSheet())
                slot.state.setStyleSheet(self.battery_slots[0].state.styleSheet())
                slot.serial.setStyleSheet(self.battery_slots[0].state.styleSheet())
                slot.locker.setStyleSheet(self.battery_slots[0].locker.styleSheet())
                slot.charger.setStyleSheet(self.battery_slots[0].charger.styleSheet())
                slot.soc.setStyleSheet(self.battery_slots[0].soc.styleSheet())
                slot.soh.setStyleSheet(self.battery_slots[0].soh.styleSheet())
                slot.voltage.setStyleSheet(self.battery_slots[0].voltage.styleSheet())
                slot.current.setStyleSheet(self.battery_slots[0].voltage.styleSheet())
                slot.temp_cell_max.setStyleSheet(self.battery_slots[0].voltage.styleSheet())
                slot.temp_cell_min.setStyleSheet(self.battery_slots[0].voltage.styleSheet())
                slot.temp_charger.setStyleSheet(self.battery_slots[0].voltage.styleSheet())

                # copy initial content
                slot.slot.setText(f'{self.battery_slots[0].slot.text().split()[0]} {int(self.battery_slots[0].slot.text().split()[-1]) + i}')
                slot.serial.setText(self.battery_slots[0].serial.text())
                slot.state.setText(self.battery_slots[0].state.text())
                slot.locker.setText(self.battery_slots[0].locker.text())
                slot.charger.setText(self.battery_slots[0].charger.text())
                slot.soc.setValue(self.battery_slots[0].soc.value())
                slot.soh.setValue(self.battery_slots[0].soh.value())
                slot.voltage.setText(self.battery_slots[0].voltage.text())
                slot.current.setText(self.battery_slots[0].current.text())
                slot.temp_cell_max.setText(self.battery_slots[0].temp_cell_max.text())
                slot.temp_cell_min.setText(self.battery_slots[0].temp_cell_min.text())
                slot.temp_charger.setText(self.battery_slots[0].temp_charger.text())

            self.battery_slots.append(slot)
            self.battery_slots[i].charger.clicked.connect(self.slot_charger_clicked)
            self.battery_slots[i].locker.clicked.connect(self.slot_locker_clicked)

            self.battery_slots[i].state.setToolTip("")

            self.battery_slots[i].slot.setAlignment(Qt.AlignCenter)
            self.battery_slots[i].state.setAlignment(Qt.AlignLeft)
            self.battery_slots[i].serial.setAlignment(Qt.AlignRight)
            self.battery_slots[i].soc.setAlignment(Qt.AlignRight)
            self.battery_slots[i].soh.setAlignment(Qt.AlignRight)
            self.battery_slots[i].voltage.setAlignment(Qt.AlignRight)
            self.battery_slots[i].current.setAlignment(Qt.AlignRight)
            self.battery_slots[i].temp_cell_max.setAlignment(Qt.AlignRight)
            self.battery_slots[i].temp_cell_min.setAlignment(Qt.AlignRight)
            self.battery_slots[i].temp_charger.setAlignment(Qt.AlignRight)



            self.grid_battery_data.addWidget(self.battery_slots[i].slot, 0, 1 + i)
            self.grid_battery_data.addWidget(self.battery_slots[i].state, 1, 1 + i)
            self.grid_battery_data.addWidget(self.battery_slots[i].serial, 2, 1 + i)
            self.grid_battery_data.addWidget(self.battery_slots[i].locker, 3, 1 + i)
            self.grid_battery_data.addWidget(self.battery_slots[i].charger, 4, 1 + i)
            self.grid_battery_data.addWidget(self.battery_slots[i].soc, 5, 1 + i)
            self.grid_battery_data.addWidget(self.battery_slots[i].soh, 6, 1 + i)
            self.grid_battery_data.addWidget(self.battery_slots[i].voltage, 7, 1 + i)
            self.grid_battery_data.addWidget(self.battery_slots[i].current, 8, 1 + i)
            self.grid_battery_data.addWidget(self.battery_slots[i].temp_cell_max, 9, 1 + i)
            self.grid_battery_data.addWidget(self.battery_slots[i].temp_cell_min, 10, 1 + i)
            self.grid_battery_data.addWidget(self.battery_slots[i].temp_charger, 11, 1 + i)

    def slot_setNumber(self, number):
        for i in range(len(self.battery_slots)):
            for key, value in self.battery_slots[i]._asdict().items():
                value.setVisible(i < number)

        if 0:
            vis = number > 1
            self.label_battery_locker.setVisible(vis)
            self.label_battery_charger.setVisible(vis)
            for i in range(number):
                self.battery_slots[i].locker.setVisible(vis)
                self.battery_slots[i].charger.setVisible(vis)

    def slot_charger_clicked(self):
        for i in range(len(self.battery_slots)):
            if self.sender() == self.battery_slots[i].charger:
                if self.battery_slots[i].state.text().find('CHARGING') >= 0:
                    self.signal_command_charger_stop.emit(i)
                else:
                    self.signal_command_charger_start.emit(i)

    def slot_locker_clicked(self):
        for i in range(len(self.battery_slots)):
            if self.sender() == self.battery_slots[i].locker:
                if self.sender().text() == "Lock":
                    self.sender().setText("Unlock")
                    self.signal_command_set_lock.emit(i, True)
                else:
                    self.sender().setText("Lock")
                    self.signal_command_set_lock.emit(i, False)

    def change_CAN_BaudRate(self):
        baudrate = self.IPC_Config_BaudRate_ID.currentText()
        if baudrate is not None:
            self.signal_change_CAN_BaudRate.emit(baudrate)

    def change_CAN_Id(self):
        canId = self.sb_setCanId.value()
        if canId is not None:
            self.signal_send_IPC_Set_CAN_ID.emit(canId)

    def initModel(self, model):
        self.model = model
        for message in model.messages:
            for signal in message.signals:
                if signal.name == 'CMCU_Config_Charger_Type':
                    entrys = [self.IPC_Config_Charger_Type.itemText(i) for i in range(self.IPC_Config_Charger_Type.count())]
                    for charger_type in signal.choices.values():
                        if charger_type not in entrys:
                            self.IPC_Config_Charger_Type.addItem(str(charger_type))

                if signal.name == 'CMCU_Config_Battery_Type':
                    for bat_type in signal.choices.values():
                        entrys = [self.IPC_Config_Battery_Type.itemText(i) for i in range(self.IPC_Config_Battery_Type.count())]
                        if bat_type not in entrys:
                            self.IPC_Config_Battery_Type.addItem(str(bat_type))


    def displayCanId(self, ID):
        if not self.cb_setCanId.isChecked():
            self.sb_setCanId.setValue(ID)
        if ID != 255:
            self.sb_setCanId.setStyleSheet("background:red")
            self.sb_setCanId.setToolTip("CanId not 255  -> FW Update not possible!\nclick Checkbox 'enable Set Can Id' if Id needs to be changed")
        else:
            self.sb_setCanId.setStyleSheet("background:white")
            self.sb_setCanId.setToolTip("click Checkbox 'enable Set Can Id' if Id needs to be changed")


    def checkSerial(self):
        sender_name = self.sender().objectName()
        if len(self.sender().text()) == 14:
            if 'Cmcu' in sender_name:
                self.signal_req_serial_cmcu.emit(True)
            else:
                self.signal_req_serial_if.emit(True)
        if self.sender().text() == '':
            self.sender().setStyleSheet("background:white")


    def displaySnbrCheck(self, board_type, snbr):
        widget = self.le_serialCmcu
        if board_type == 'IF':
            widget = self.le_serialIf

        widget.setToolTip(f'Serialnr. on board is: {snbr}')
        try:
            if widget.text() != '':
                if int(widget.text()) == snbr:
                    widget.setStyleSheet("background:green")
                else:
                    widget.setStyleSheet("background:red")
        except Exception as e:
            print(traceback.format_exc())




    def clearSlot(self, slot):
        self.battery_slots[slot].serial.setText('')
        self.battery_slots[slot].soc.setValue(0)
        self.battery_slots[slot].soh.setValue(0)
        self.battery_slots[slot].voltage.setText(str(0) + ' V')
        self.battery_slots[slot].current.setText(str(0) + ' A')
        self.battery_slots[slot].temp_cell_max.setText(str(0) + ' °C')
        self.battery_slots[slot].temp_cell_min.setText(str(0) + ' °C')
        self.battery_slots[slot].temp_charger.setText(str(0) + ' °C')
        self.battery_slots[slot].state.setToolTip("")

    def updateSignals(self, signal_dict):
        slotNum = signal_dict['slotNum']
        for key, value in signal_dict.items():
            ### filter for 'CMCU_' signals
            if key.find('CMCU_') == 0:
                # for i in range(self.signalListRX.count()):
                #     signal_widget = self.signalListRX.itemWidget(self.signalListRX.item(i))
                #     if signal_widget:
                #         if signal_widget.model.name == key:
                #             signal_widget.setValue(value)

                ### manual mapping to dashboard view
                if key == 'CMCU_State':
                    value = str(value)
                    self.stateLabel.setText(value)


                    if value.find('OK_DOOR_CLOSED') != -1:
                        self.stateLabel.setStyleSheet("color:black")
                    elif value.find('OK_DOOR_OPEN') != -1:
                        self.stateLabel.setStyleSheet("color:blue")
                    elif value.find('ERROR') != -1:
                        self.stateLabel.setStyleSheet("color:red")
                    elif value.find('INIT') != -1:
                        self.stateLabel.setStyleSheet("color:grey")
                    elif value.find('BOOTLOADER') != -1:
                        self.stateLabel.setStyleSheet("color:yellow")
                    else:
                        self.stateLabel.setText('ADMINAPP')
                        self.stateLabel.setStyleSheet("color:purple")


                    self.charger_start.setEnabled(1)
                    self.charger_stop.setEnabled(1)
                    self.charger_set.setEnabled(1)

                    isErrorState = value.find('ERROR') != -1
                    self.label_cmcu_error.setVisible(isErrorState)
                    self.cmcu_error.setVisible(isErrorState)

                    isDoorClosed = value.find('OK_DOOR_OPEN') == -1
                    self.door_state.setText(value)
                    self.door_open.setEnabled(isDoorClosed)

                    if isDoorClosed:
                        self.door_state.setStyleSheet("color:black")
                    elif value.find('OK_DOOR_OPEN') != -1:
                        self.door_state.setStyleSheet("color:blue")
                    elif value.find('ERROR') != -1:
                        self.door_state.setStyleSheet("color:red")


                    signal = self.model.getSignalByName('CMCU_State')


                if key.find('CMCU_Temp_Ext') != -1:
                    self.temp_ext.setText(str(value) + ' °C')
                if key.find('Alternative_1') != -1:
                    self.temp_b.setText(str(value) + ' °C')
                if key.find('Alternative_2') != -1:
                    self.temp_a.setText(str(value) + ' °C')

                if key.find('CMCU_Slot_') == 0 and "Error" not in key:
                    slot = int(key.split('_')[-2])
                    value = str(value)
                    self.battery_slots[slot].state.setText(str(value))


                    for k, v in self.battery_slots[slot]._asdict().items():
                        v.setEnabled(value.find('DISABLED') == -1)
                    if value.find('UNKNOWN') >= 0:
                        self.battery_slots[slot].state.setStyleSheet("background:red")
                        self.clearSlot(slot)
                    if value.find('DISABLED') >= 0:
                        self.battery_slots[slot].state.setStyleSheet("background:gray")
                        self.clearSlot(slot)

                    if value.find('EMPTY') >= 0:
                        self.battery_slots[slot].state.setStyleSheet("background:white")
                        self.clearSlot(slot)
                        if value.find("ERROR") < 0:
                            self.battery_slots[slot].state.setToolTip('EMPTY')
                    if value.find('STORAGE') >= 0 and value.find("ERROR") < 0:
                        self.battery_slots[slot].state.setStyleSheet("background:#CEE741")
                        self.battery_slots[slot].state.setToolTip("SLOT OK")
                    if value.find('CHARGING') >= 0:
                        self.battery_slots[slot].state.setStyleSheet("background:#DA0063")
                        self.battery_slots[slot].state.setToolTip("SLOT OK")
                    if value.find('ERROR') >= 0:
                        self.battery_slots[slot].state.setStyleSheet("background:red")
                        if self.battery_slots[slot].state.toolTip() in ['EMPTY','SLOT OK',""]:
                            self.signal_request_bat_data.emit(slot)


                    if value.find('CHARGING') >= 0:
                        self.battery_slots[slot].charger.setText("Stop")
                    else:
                        self.battery_slots[slot].charger.setText("Start")

                if key.find('CMCU_SLOT_ERROR') != -1 and value == 1:
                    error_name = '_'.join(key.split('_')[2:])
                    self.battery_slots[slotNum].state.setToolTip(error_name)




                if key.find('CMCU_ERROR_') != -1:
                    if (value == 1):
                        error = str(key)
                        error.replace("CMCU_ERROR_", "")
                        self.cmcu_error.setText(error)
                        self.cmcu_error.setStyleSheet("background:red")

                if key.find('CMCU_Bat') == 0:
                    slot = int(key[8])

                    if key.find('_Serial_Number') > 0:
                        self.battery_slots[slot].serial.setText(str(value))
                    if key.find('_SOC') > 0:
                        self.battery_slots[slot].soc.setValue(int(value))
                    if key.find('_SOH') > 0:
                        self.battery_slots[slot].soh.setValue(int(value))
                    if key.find('_Voltage') > 0:
                        self.battery_slots[slot].voltage.setText(str(value / 1000) + ' V')
                    if key.find('_Current') > 0:
                        self.battery_slots[slot].current.setText(str(value / 1000) + ' A')
                    if key.find('_Temp_Cell_Max') > 0:
                        self.battery_slots[slot].temp_cell_max.setText(str(value) + ' °C')
                    if key.find('_Temp_Cell_Min') > 0:
                        self.battery_slots[slot].temp_cell_min.setText(str(value) + ' °C')



                if key.find('CMCU_Charger_Temp_1') != -1:
                    self.battery_slots[int(slotNum)].temp_charger.setText(str(value) + ' °C')

                if key.find('Temp_IF_Ext') > 0:
                    slot = int(key.split('_')[-1])
                    self.battery_slots[slot].temp_charger.setText(str(value) + ' °C')

                if key == 'CMCU_Config_Charger_Type':
                    index = self.IPC_Config_Charger_Type.findText(str(value), QtCore.Qt.MatchFixedString)
                    if index >= 0:
                        self.IPC_Config_Charger_Type.setCurrentIndex(index)

                if key == 'CMCU_Config_Battery_Type':
                    index = self.IPC_Config_Battery_Type.findText(str(value), QtCore.Qt.MatchFixedString)

                    # okai_string = 'OKAI'
                    # if value == okai_string:
                    self.slot_setNumber(8)
                    self.cb_charger_parameters.setChecked(True)
                    self.battery_charger.setEnabled(True)
                    # else:
                    #     self.slot_setNumber(1)
                    #     self.cb_charger_parameters.setChecked(True)
                    #     self.battery_charger.setEnabled(False)


                    # self.charger_start.setVisible(value != okai_string)
                    # self.charger_stop.setVisible(value != okai_string)

                    self.battery_type.setText(str(value))

                    if index >= 0:
                        self.IPC_Config_Battery_Type.setCurrentIndex(index)

                if 'Serial' in key and 'Bat' not in key:
                    board = 'IF' if 'IF' in key else "CMCU"
                    self.displaySnbrCheck(board_type=board, snbr=value)

    def readBatInit(self):
        with open(BATINIT_PATH) as BATDATA_file:
            bat_data = json.load(BATDATA_file)
            for key in bat_data["bat_default_values_config"]['value']:
                try:
                    BATDATA[key] = bat_data["bat_default_values_config"]['value'][key]
                except:
                    pass

    def transmit_charger_parameters(self):
        voltage_max_mV = self.charger_voltage_max.value() * 1000
        current_max_mA = self.charger_current_max.value() * 1000
        current_min_mA = self.charger_current_min.value() * 1000
        self.signal_send_IPC_Charge_Parameters.emit(voltage_max_mV, current_max_mA, current_min_mA)

    def transmit_IPC_Config(self):
        ChargerType = self.IPC_Config_Charger_Type.currentText()
        BatteryType = self.IPC_Config_Battery_Type.currentText()

        self.signal_send_IPC_Config.emit(ChargerType, BatteryType)


class WorkerThread(QtCore.QThread):
    update_progress = QtCore.pyqtSignal(int)
    def __init__(self, path, bus, notifier, device_id):
        super(WorkerThread, self).__init__()
        self.bus = bus
        self.path = path
        self.notifier = notifier
        self.device_id = device_id

    def run(self):
        self.notifier.stop() ## has to be removed temporarily so update works
        runUpdate(filepath=self.path,
                  can_bus=self.bus,
                  ID=self.device_id,
                  signal_running=self.update_progress
                 )



