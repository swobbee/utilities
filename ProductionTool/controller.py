import os
from PyQt5 import QtCore
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *


import can
import cantools
import traceback
import sys
import model
import can_setup
import inspect
import json

def split_arbitration_id(arbitration_id):
    slot_num = (arbitration_id >> 16) & 0x7
    device_id = (arbitration_id >> 8) & 0xFF
    message_id = arbitration_id & 0xFF
    return slot_num, device_id, message_id


def join_arbitration_id(device_id, slot_num, message_id):
    arbitration_id = (slot_num << 16) | (device_id << 8) | message_id
    return arbitration_id


### helper object to setup CAN RX callback
class ListenerSubclass(can.Listener):
	def __init__(self, on_receive):
		self._on_receive = on_receive
	def on_message_received(self, msg):
		self._on_receive(msg)

class Controller(QObject):
	
	receivedData = QtCore.pyqtSignal(dict)
	requestedDebugLog = QtCore.pyqtSignal(str)
	receivedId    = QtCore.pyqtSignal(int)
	snbrCheck     = QtCore.pyqtSignal(bool)
	model = model.Model()

	CMCU_CAN_ID = 255
	current_slot = 0

	c_min = None
	c_max = None
	v_max = None

	def __init__(self):
		super().__init__()

		### initialize dbc and create model
		self.dbc = cantools.database.load_file(r"cmcu_ipc.dbc")
		
		for message in self.dbc.messages:
			model_message = model.Message(message)

			for signal in message.signals:
				# prepare signals for 4 slots
				if signal.name.find('CMCU_Bat') == 0:
					name = signal.name
					for i in range(8):
						signal.name = name[:8] + str(i) + name[8:]
						model_signal = model.Signal(signal, model_message)
						model_message.signals.append(model_signal)
					signal.name = name
				else:
					model_signal = model.Signal(signal, model_message)
					model_message.signals.append(model_signal)
			
			self.model.messages.append(model_message)

		self.can_bus = can_setup.init(bitrate=250000)
		self.can_notifier = can.Notifier(self.can_bus, [ListenerSubclass(self.receiveCANMessage)])




	def restartCAN_BUS(self, baudrate):
		if (self.can_bus):
			self.can_bus.reset()
			self.can_bus.shutdown()
			self.can_notifier.stop()
			self.can_bus = can_setup.reinit(bitrate=baudrate)
			self.can_notifier = can.Notifier(self.can_bus, [ListenerSubclass(self.receiveCANMessage)])


	def setView(self, view):
		view.initModel(self.model)
		self.receivedData.connect(view.updateSignals)
		self.requestedDebugLog.connect(view.addToTerminal)	
		self.receivedId.connect(view.displayCanId)
		self.snbrCheck.connect(view.displaySnbrCheck)
		
		view.pb_firmwareUpdate.clicked.connect(lambda: view.doFirmwareUpdate(self.can_bus, self.can_notifier))
		view.door_open.clicked.connect(self.command_door_open)
		view.cmcu_reset.clicked.connect(self.command_cmcu_reset) 
		view.charger_start.clicked.connect(self.command_charger_start)
		view.charger_stop.clicked.connect(self.command_charger_stop)
		view.pb_setCanId.clicked.connect(self.set_Can_Id)
		view.signal_req_serial_if.connect(self.req_Serial_If)
		view.signal_req_serial_cmcu.connect(self.req_Serial_CMCU)
		view.signal_restart_listener.connect(self.restartCAN_BUS)

		view.signal_command_charger_start.connect(self.command_charger_start)
		view.signal_command_charger_stop.connect(self.command_charger_stop)
		view.signal_send_IPC_Charge_Parameters.connect(self.send_IPC_Charge_Parameters)
		view.signal_command_set_lock.connect(self.command_set_lock)
		view.signal_send_IPC_Config.connect(self.send_IPC_Config)
		view.signal_change_CAN_BaudRate.connect(self.restartCAN_BUS)
		view.signal_send_IPC_Set_CAN_ID.connect(self.set_Can_Id)
		view.signal_request_bat_data.connect(self.req_bat_error)

		view.pb_l0.clicked.connect(self.set_Fan_Level)
		view.pb_l1.clicked.connect(self.set_Fan_Level)
		view.pb_l2.clicked.connect(self.set_Fan_Level)
		view.pb_l3.clicked.connect(self.set_Fan_Level)



		# connect Message list
		# for i in range(view.messageListTX.count()):
		# 	message_widget = view.messageListTX.itemWidget(view.messageListTX.item(i))
		# 	message_widget.signal_transmitCANMessage.connect(self.transmitCANMessage)
	
	def receiveCANMessage(self, message):
		message_id = None
		try:
			slotNum, device_id, message_id = split_arbitration_id(message.arbitration_id)
			raw_data = self.dbc.decode_message(message_id | 0x1FF80000, message.data)

			# differentiate into 4 slotNum s
			data = {'slotNum':slotNum}
			for key, value in raw_data.items():
				if key.find('CMCU_Bat') == 0:
					new_key = key[:8] + str(slotNum) + key[8:]
					data[new_key] = value
				else:
					data[key] = value

			# update GUI (via signal->slot)
			self.receivedId.emit(device_id)
			self.receivedData.emit(data)
			self.requestedDebugLog.emit(str(message))

		except Exception as excp:
			try:
				msg = self.dbc.get_message_by_frame_id(message_id)
			except:
				msg = None
			error = ["ERROR while trying to process the following received message"]
			error.append(str(message))
			if message_id is None:
				error.append("failed to split arbitration id")
			elif msg:
				error.append("message name: %s" % msg.name)
			else:
				error.append("unknown message id 0x%x" % message_id)
			error.append(str(excp))
			error.append("")

			self.requestedDebugLog.emit("\n".join(error))
	
	def transmitCANMessage(self, message_model, slot_num=-1):
			try:
				message_prototype = self.dbc.get_message_by_name(message_model.name)
				data_signals = {}
				for signal in message_model.signals:
					data_signals.update({signal.name:signal.value})
				data = message_prototype.encode(data_signals)
				
				if (message_model.name == 'IPC_Set_CAN_ID'):
					self.CMCU_CAN_ID = 0 # set New CAN_ID via broadcast for now

				if slot_num == -1:
					slot_num = self.current_slot

				canMessage = can.Message(arbitration_id=join_arbitration_id(self.CMCU_CAN_ID, slot_num, message_prototype.frame_id), data=data)
				self.can_bus.send(canMessage)
				
				# if (message_model.name == 'IPC_Set_CAN_ID'):
				# 	self.use_CMCU_CAN_ID(message_model.signals[0].value)
				print(inspect.getouterframes(inspect.currentframe())[1])
				# print(f'Sending {self.CMCU_CAN_ID}')
			except Exception as e:
				print(print(traceback.format_exc()))
				print('ERROR SENDING')

	def use_CMCU_CAN_ID(self, value):
		self.CMCU_CAN_ID = value

	def use_current_slot(self, id):
		self.current_slot = id


	def set_Fan_Level(self):
		level = int(self.sender().objectName()[-1])
		message_model = self.model.getMessageByName('IPC_Set_Fan_Speed')
		message_model.getSignalByName('Fan_Level').value = level
		self.transmitCANMessage(message_model)


	def set_Can_Id(self, ID):
		if ID is not False:
			newId = ID
			message_model = self.model.getMessageByName('IPC_Set_CAN_ID')
			message_model.getSignalByName('IPC_Config_CAN_ID').value = newId
			self.transmitCANMessage(message_model)
		else:
			print(f'{ID}: {inspect.getouterframes(inspect.currentframe())[1]}')



	def send_IPC_Config(self, ChargerType, BatteryType):
		message_model = self.model.getMessageByName('IPC_Set_Config')
		sig_charger = message_model.getSignalByName('CMCU_Config_Charger_Type')
		sig_bat		= message_model.getSignalByName('CMCU_Config_Battery_Type')

		ChargerTypeIndex = list(sig_charger.choices.keys())[list(sig_charger.choices.values()).index(ChargerType)]
		BatteryTypeIndex = list(sig_bat.choices.keys())[list(sig_bat.choices.values()).index(BatteryType)]

		message_model.getSignalByName('CMCU_Config_Charger_Type').value = ChargerTypeIndex
		message_model.getSignalByName('CMCU_Config_Battery_Type').value = BatteryTypeIndex

		self.transmitCANMessage(message_model)


	def send_IPC_Charge_Parameters(self, voltage_max_mV, current_max_mA, current_min_mA, slotNum=-1):
		self.v_max = voltage_max_mV
		self.c_max = current_max_mA
		self.c_min = current_min_mA
		message_model = self.model.getMessageByName('IPC_Charge_Parameters')
		message_model.getSignalByName('IPC_Charge_Voltage_Max').value = voltage_max_mV
		message_model.getSignalByName('IPC_Charge_Current_Max').value = current_max_mA
		message_model.getSignalByName('IPC_Charge_Current_Min').value = current_min_mA
		self.transmitCANMessage(message_model, slotNum)

	def req_Serial_CMCU(self):
		self.send_IPC_Cmd(value=0x0C)

	def req_Serial_If(self):
		self.send_IPC_Cmd(value=0x0D)

	def send_IPC_Cmd(self, value, slotNum=-1):
		message_model = self.model.getMessageByName('IPC_Cmd')
		message_model.getSignalByName('Cmd').value = value
		self.transmitCANMessage(message_model, slotNum)

	def req_bat_error(self, slotNum):
		self.send_IPC_Cmd(value=17, slotNum=slotNum)

	def command_door_open(self):
		self.send_IPC_Cmd('DOOR_OPEN')

	def command_cmcu_reset(self):
		self.send_IPC_Cmd('RESET')

	def command_charger_start(self, slotNum=-1):
		print('Slot #' + str(slotNum) + ' start charger!')
		if self.v_max:
			self.send_IPC_Charge_Parameters(self.v_max, self.c_max, self.c_min, slotNum)
		self.send_IPC_Cmd('CHARGE_START', slotNum)
		
	def command_charger_stop(self, slotNum=-1):
		print('Slot #' + str(slotNum) + ' stop  charger!')
		self.send_IPC_Cmd('CHARGE_STOP', slotNum)
		
	def command_set_lock(self, slotNum=-1, lockValue=True):
		if lockValue:
			print('Lock Slot #' + str(slotNum) + '!')
			self.send_IPC_Cmd('BATTERY_LOCK', slotNum)
		else:
			print('Unlock Slot #' + str(slotNum) + '!')
			self.send_IPC_Cmd('BATTERY_UNLOCK', slotNum)


