# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['main.py'],
             pathex=['C:\\Users\\p.goldmann\\PycharmProjects\\utilities'],
             binaries=[],
             datas=[('*', '.'), ('..\\CmcuUpdater\\updater_newBootloader.py','.\\CmcuUpdater')],
             hiddenimports=['can.interfaces.pcan'],
             hookspath=[],
             runtime_hooks=[],
             hooksconfig={},
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='CMCU_CONTROLLER',
          debug=True,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True,
          disable_windowed_traceback=False,
        argv_emulation=False,
        target_arch=None,
        codesign_identity=None,
        entitlements_file=None,
    )