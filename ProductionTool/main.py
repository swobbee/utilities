#!/usr/bin/env python3

from PyQt5 import QtWidgets

import sys
from view_mainwindow import MainWindow
from controller import Controller
import threading

if __name__ == '__main__':
	import os
	path = os.path.split(__file__)[0]
	if path:
		os.chdir(path)

	app = QtWidgets.QApplication(sys.argv)

	mainwindow = MainWindow()
	controller = Controller()

	controller.setView(mainwindow)

	app.exec()